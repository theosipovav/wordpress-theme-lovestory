<div class="profile-preview">
    <?php



    $isBirthday = false;
    if (validateDate(ThemexUser::$data['active_user']['profile']['age'])) {
        $birthdayDateTime = new DateTime(ThemexUser::$data['active_user']['profile']['age'], new DateTimeZone('EUROPE/Moscow'));
        $curDateTime = new DateTime("now", new DateTimeZone('EUROPE/Moscow'));
        if ($curDateTime->format("d") == $birthdayDateTime->format("d") && $curDateTime->format("m") == $birthdayDateTime->format("m")) {
            $isBirthday = true;
        }
    }
    ?>

    <?php if ($isBirthday == true) { ?>
        <div class="profile-birthday">
            <img src="<?= get_template_directory_uri() . "/assets/img/birthday_" . rand(1, 3) . ".svg" ?>" alt="">
            <a class="colorbox inline" href="#gifts_listing" title="У пользователя сегодня ДЕНЬ РОЖДЕНИЯ! Не забудьте поздравить его!" data-value="<?= ThemexUser::$data['active_user']["ID"] ?>"></a>
        </div>
    <?php } ?>

    <div class="profile-image">
        <a href="<?php echo ThemexUser::$data['active_user']['profile_url']; ?>">
            <img src="<?= ThemexUser::getUrlAvatar(ThemexUser::$data['active_user']['ID'], 300) ?>" class="avatar" width="300">
        </a>
        <div class="profile-image-count"><i class="fas fa-camera"></i>
            <span>
                <?php

                $count = count(ThemexUser::getUser(ThemexUser::$data['active_user']["ID"], true)["photos"]);
                if (!ThemexCore::getUserMeta(ThemexUser::$data['active_user']["ID"], 'avatar') == '') {
                    $count++;
                }
                echo $count; ?>
            </span>
        </div>
    </div>
    <?php if (is_page_template('template-search.php') || is_page_template('template-profiles.php') || ThemexCore::isRewriteRule('message') || ThemexCore::isRewriteRule('chat') || ThemexUser::isUserFilter()) { ?>
        <div class="profile-text">
            <h5><?php get_template_part('module', 'status'); ?><a href="<?php echo ThemexUser::$data['active_user']['profile_url']; ?>"><?php echo ThemexUser::$data['active_user']['profile']['full_name']; ?></a></h5>
            <p><?php echo ThemexUser::getExcerpt(ThemexUser::$data['active_user']['profile']); ?></p>
        </div>
    <?php } ?>
    <div class="profile-options popup-container clearfix">
        <div class="profile-option">
            <form class="ajax-form" action="<?php echo AJAX_URL; ?>" method="POST">
                <?php if (ThemexUser::isFavorite(ThemexUser::$data['active_user']['ID'])) { ?>
                    <a href="#" title="<?php _e('Remove from Favorites', 'lovestory'); ?>" data-title="<?php _e('Add to Favorites', 'lovestory'); ?>" class="icon-heart submit-button current"></a>
                    <input type="hidden" class="toggle" name="user_action" value="remove_favorite" data-value="add_favorite" />
                <?php } else { ?>
                    <a href="#" title="<?php _e('Add to Favorites', 'lovestory'); ?>" data-title="<?php _e('Remove from Favorites', 'lovestory'); ?>" class="icon-heart submit-button"></a>
                    <input type="hidden" class="toggle" name="user_action" value="add_favorite" data-value="remove_favorite" />
                <?php } ?>
                <input type="hidden" name="user_favorite" value="<?php echo ThemexUser::$data['active_user']['ID']; ?>" />
                <input type="hidden" class="nonce" value="<?php echo wp_create_nonce(THEMEX_PREFIX . 'nonce'); ?>" />
                <input type="hidden" class="action" value="<?php echo THEMEX_PREFIX; ?>update_user" />
            </form>
        </div>
        <?php if (!ThemexCore::checkOption('user_gifts')) { ?>
            <div class="profile-option">
                <a href="#gifts_listing" title="<?php _e('Send Gift', 'lovestory'); ?>" class="icon-gift colorbox inline" data-value="<?php echo ThemexUser::$data['active_user']['ID']; ?>"></a>
            </div>
        <?php } ?>
        <div class="profile-option">
            <a href="<?php echo ThemexUser::$data['active_user']['message_url']; ?>" title="Сообщения" class="icon-envelope-alt"></a>
        </div>
        <?php if (!ThemexCore::checkOption('user_chat')) { ?>
            <div class="profile-option">
                <a href="<?php echo ThemexUser::$data['active_user']['chat_url']; ?>" title="Видео чат" data-popup="chat" class="fas fa-video"></a>
            </div>
        <?php } ?>
        <?php if (!is_user_logged_in()) { ?>
            <div class="popup hidden">
                <ul class="error">
                    <li><?php _e('You must be signed in to do that', 'lovestory'); ?></li>
                </ul>
            </div>
        <?php } else if (!ThemexUser::$data['user']['membership']['chat']) { ?>
            <div class="popup hidden" data-id="chat">
                <ul class="error">
                    <li><?php _e('Live chat is disabled in your membership', 'lovestory'); ?></li>
                </ul>
            </div>
        <?php } ?>
    </div>
</div>