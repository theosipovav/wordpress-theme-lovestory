<?php

$currentProfile = ThemexUser::$data['user']['profile'];
$genderUser = $currentProfile['gender'];
$profileActiveUser = ThemexUser::$data['active_user']['profile'];
$isWriteMe = true;


$age = $currentProfile['age'];
if (validateDate($age, "Y-m-d")) {
	$dateTime = new DateTime($age);
	$age = getAge($dateTime->format("Y"), $dateTime->format("m"), $dateTime->format("d"));
} else {
	$age = 0;
}

if ($profileActiveUser["write-me-$genderUser"] == 'off') {
	$isWriteMe = false;
}
if ($age < ThemexUser::$data['active_user']['profile']['write-me-age-pre']) {
	$isWriteMe = false;
}
if ($age > ThemexUser::$data['active_user']['profile']['write-me-age-after']) {
	$isWriteMe = false;
}
get_header(); ?>
<aside class="message-preview column threecol <?php if (!ThemexCore::checkOption('user_ignore')) { ?>unbordered<?php } ?>">
	<?php get_template_part('content', 'profile-grid'); ?>
	<?php if (!ThemexCore::checkOption('user_ignore')) { ?>
		<div class="profile-footer clearfix">
			<form action="" method="POST">
				<?php if (ThemexUser::isIgnored(ThemexUser::$data['active_user']['ID'])) { ?>
					<a href="#" class="button secondary submit-button"><?php _e('Unignore User', 'lovestory'); ?></a>
					<input type="hidden" name="user_action" value="unignore_user" />
				<?php } else { ?>
					<a href="#" class="button submit-button"><?php _e('Ignore User', 'lovestory'); ?></a>
					<input type="hidden" name="user_action" value="ignore_user" />
				<?php } ?>
				<input type="hidden" name="user_ignore" value="<?php echo ThemexUser::$data['active_user']['ID']; ?>" />
				<input type="hidden" name="nonce" value="<?php echo wp_create_nonce(THEMEX_PREFIX . 'nonce'); ?>" />
			</form>
		</div>
	<?php } ?>
</aside>
<div class="ninecol column last">
	<div class="pagination top-pagination clearfix">
		<?php ThemexInterface::renderPagination(themex_paged(), themex_pages(ThemexUser::getMessages(ThemexUser::$data['user']['ID'], get_query_var('message')), 5)); ?>
	</div>
	<!-- /pagination -->
	<ul class="bordered-list">
		<?php $messages = ThemexUser::getMessages(ThemexUser::$data['user']['ID'], get_query_var('message'), themex_paged()); ?>
		<?php
		foreach ($messages as $message) {
			$GLOBALS['comment'] = $message;
			get_template_part('content', 'message');
		}
		?>
	</ul>
	<!-- /messages -->
	<?php if ($isWriteMe) { ?>
		<div class="message-form">
			<form class="formatted-form" enctype="multipart/form-data" action="<?php echo ThemexUser::$data['active_user']['message_url']; ?>" method="POST">
				<div class="message">
					<?php ThemexInterface::renderMessages(); ?>
				</div>

				<div class="msg-editor-v2">

					<div class="msg-editor-in">
						<?php ThemexInterface::renderEditorV2('user_message'); ?>
					</div>
				</div>
				<div class="d-flex">
					<div class="flex-grow-1">
						<?php
							include(locate_template('module-smiles-select.php'));
							?>
					</div>
					<div class="d-flex flex-column" style="min-width: 150px;">
						<div class="d-flex justify-content-end align-items-start">
							<?php wp_nonce_field('user_message_photo', 'my_image_upload_nonce'); ?>
							<label id="LabelUploadPhoto" for="InputFileMsg" class="button" title="Добавить фото"><i class="fas fa-image"></i></label>
							<input type="file" hidden class="" id="InputFileMsg" name="user_message_photo">
							<a href="#" class="button submit-button" style="float: right;"><?php _e('Send Message', 'lovestory'); ?></a>
							<input type="hidden" name="user_recipient" value="<?php echo get_query_var('message'); ?>" />
							<input type="hidden" name="user_action" value="add_message" />
							<input type="hidden" name="nonce" value="<?php echo wp_create_nonce(THEMEX_PREFIX . 'nonce'); ?>" />
						</div>
						<div class="d-flex flex-column">
							<img src="" alt="" class="msg-editor-preview" id="PreviewMessagePhoto">
						</div>
					</div>
				</div>
			</form>
		</div>



	<?php } else { ?>
		<div class="message-form">
			<div class="alert alert-danger">
				<?php
					$name = '';
					if (ThemexUser::$data['active_user']['profile']['name'] != '') {
						$name = ThemexUser::$data['active_user']['profile']['name'];
					} else {
						if (ThemexUser::$data['active_user']['profile']['nickname'] != '') {
							$name = ThemexUser::$data['active_user']['profile']['nickname'];
						} else {
							$name = 'Пользователь';
						}
					}
					$writesMe = array();
					if (ThemexUser::$data['active_user']['profile']['write-me-man'] != 'off') {
						array_push($writesMe, "мужчинами");
					}
					if (ThemexUser::$data['active_user']['profile']['write-me-woman'] != 'off') {
						array_push($writesMe, "женщинами");
					}
					if (ThemexUser::$data['active_user']['profile']['write-me-para'] != 'off') {
						array_push($writesMe, "парами");
					}
					if (ThemexUser::$data['active_user']['profile']['write-me-mm'] != 'off') {
						array_push($writesMe, "парами М+М");
					}
					if (ThemexUser::$data['active_user']['profile']['write-me-zhzh'] != 'off') {
						array_push($writesMe, "парами Ж+Ж");
					}
					$writeMe = '';
					for ($i = 0; $i < count($writesMe); $i++) {
						if ($i > 0 && $i != count($writesMe) - 1) {
							$writeMe .= ", ";
						}
						if ($i != 0 && $i == count($writesMe) - 1) {
							$writeMe .= " и ";
						}
						$writeMe .= $writesMe[$i];
					}
					$date1 = ThemexUser::$data['active_user']['profile']['write-me-age-pre'];
					$date2 = ThemexUser::$data['active_user']['profile']['write-me-age-after'];

					if (ThemexUser::$data['active_user']['profile']['gender'] == 'man') {
						$textGender1 = 'указал';
						$textGender2 = 'знакомится';
					}
					if (ThemexUser::$data['active_user']['profile']['gender'] == 'woman') {
						$textGender1 = 'указала';
						$textGender2 = 'знакомится';
					}
					if (ThemexUser::$data['active_user']['profile']['gender'] == 'para') {
						$textGender1 = 'указали';
						$textGender2 = 'знакомятся';
					}
					if (ThemexUser::$data['active_user']['profile']['gender'] == 'mm') {
						$textGender1 = 'указали';
						$textGender2 = 'знакомятся';
					}
					if (ThemexUser::$data['active_user']['profile']['gender'] == 'zhzh') {
						$textGender1 = 'указали';
						$textGender2 = 'знакомятся';
					}
					$text = "$name $textGender1 в анкете, что $textGender2 только с <b>$writeMe</b>";
					if ($date1 != '' && $date2 != '') {
						$text .= " и в возрасте от <b>$date1</b> и до <b>$date2</b> лет.";
					}
					$text .= "<br>Если вы всё же хотите начать общаться, вы можете отправить Подарок и пользователь вас заметит.";
					echo $text;
					?>
			</div>
			<div class="surprise-msg">
				<img src="<?= get_template_directory_uri() ?>/assets/img/presents-001.svg" height="200" alt="" srcset="">
			</div>
		</div>
	<?php } ?>

</div>
<?php get_footer(); ?>