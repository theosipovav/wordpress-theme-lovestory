<?php
add_filter('wp_title', 'filter_function_name_8858', 10, 3);
function filter_function_name_8858($title, $sep, $seplocation)
{
	return "Профиль | Мои настройки";
}
get_header();
?>
<?php get_sidebar('profile-left'); ?>
<div class="full-profile fivecol column">
	<div class="section-title">
		<h2>Мои Настройки</h2>
	</div>
	<form class="formatted-form" action="" method="POST">
		<div class="message">
			<?php ThemexInterface::renderMessages(isset($_POST['success']) ? $_POST['success'] : false); ?>
		</div>
		<table class="profile-fields">
			<tbody>
				<tr>
					<th class="form-control-title" colspan="2">Симпатии</th>
				</tr>
				<tr>
					<th>Избранное (видимость):</th>
					<td>
						<div class="select-field">
							<span></span>
							<?php
							echo ThemexInterface::renderOption(array(
								'id' => 'user_favorites',
								'type' => 'select',
								'value' => ThemexUser::$data['user']['settings']['favorites'],
								'wrap' => false,
								'options' => array(
									'1' => __('Everybody', 'lovestory'),
									'2' => __('Favorites', 'lovestory'),
									'3' => __('Nobody', 'lovestory'),
								),
							));
							?>
						</div>
					</td>
				</tr>
				<tr>
					<th class="form-control-title" colspan="2">Фото</th>
				</tr>
				<tr>
					<th>Фотографии (видимость):</th>
					<td>
						<div class="select-field">
							<span></span>
							<?php
							echo ThemexInterface::renderOption(array(
								'id' => 'user_photos',
								'type' => 'select',
								'value' => ThemexUser::$data['user']['settings']['photos'],
								'wrap' => false,
								'options' => array(
									'1' => __('Everybody', 'lovestory'),
									'2' => __('Favorites', 'lovestory'),
									'3' => __('Nobody', 'lovestory'),
								),
							));
							?>
						</div>
					</td>
				</tr>
				<tr>
					<th>
						<label for="passwordPrivatePhoto">Пароль для приватных фото</label>
					</th>
					<td>
						<div class="field-wrap">
							<input type="password" name="user_private_photos" id="passwordPrivatePhoto" value="<?= ThemexUser::$data['user']['settings']['private-photos'] ?>">
						</div>
					</td>
				</tr>
				<tr>
					<th class="form-control-title" colspan="2">Подарки</th>
				</tr>
				<?php if (!ThemexCore::checkOption('user_gifts')) { ?>
					<tr>
						<th>Подарки (видимость):</th>
						<td>
							<div class="select-field">
								<span></span>
								<?php
								echo ThemexInterface::renderOption(array(
									'id' => 'user_gifts',
									'type' => 'select',
									'value' => ThemexUser::$data['user']['settings']['gifts'],
									'wrap' => false,
									'options' => array(
										'1' => __('Everybody', 'lovestory'),
										'2' => __('Favorites', 'lovestory'),
										'3' => __('Nobody', 'lovestory'),
									),
								));
								?>
							</div>
						</td>
					</tr>
				<?php } ?>
				<?php if (!ThemexCore::checkOption('user_notice')) { ?>
					<tr>
						<th class="form-control-title" colspan="2">Уведомления</th>
					</tr>
					<tr>
						<th>Уведомления на email:</th>
						<td>
							<div class="select-field">
								<span></span>
								<?php
								$args = array(
									'id' => 'user_notices',
									'type' => 'select',
									'value' => ThemexUser::$data['user']['settings']['notices'],
									'wrap' => false,
									'options' => array(
										'2' => __('Messages', 'lovestory'),
										'4' => __('None', 'lovestory'),
									),
								);

								if (!ThemexCore::checkOption('user_gifts')) {
									$args['options']['1'] = __('Messages and Gifts', 'lovestory');
									$args['options']['3'] = __('Gifts', 'lovestory');
									ksort($args['options']);
								}

								echo ThemexInterface::renderOption($args);
								?>
							</div>
						</td>
					</tr>
				<?php } ?>
				<tr>
					<th class="form-control-title" colspan="2">Безопасность</th>
				</tr>
				<tr>
					<th><?php _e('Email Address', 'lovestory'); ?></th>
					<td>
						<div class="field-wrap">
							<input type="text" name="user_email" value="<?php echo ThemexUser::$data['user']['email']; ?>" />
						</div>
					</td>
				</tr>
				<tr>
					<th><?php _e('New Password', 'lovestory'); ?></th>
					<td>
						<div class="field-wrap">
							<input type="password" name="user_password_new" size="50" />
						</div>
					</td>
				</tr>
				<tr>
					<th><?php _e('Confirm Password', 'lovestory'); ?></th>
					<td>
						<div class="field-wrap">
							<input type="password" name="user_password_confirm" size="50" />
						</div>
					</td>
				</tr>
				<tr>
					<th><?php _e('Current Password', 'lovestory'); ?></th>
					<td>
						<div class="field-wrap">
							<input type="password" name="user_password" size="50" />
						</div>
					</td>
				</tr>
			</tbody>
		</table>
		<a href="#" class="button submit-button"><?php _e('Save Changes', 'lovestory'); ?></a>
		<input type="hidden" name="user_action" value="update_settings" />
		<input type="hidden" name="nonce" value="<?php echo wp_create_nonce(THEMEX_PREFIX . 'nonce'); ?>" />
	</form>
</div>
<?php get_sidebar('profile-right'); ?>
<?php get_footer(); ?>