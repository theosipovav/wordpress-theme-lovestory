<?php

function ajaxPasswordPrivatePhoto()
{
    $res = [];
    $res['status'] = 0;
    $res['data'] = 0;
    $userId = $_GET['user'];
    $password = $_GET['password'];
    $setting = ThemexUser::getSettings($userId);
    if ($setting['private-photos'] == $password) {
        $res['status'] = 1;
    } else {
        $res['status'] = 0;
        $res['data'] = 'Указан неверный пароль';
    }

    print_r(json_encode($res));
    die;
}
add_action('wp_ajax_actionPasswordPrivatePhoto', 'ajaxPasswordPrivatePhoto');



/**
 * Вывести список всех чатов
 */
function ajaxWidgetMessageAll()
{
    $res = "";
    $userId = $_GET['id'];
    ob_start();
    $LiveChat = new ThemexLiveChat();
    $LiveChat->UserCurrent = ThemexUser::getUser($userId);
    $LiveChat->recipients = ThemexUser::getRecipients($userId);
    foreach ($LiveChat->recipients as $recipient) {
        $LiveChat->UserActive = ThemexUser::getUser($recipient['ID']);
        include "widget-message-all--item.php";
        $LiveChat->UserActive = null;
    }
    $res = ob_get_contents();
    ob_end_clean();
    echo $res;
    die;
}
add_action('wp_ajax_widget_message_all', 'ajaxWidgetMessageAll');




/**
 * Сгенерировать приватный чат для пользователя
 */
function ajaxWidgetMessagePrivateHeader()
{
    $res = "";
    $userId = $_GET['id'];
    ob_start();
    $LiveChat = new ThemexLiveChat();
    $LiveChat->UserCurrent = ThemexUser::getUser($_GET['id_user']);
    $LiveChat->UserActive = ThemexUser::getUser($_GET['id_user_active']);
    include 'widget-message-private--content-header.php';
    $res = ob_get_contents();
    ob_end_clean();
    echo $res;
    die;
}
add_action('wp_ajax_widget_message_private__header', 'ajaxWidgetMessagePrivateHeader');

/**
 * Сгенерировать приватный чат для пользователя
 */
function ajaxWidgetMessagePrivateContent()
{
    $res = "";
    $userId = $_GET['id'];
    ob_start();
    $LiveChat = new ThemexLiveChat();
    $LiveChat->UserCurrent = ThemexUser::getUser($_GET['id_user']);
    $LiveChat->UserActive = ThemexUser::getUser($_GET['id_user_active']);
    $LiveChat->messages = ThemexUser::getMessages_v2($LiveChat->UserCurrent['ID'], $LiveChat->UserActive["ID"], themex_paged());
    include 'widget-message-private--content-list.php';
    $res = ob_get_contents();
    ob_end_clean();
    echo $res;
    die;
}
add_action('wp_ajax_widget_message_private__content', 'ajaxWidgetMessagePrivateContent');

/**
 * Сгенерировать приватный чат для пользователя
 */
function ajaxWidgetMessagePrivateForm()
{
    $res = "";
    if (isset($_GET['id_user']) && isset($_GET['id_user_active'])) {
        ob_start();
        $LiveChat = new ThemexLiveChat();
        $LiveChat->UserCurrent = ThemexUser::getUser($_GET['id_user']);
        $LiveChat->UserActive = ThemexUser::getUser($_GET['id_user_active']);
        include 'widget-message-private--content-form.php';
        $res = ob_get_contents();
        ob_end_clean();
    }

    echo $res;
    die;
}
add_action('wp_ajax_widget_message_private__form', 'ajaxWidgetMessagePrivateForm');




function ajaxWidgetMessageSend()
{
    $idUser = $_POST['user'];
    $idActiveUser = $_POST['active_user'];
    $message = $_POST['user_message'];
    $idLastMessage = $_POST["last-message-id"];
    ThemexUser::$data["user"] = ThemexUser::getUser($idUser, false);
    ThemexUser::$data["user"]['membership']['messages'] = 1;
    ThemexUser::$data["active_user"] = ThemexUser::getUser($idActiveUser, false);
    ThemexUser::messengerAddMessage($idUser, $idActiveUser, $message);
    $availableMessages = ThemexUser::getAvailableMessages($idUser);
    $res = array();
    $res["available_messages"] = intval($availableMessages);
    $res['html_alert'] = "";
    if ($res["available_messages"] <= 0) {
        ob_start();
        include 'widget-message-private--alert-danger.php';
        $html = ob_get_contents();
        ob_end_clean();
        $res['html_alert'] = $html;
    }
    echo json_encode($res);
    die();
}
add_action('wp_ajax_widget_message_send', 'ajaxWidgetMessageSend');


function  ajaxWidgetMessageSendImage()
{
    $idUser = $_POST['user'];
    $idActiveUser = $_POST['active_user'];
    $idLastMessage = $_POST["last-message-id"];
    ThemexUser::$data["user"] = ThemexUser::getUser($idUser, false);
    ThemexUser::$data["user"]['membership']['messages'] = 1;
    ThemexUser::$data["active_user"] = ThemexUser::getUser($idActiveUser, false);
    $attachment_id = -1;
    if (wp_verify_nonce($_POST['my_image_upload_nonce'], 'user_message_photo') && ($_FILES["user_message_photo"]["size"] != 0)) {
        require_once(ABSPATH . 'wp-admin/includes/image.php');
        require_once(ABSPATH . 'wp-admin/includes/file.php');
        require_once(ABSPATH . 'wp-admin/includes/media.php');
        $attachment_id = media_handle_upload('user_message_photo', 0);
        if (is_wp_error($attachment_id)) {
            ThemexInterface::$messages[] = "Ошибка загрузки медиафайла.";
            $attachment_id = -1;
            return;
        }
    } else {
    }
    if ($attachment_id != -1) {
        $photoHtml = wp_get_attachment_image($attachment_id, 'thumbnail');
        $photoUrl = wp_get_attachment_image_url($attachment_id, 'full');
        $message = "<a href='$photoUrl' target='_blank'>$photoHtml</a>";
    } else {
        $message = "";
    }
    ThemexUser::messengerAddMessage($idUser, $idActiveUser, $message, false);
    $messages = ThemexUser::messengerUpdate($idUser, $idActiveUser, $idLastMessage);
    echo json_encode($messages);
    die();
}
add_action('wp_ajax_widget_message_send_image', 'ajaxWidgetMessageSendImage');




//
// 
// Для авторизованных пользователей
// Для не авторизованных пользователей
//
function ajaxWidgetMessageCheck()
{
    $userId = $_GET['id'];
    $res = intval(ThemexUser::countMessages($userId));
    print_r($res);
    die;
}
add_action('wp_ajax_widget_message_check', 'ajaxWidgetMessageCheck');




function ajaxWidgetTopPhoto()
{
    $idLast = $_GET['id'];
    $idUser = $_GET['id_user'];
    $slides = ThemexTopPhoto::getSlidesForUpdate($idLast, $idUser);
    print_r(json_encode($slides));
    die;
}
add_action('wp_ajax_widget_update_top_photo', 'ajaxWidgetTopPhoto');
add_action('wp_ajax_nopriv_widget_update_top_photo', 'ajaxWidgetTopPhoto');
