<div class="widget clearfix">
    <h4 class="widget-title clearfix">
        <span class="left">Мои гости</span>
        <span class="widget-options"></span>
    </h4>
    <?php if (empty(ThemexUser::$data['user']['guests'])) { ?>
        <span class="secondary">Посетителей не было</span>
    <?php } else { ?>
        <div class="themex-slider carousel-slider">
            <ul>
                <?php
                $current = ThemexUser::isProfile();
                $counter = 0;

                foreach (explode(',', ThemexUser::$data['active_user']['guests']) as $guestId) {
                    if ($guestId == '') continue;
                    ThemexUser::$data['active_user'] = ThemexUser::getUser($guestId);

                    if (ThemexUser::$data['active_user']['role'] == 'administrator') {
                        continue;
                    }


                    $counter++;
                    if ($counter == 1) {
                ?>
                        <li class="clearfix">
                        <?php } ?>
                        <div class="fourcol static-column <?php if ($counter == 3) { ?>last<?php } ?>">
                            <div class="profile-preview widget-profile">
                                <div class="profile-image">
                                    <a href="<?php echo ThemexUser::$data['active_user']['profile_url']; ?>" title="<?php echo ThemexUser::$data['active_user']['profile']['full_name']; ?>">
                                        <?php echo get_avatar($guestId, 100); ?>
                                    </a>
                                </div>
                                <?php if ($current) { ?>
                                    <div class="profile-options clearfix">
                                        <div class="profile-option">
                                            <?php get_template_part('module', 'status'); ?>
                                        </div>

                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php
                        if ($counter == 3) {
                            $counter = 0;
                        ?>
                        </li>
                    <?php
                        }
                    }

                    ThemexUser::refresh();
                    if ($counter !== 0) {
                    ?>
                    </li>
                <?php } ?>
            </ul>
        </div>
    <?php } ?>
</div>