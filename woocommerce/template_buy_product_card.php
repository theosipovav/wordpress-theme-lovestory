<?php

/**
 * $data['active']
 */

?>
<div class="buy-product-card<?php if ($data['active'] == 1) echo ' active'; ?>">
    <?php if ($data['url'] != '' && $data['url'] != "#") { ?><a href="<?= $data['url'] ?>" class="buy-product-card-link-hover"></a><?php } ?>
    <?php if ($data['popular'] == 1) { ?> <div class="buy-product-card-promo">Популярно</div> <?php } ?>
    <div class="buy-product-card-inner">
        <div class="buy-product-card-img">
            <img src="<?= $data['img'] ?>" alt="<?= $data['name'] ?>">
        </div>
        <div class="buy-product-card-name">
            <h2><?= $data['name'] ?></h2>
        </div>
        <div class="buy-product-card-price<?php if ($data['price_sale'] != '') echo ' buy-product-card-price-old'; ?>">
            <span><?= $data['price_regular'] ?></span><span><?= get_woocommerce_currency_symbol() ?></span>
        </div>
        <?php
        if ($data['price_sale'] != '') { ?>
            <div class="buy-product-card-price-sale">
                <span><?= $data['price_sale'] ?></span><span><?= get_woocommerce_currency_symbol() ?></span>
            </div>
        <?php } ?>
    </div>
</div>