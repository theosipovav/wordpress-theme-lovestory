<?php
defined('ABSPATH') || exit;
global $product;

$idCategory = $product->get_category_ids()[0];
$term = get_term_by('id', $idCategory, 'product_cat');

if ($term->slug == 'service') {
    include(locate_template('template/template-wc-cat-service.php'));
}
if ($term->slug == 'coins') {
    include(locate_template('template/template-wc-cat-coins.php'));
}
