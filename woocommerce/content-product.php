<?php


defined('ABSPATH') || exit;

global $product;

if (empty($product) || !$product->is_visible()) {
	return;
}


$productCurrencyCat = $product;
$idProductCurrencyCat = $product->id;
$data = [];
$data['id'] =  $idProductCurrencyCat;
$data['price'] =  $productCurrencyCat->get_price();
$data['price_regular'] =  $productCurrencyCat->get_regular_price();
$data['price_sale'] = $productCurrencyCat->get_sale_price();
$data['name'] = $productCurrencyCat->get_name();
$idImage = $productCurrencyCat->get_image_id();
$urlImage = wp_get_attachment_image_url($idImage, 'full');
$data['img'] = wp_get_attachment_image_url($idImage, 'full');
$data['active'] = 0;
$isPopular = 0;
$terms = get_the_terms(get_the_ID(), 'product_tag');
if (is_array($terms)) {
	foreach ($terms as $t) {
		if ($t->slug == 'popular') $isPopular = 1;
	}
}
$isPopular = 0;
$terms = get_the_terms($idProductCurrencyCat, 'product_tag');
if (is_array($terms)) {
	foreach ($terms as $t) {
		if ($t->slug == 'popular') $isPopular = 1;
	}
}
$data['popular'] = $isPopular;
$data['url'] = get_permalink($idProductCurrencyCat);
if (is_product_tag('top-photo')) {
	include(locate_template('template/template-wc-tag-top-photo.php'));
} else {
	include(locate_template('woocommerce/template_buy_product_card.php'));
}
