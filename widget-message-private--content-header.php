<div class="widget-message-content-back">
    <button class="widget-message-private--btn-back" onclick="clickWidgetMessagePrivateBack(<?= $LiveChat->UserCurrent['ID'] ?>)">
        <i class="fas fa-chevron-left"></i>
    </button>
</div>
<div class="widget-message-private-header-avatar">
    <?php if (isset($LiveChat->UserActive)) : ?>
        <img src="<?= ThemexUser::getUrlAvatar($LiveChat->UserActive['ID']) ?>" alt="<?= $LiveChat->UserActive['profile']['full_name']; ?>">
    <?php endif ?>
</div>
<div class="widget-message-private-header-status">
    <?php if (isset($LiveChat->UserActive)) : ?>
        <span title="<?= $LiveChat->UserActive['status']['name']; ?>" class="profile-status <?= $LiveChat->UserActive['status']['value']; ?>"></span>
    <?php endif ?>
</div>
<?php if (isset($LiveChat->UserActive)) : ?>
    <div class="widget-message-private-header-name"><?= $LiveChat->UserActive['profile']['full_name']; ?></div>
<?php endif ?>
<input type="hidden" id="InputWidgetMessageUserActiveId" value="<?= $LiveChat->UserActive["ID"]; ?>">