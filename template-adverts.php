<?php
/*
Template Name: Adverts
Template Post Type: post, page, product
*/
?>
<?php get_header(); ?>
<div class="d-flex flex-column ">
    <div class="btn-panel-right mb-3">
        <a href="/adverts/add/" class="btn btn-primary m-1"><i class="fas fa-plus-circle mr-2"></i> Добавить объявление</a>
        <a href="/adverts/manage/" class="btn btn-outline-primary m-1"><i class="fas fa-edit mr-2"></i> Редактировать объявления</a>
    </div>
    <div class="">
        <?php while (have_posts()) : the_post(); ?>
            <?php the_content(); ?>
        <?php endwhile; // End of the loop. 
        ?>
    </div>
</div>
<?php get_footer(); ?>