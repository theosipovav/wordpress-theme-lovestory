<?php

/** Создание полей
 * Блок с классом options_group позволяет визуально разделять группы полей с помощью нижней границы.
 */


function mytheme_add_woocommerce_support()
{
    add_theme_support('woocommerce');
}
add_action('after_setup_theme', 'mytheme_add_woocommerce_support');


add_action('woocommerce_product_options_general_product_data', 'art_woo_add_custom_fields');
function art_woo_add_custom_fields()
{
    global $product, $post;
    // Группировка полей
    echo '<div class="options_group">';
    // Количество
    woocommerce_wp_text_input(array(
        'id'                => '_count_field',
        'label'             => __('Количество', 'woocommerce'),
        'placeholder'       => 'Ввод чисел',
        'description'       => __('Вводятся только числа от 0 до 999999', 'woocommerce'),
        'type'              => 'number',
        'custom_attributes' => array(
            'step' => 'any',
            'min'  => '0',
            'max' => '999999',
        ),
    ));
    echo '</div>';
}
/** Сохранение полей
 * Для сохранения полей используем хук woocommerce_process_product_meta.
 * Выдергиваем нужное значение из переменной $_POST,
 * если там что-то есть то записываем в поле.
 */
add_action('woocommerce_process_product_meta', 'art_woo_custom_fields_save', 10);
function art_woo_custom_fields_save($post_id)
{
    // Сохранение числового поля
    $field = $_POST['_count_field'];

    if (!empty($field)) {
        update_post_meta($post_id, '_count_field', esc_attr($field));
    }
}


add_filter('woocommerce_checkout_fields', 'custom_override_checkout_fields');

function custom_override_checkout_fields($fields)
{
    $idUser = get_current_user_id();
    $nameUser = get_current_user();
    $userdate = get_userdata($idUser);
    $fields['billing']['billing_first_name']['default'] = $nameUser;
    $fields['billing']['billing_email']['default'] = $userdate->user_email;




    unset($fields['billing']['billing_last_name']);
    unset($fields['billing']['billing_city']);
    return $fields;
}
add_filter('woocommerce_checkout_fields', 'custom_override_checkout_fields');
