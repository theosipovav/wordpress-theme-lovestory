<div class="buy-currency-product">
    <div class="buy-product-logo">
        <a href="<?php echo SITE_URL; ?>" rel="home">
            <img src="<?php echo ThemexCore::getOption('logo', THEME_URI . 'images/logo.png'); ?>" alt="<?php bloginfo('name'); ?>" />
        </a>
    </div>
    <?php
    $data = [];
    $data["id"] = $product->id;
    $data['price'] =  $product->get_price();
    $data['price_regular'] =  $product->get_regular_price();
    $data['price_sale'] = $product->get_sale_price();
    $data['name'] = $product->get_name();
    $data['desc'] = $product->get_description();
    $idImage = $product->get_image_id();
    $urlImage = wp_get_attachment_image_url($idImage, 'full');
    $data['img'] = wp_get_attachment_image_url($idImage, 'full');
    $data['active'] = 1;
    $isPopular = 0;
    $terms = get_the_terms($product->ID, 'product_tag');
    if (is_array($terms)) {
        foreach ($terms as $t) {
            if ($t->slug == 'popular') $isPopular = 1;
        }
    }
    $data['popular'] = $isPopular;
    $data['url'] = '#';
    include(locate_template('template/template-wc-tag-top-photo.php'));
    ?>
</div>
<div class="buy-currency-cat mt-3">
    <?php
    $query = new WP_Query([
        'post_type' => 'product',
        'hierarchical' => 0,
        'order' => 'ASC',
        'product_cat' => 'service',
    ]);
    $items = [];
    if ($query->have_posts()) {
        while ($query->have_posts()) {
            $query->next_post();
            $idProductCurrencyCat = $query->post->ID;
            $productCurrencyCat = new WC_Product($idProductCurrencyCat);
            $data = [];
            $data["id"] = $product->id;
            $data['price'] =  $productCurrencyCat->get_price();
            $data['price_regular'] =  $productCurrencyCat->get_regular_price();
            $data['price_sale'] = $productCurrencyCat->get_sale_price();
            $data['name'] = $productCurrencyCat->get_name();

            //$data['desc'] = $productCurrencyCat->get_description();

            $idImage = $productCurrencyCat->get_image_id();
            $urlImage = wp_get_attachment_image_url($idImage, 'full');
            $data['img'] = wp_get_attachment_image_url($idImage, 'full');
            $data['active'] = 0;
            $isPopular = 0;
            $terms = get_the_terms(get_the_ID(), 'product_tag');
            if (is_array($terms)) {
                foreach ($terms as $t) {
                    if ($t->slug == 'popular') $isPopular = 1;
                }
            }
            $isPopular = 0;
            $terms = get_the_terms($idProductCurrencyCat, 'product_tag');
            if (is_array($terms)) {
                foreach ($terms as $t) {
                    if ($t->slug == 'popular') $isPopular = 1;
                }
            }
            $data['popular'] = $isPopular;
            $data['url'] = get_permalink($idProductCurrencyCat);
            include(locate_template('template/template-wc-tag-top-photo.php'));
        }
    }

    ?>
</div>