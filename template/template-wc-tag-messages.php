<?php
$guid = GUID();
?>

<div class="buy-service-product-card">
    <div class="buy-card-img">
        <img src="<?= $data['img'] ?>" alt="<?= $data['name'] ?>">
    </div>
    <div class="buy-card-desc">
        <h2><?= $data['name'] ?></h2>
        <p>
            <?= $data['desc'] ?>
        </p>
    </div>
    <div class="buy-card-payment">
        <form id="Form<?= $guid ?>" action="/" method="POST" class="">
            <input type="hidden" name="user_recipient" value="<?= get_query_var('message'); ?>" />
            <input type="hidden" name="nonce" value="<?= wp_create_nonce(THEMEX_PREFIX . 'nonce'); ?>" />
            <input type="hidden" name="user" value="<?= ThemexUser::$data['user']['ID'] ?>" />
            <input type="hidden" name="product" value="<?= $data["id"] ?>" />
            <input type="hidden" name="user_action" value="service_payment" />
            <input type="hidden" name="product_tag" value="messages">
        </form>
        <button type="submit" form="Form<?= $guid ?>" class="button buy-card-btn">Купить</button>
        <div class="buy-product-card-price<?php if ($data['price_sale'] != '') echo ' buy-product-card-price-old'; ?>">
            <span><?= $data['price_regular'] ?></span> лафчиков
        </div>
        <?php
        if ($data['price_sale'] != '') { ?>
            <div class="buy-product-card-price-sale">
                <span><?= $data['price_sale'] ?></span> лафчиков
            </div>
        <?php } ?>
    </div>
</div>