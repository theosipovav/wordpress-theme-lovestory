<aside class="sidebar fourcol column last">
	<?php
	if (ThemexUser::checkAccess(ThemexUser::$data['user']['ID'], ThemexUser::$data['active_user']['ID'], 'favorites')) {
		// Избранное
		get_template_part('module', 'favorites');
	}
	if (ThemexUser::checkAccess(ThemexUser::$data['user']['ID'], ThemexUser::$data['active_user']['ID'], 'photos')) {
		// Фотографии
		get_template_part('module', 'photos');
		// Приватные фото
		get_template_part('module', 'private-photos');
	}
	if (ThemexUser::checkAccess(ThemexUser::$data['user']['ID'], ThemexUser::$data['active_user']['ID'], 'gifts') && !ThemexCore::checkOption('user_gifts')) {
		// Подарки
		get_template_part('module', 'gifts');
	}
	if (ThemexUser::$data['user']["ID"] == ThemexUser::$data['active_user']["ID"]) {
		// Мои гости
		get_template_part('module', 'guests');
	}
	if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('profile_right'));
	?>
</aside>