<?php


$currentProfile = ThemexUser::$data['user']['profile'];
$genderUser = $currentProfile['gender'];
$profileActiveUser = ThemexUser::$data['active_user']['profile'];
$isWriteMe = true;



$age = $currentProfile['age'];
if (validateDate($age, "Y-m-d")) {
    $dateTime = new DateTime($age);
    $age = getAge($dateTime->format("Y"), $dateTime->format("m"), $dateTime->format("d"));
} else {
    $age = 0;
}

if ($profileActiveUser["write-me-$genderUser"] == 'off') {
    $isWriteMe = false;
}
if ($age < ThemexUser::$data['active_user']['profile']['write-me-age-pre']) {
    $isWriteMe = false;
}
if ($age > ThemexUser::$data['active_user']['profile']['write-me-age-after']) {
    $isWriteMe = false;
}
add_filter('wp_title', 'filter_function_name_8858', 10, 3);
function filter_function_name_8858($title, $sep, $seplocation)
{
    return "Чат";
}
get_header(); ?>
<aside class="message-preview column threecol <?php if (!ThemexCore::checkOption('user_ignore')) { ?>unbordered<?php } ?>">
    <?php get_template_part('content', 'profile-grid'); ?>
    <?php if (!ThemexCore::checkOption('user_ignore')) { ?>
        <div class="profile-footer clearfix">
            <form action="" method="POST">
                <?php if (ThemexUser::isIgnored(ThemexUser::$data['active_user']['ID'])) { ?>
                    <a href="#" class="button secondary submit-button"><?php _e('Unignore User', 'lovestory'); ?></a>
                    <input type="hidden" name="user_action" value="unignore_user" />
                <?php } else { ?>
                    <a href="#" class="button submit-button"><?php _e('Ignore User', 'lovestory'); ?></a>
                    <input type="hidden" name="user_action" value="ignore_user" />
                <?php } ?>
                <input type="hidden" name="user_ignore" value="<?php echo ThemexUser::$data['active_user']['ID']; ?>" />
                <input type="hidden" name="nonce" value="<?php echo wp_create_nonce(THEMEX_PREFIX . 'nonce'); ?>" />
            </form>
        </div>
    <?php } ?>
</aside>
<div class="ninecol column last">
    <ul class="bordered-list-message">
        <?php $messages = ThemexUser::getMessages_v2(ThemexUser::$data['user']['ID'], get_query_var('message'), themex_paged()); ?>
        <?php
        $lastMessageId = -1;
        foreach ($messages as $message) {
            $lastMessageId = $message->comment_ID;
            $GLOBALS['comment'] = $message;
            if ($message->user_id == ThemexUser::$data['user']['ID']) {
                get_template_part('content', 'message-me');
            } else {
                get_template_part('content', 'message');
            }
        }
        ?>
    </ul>



    <!-- /messages -->
    <?php if ($isWriteMe) { ?>
        <div class="message-form">
            <form action="<?php echo AJAX_URL; ?>" method="POST" id="FormMessenger" class="formatted-form form-messages-v2" data-user-name="<?= ThemexUser::$data['user']['login'] ?>" data-user-id="<?= ThemexUser::$data['user']['ID'] ?>" data-active-user-name="<?= ThemexUser::$data['active_user']['login'] ?>" data-active-user-id="<?= ThemexUser::$data['active_user']['ID'] ?>">
                <div class="message">
                    <?php ThemexInterface::renderMessages(); ?>
                </div>
                <?php if (ThemexUser::$data['user']['membership']['messages'] > 0) { ?>
                    <div class="msg-editor-v2">
                        <div class="msg-editor-in">
                            <?php ThemexInterface::renderEditorV2('user_message'); ?>
                        </div>
                    </div>
                    <div class="d-flex">
                        <div class="flex-grow-1">
                            <?php
                            include(locate_template('module-smiles-select.php'));
                            ?>
                        </div>
                        <div class="d-flex flex-column" style="min-width: 150px;">
                            <div class="d-flex justify-content-end align-items-start">
                                <div>
                                    <label id="LabelUploadPhoto" for="InputFileMsg" class="button messenger-send-image" title="Добавить фото"><i class="fas fa-image"></i></label>
                                    <input type="file" hidden class="" id="InputFileMsg" name="user_message_photo">
                                    <input type="hidden" class="action-messenger-send-image" value="themex_messenger_send_image" />
                                    <?php wp_nonce_field('user_message_photo', 'my_image_upload_nonce'); ?>
                                </div>
                                <input type="submit" class="button messenger-send" value="Отправить">
                                <input type="hidden" name="user_recipient" value="<?php echo get_query_var('message'); ?>" />
                                <input type="hidden" name="nonce" value="<?php echo wp_create_nonce(THEMEX_PREFIX . 'nonce'); ?>" />
                                <input type="hidden" class="action-messenger-send-message" value="themex_messenger_send_message" />
                                <input type="hidden" name="user" value="<?= ThemexUser::$data['user']['ID'] ?>" />
                                <input type="hidden" name="active_user" value="<?= ThemexUser::$data['active_user']['ID'] ?>" />
                                <input type="hidden" name="last-message-id" value="<?= $lastMessageId ?>">
                            </div>
                            <?php
                            /*

                            <div class="d-flex flex-column">
                                <img src="" alt="" class="msg-editor-preview" id="PreviewMessagePhoto">
                            </div>
                        */

                            ?>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="alert alert-danger">
                        <p>Вы превысили количество сообщений</p>
                    </div>
                <?php } ?>
            </form>
        </div>
    <?php } else { ?>
        <div class="message-form">
            <div class="alert alert-danger">
                <?= ThemexInterface::renderMessageWritesMeForbidden(ThemexUser::$data['active_user']['ID']) ?>
            </div>
            <div class="surprise-msg">
                <img src="<?= get_template_directory_uri() ?>/assets/img/presents-001.svg" height="200" alt="" srcset="">
            </div>
        </div>
    <?php } ?>

</div>
<?php get_footer(); ?>