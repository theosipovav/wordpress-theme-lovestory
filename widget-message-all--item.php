<li class="widget-message-all-item" onclick="clickWidgetMessageAllItem('<?= $LiveChat->UserCurrent['ID']; ?>','<?= $LiveChat->UserActive['ID']; ?>')">
    <div class="widget-message-all-avatar">
        <img src="<?= ThemexUser::getUrlAvatar($LiveChat->UserActive['ID']) ?>" alt="">
    </div>
    <div class="widget-message-all-status">
        <span title="<?= $LiveChat->UserActive['status']['name']; ?>" class="profile-status <?= $LiveChat->UserActive['status']['value']; ?>"></span>
    </div>
    <div class="widget-message-all-name"><?= $LiveChat->UserActive['profile']['full_name']; ?></div>
    <div class="widget-message-all-count">

        <?php if ($recipient['unread'] > 0) { ?>
            <span>Сообщения: <?= $recipient['unread']; ?></span>
        <?php } ?>
    </div>

</li>