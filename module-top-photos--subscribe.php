<div class="slide slide-add-subscribe">
    <a href="#modal-payment" class="tooltip slide-img" data-tooltip-content="#tooltip_content_add">
        <?= get_avatar(get_current_user_id(), 175); ?>
        <div class="slide-add-subscribe--pre"></div>
    </a>
    <div class="tooltip_templates">
        <div id="tooltip_content_add" class="tooltip_content slide-info">
            <div class="slide-add-subscribe--title">Опубликуйте фото!</div>
            <div class="slide-add-subscribe--after">Вас увидят все пользователи, Вашего региона!</div>
        </div>
    </div>
</div>