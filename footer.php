</section>
<?php
if (is_user_logged_in()) {
	include_once 'widget-message.php';
}
?>
<!-- /site content -->
<footer class="site-footer container">
	<div class="site-copyright static-column">
		<?= ThemexCore::getOption('copyright', 'CommonLove &copy; ' . date('Y')); ?>
	</div>
	<nav class="footer-menu static-column">
		<?php wp_nav_menu(array('theme_location' => 'footer_menu')); ?>
	</nav>
</footer>
<!-- /footer -->
</div>
</div>
<!-- /wrap -->
<?php if (is_user_logged_in()) { ?>
	<form class="ajax-form status-form hidden" action="<?php echo AJAX_URL; ?>" method="POST">
		<div class="message popup"></div>
		<input type="hidden" name="user_action" value="update_status" />
		<input type="hidden" class="nonce" value="<?php echo wp_create_nonce(THEMEX_PREFIX . 'nonce'); ?>" />
		<input type="hidden" class="action" value="<?php echo THEMEX_PREFIX; ?>update_user" />
	</form>
<?php } ?>
<script type="text/javascript" src="<?= get_template_directory_uri() ?>/assets/js/payment.js"></script>
<script type="text/javascript" src="<?= get_template_directory_uri() ?>/assets/js/tooltipster.bundle.min.js"></script>
<script type="text/javascript" src="<?= get_template_directory_uri() ?>/assets/js/js.stripe.com-v2.js"></script>
<script type="text/javascript" src="<?= get_template_directory_uri() ?>/assets/js/remodal.min.js"></script>
<script type="text/javascript" src="<?= get_template_directory_uri() ?>/assets/js/script.js"></script>
<script type="text/javascript" src="<?= get_template_directory_uri() ?>/assets/lib/jquery-ui-1.11.4/external/jquery/jquery.js"></script>
<script type="text/javascript" src="<?= get_template_directory_uri() ?>/assets/lib/jquery-ui-1.11.4/jquery-ui.js"></script>
<?php
wp_enqueue_script('widget-message', get_template_directory_uri() . '/assets/js/widget-message.js', [], false, true);
?>
<?php wp_footer(); ?>
<div class="body-overflow"></div>
<?php if (false) : ?>

	<div class="d-flex">
		<div class="flex-grow-1">
			<?php
			d(ThemexUser::getSettings(ThemexUser::$data['user']["ID"]));
			?>
			<hr>
			<?php
			d(ThemexUser::getUser(ThemexUser::$data['user']["ID"], true));
			?>
		</div>
		<div class="flex-grow-1">
			<?php
			$id = ThemexUser::$data['active_user']["ID"];
			d(ThemexUser::getSettings($id));
			?>
			<hr>
			<?php
			d(ThemexUser::$data['active_user']['profile']);
			?>
		</div>
	</div>
<?php endif ?>


</body>

</html>