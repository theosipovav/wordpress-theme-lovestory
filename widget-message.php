<?php
$LiveChat = new ThemexLiveChat();
$LiveChat->UserCurrent = ThemexUser::$data['user'];

?>


<div class="widget-message widget-message-minimize">
    <div class="widget-message-header">

        <div class="widget-message-switch">

            <?php if (intval(ThemexUser::countMessages($LiveChat->UserCurrent["ID"])) == 0) : ?>

                <span id="textWidgetMessageSwitch">Сообщения</span>

            <?php else : ?>
                <span id="textWidgetMessageSwitch" class="widget-message-switch-new-message">Новые сообщения</span>
            <?php endif ?>
        </div>
        <div class="widget-message-top-controls">
            <a id="ButtonWidgetOpenMessagesUrl" href="<?= ThemexUser::$data['user']['messages_url']; ?>" class="widget-message-btn" title="Мои сообщения">
                <i class="fas fa-external-link-alt"></i>
            </a>
            <button id="ButtonWidgetRefresh" class="widget-message-btn" onclick="clickWidgetMessageRefresh();">
                <i class="fas fa-sync"></i>
            </button>
            <button class="widget-message-btn widget-message--minimize">
                <i class="far fa-window-minimize"></i>
            </button>
            <button class="widget-message-btn widget-message--maximize show">
                <i class="far fa-window-maximize"></i>
            </button>
        </div>
    </div>
    <div class="widget-message-content">
        <?php include_once 'widget-message-all.php' ?>
        <?php include_once 'widget-message-private.php' ?>
    </div>
</div>