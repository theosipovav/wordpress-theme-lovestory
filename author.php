<?php



$userId = ThemexUser::$data['user']["ID"];


$activeUser = ThemexUser::$data['active_user'];
$activeUserId = ThemexUser::$data['active_user']["ID"];
$guests = [];
$guestsStr = '';





if ($userId != $activeUserId) {

	if (isset(ThemexUser::$data['active_user']['guests']) && ThemexUser::$data['active_user']['guests'] !== "") {

		$guests = explode(',', ThemexUser::$data['active_user']['guests']);


		$tmpPRE = $userId;
		$tmpAFTER = '';
		for ($i = 0; $i < 10; $i++) {
			$tmpAFTER = $guests[$i];
			$guests[$i] = $tmpPRE;
			$tmpPRE = $tmpAFTER;
			if ($tmpPRE == $userId) {
				break;
			}
		}
	} else {
		$guestsStr = $userId;
	}

	foreach ($guests as $key => $value) {
		if ($key > 0 && $key < count($guests)) $guestsStr .= ",";
		$guestsStr .= $value;
	}
	ThemexCore::updateUserMeta($activeUserId, 'guests', $guestsStr);

	$guests = explode(',', ThemexUser::$data['active_user']['guests']);

	$isNewGuest = true;
	foreach ($guests as $g) {
		if ($g == $userId) {
			$isNewGuest = false;
			break;
		}
	}
	if ($isNewGuest) {
		$strGuestsNew = ThemexUser::$data['active_user']['guests_new'];
		if (empty($strGuestsNew)) {
			$strGuestsNew = $userId;
		} else {
			$strGuestsNew .= "," . $userId;
		}
		ThemexCore::updateUserMeta($activeUserId, 'guests_new', $strGuestsNew);
	}
} else {
	ThemexCore::updateUserMeta($activeUserId, 'guests_new', "");
}
?>

<?php if (ThemexUser::isProfile()) { ?>
	<?php get_template_part('template', 'profile'); ?>
<?php } else if (ThemexUser::$data['active_user']['role'] != 'inactive') { ?>
	<?php get_header(); ?>
	<aside class="column threecol">
		<?php get_template_part('content', 'profile-grid'); ?>
		<?php get_template_part('content', 'profile-views-mounts'); ?>
	</aside>
	<div class="full-profile fivecol column">
		<div class="section-title separated-title">
			<h2>
				<?php
				get_template_part('module', 'status');
				echo ThemexUser::$data['active_user']['profile']['full_name'];
				?>
			</h2>
		</div>
		<table class="profile-fields">
			<tbody>
				<?php if (!ThemexCore::checkOption('user_gender')) { ?>
					<?php if (!empty(ThemexUser::$data['active_user']['profile']['gender'])) { ?>
						<tr>
							<th><?php _e('Gender', 'lovestory'); ?></th>
							<td><?php echo themex_array_value(ThemexUser::$data['active_user']['profile']['gender'], ThemexCore::$components['genders']); ?></td>
						</tr>
					<?php } ?>
					<?php if (!empty(ThemexUser::$data['active_user']['profile']['seeking'])) { ?>
						<tr>
							<th><?php _e('Seeking', 'lovestory'); ?></th>
							<td><?php echo themex_array_value(ThemexUser::$data['active_user']['profile']['seeking'], ThemexCore::$components['genders_alt']); ?></td>
						</tr>
					<?php } ?>
				<?php } ?>
				<?php if (!empty(ThemexUser::$data['active_user']['profile']['age']) && !ThemexCore::checkOption('user_age')) { ?>
					<tr>
						<th><?php _e('Age', 'lovestory'); ?></th>
						<td>
							<?php
							$age = ThemexUser::$data['active_user']['profile']['age'];
							if (validateDate($age, "Y-m-d")) {
								$dateTime = new DateTime($age);
								$age = getAge($dateTime->format("Y"), $dateTime->format("m"), $dateTime->format("d"));
							} else {
								$age = "???";
							}
							echo $age;
							?>


						</td>
					</tr>
				<?php } ?>
				<?php if (!ThemexCore::checkOption('user_location')) { ?>
					<?php if (!empty(ThemexUser::$data['active_user']['profile']['country'])) { ?>
						<tr>
							<th><?php _e('Country', 'lovestory'); ?></th>
							<td>
								<?php


								if (ThemexUser::$data['active_user']['profile']['country'] == 'RU') {
									ThemexUser::$data['active_user']['profile']['country'] = "Россия";
								}
								echo ThemexUser::$data['active_user']['profile']['country'];
								?>
							</td>
						</tr>
					<?php } ?>
					<?php if (!empty(ThemexUser::$data['active_user']['profile']['city'])) { ?>
						<tr>
							<th><?php _e('City', 'lovestory'); ?></th>
							<td><?php echo ThemexUser::$data['active_user']['profile']['city']; ?></td>
						</tr>
					<?php } ?>
				<?php } ?>
				<?php
				ThemexForm::renderData('profile', array(
					'before_title' => '<tr><th>',
					'after_title' => '</th>',
					'before_content' => '<td>',
					'after_content' => '</td></tr>',
				), ThemexUser::$data['active_user']['profile']);
				?>
			</tbody>
		</table>
		<div class="profile-description">
			<?php echo wpautop(ThemexInterface::parseEmbed(ThemexUser::$data['active_user']['profile']['description'])); ?>
		</div>
	</div>
	<?php get_sidebar('profile-right'); ?>
	<?php get_footer(); ?>
<?php } else { ?>
	<?php get_header(); ?>
	<h3><?php _e('No profiles found. Try a different search?', 'lovestory'); ?></h3>
	<p><?php _e('Sorry, no profiles matched your search. Try again with different parameters.', 'lovestory'); ?></p>
	<?php get_footer(); ?>
<?php } ?>