<form action="/wp-admin/admin-ajax.php?action=widget_message_send" method="POST" id="FormWidgetPrivateForm" class="widget-message-private-form d-flex" data-user-name="<?= $LiveChat->UserCurrent['login'] ?>" data-user-id="<?= $LiveChat->UserCurrent['ID'] ?>" data-active-user-name="<?= $LiveChat->UserActive['login'] ?>" data-active-user-id="<?= $LiveChat->UserActive['ID'] ?>" data-status="0">
    <?php if (ThemexUser::isWriteMe($LiveChat->UserCurrent['ID'], $LiveChat->UserActive['ID'])) : ?>

        <?php if (ThemexUser::getAvailableMessages($LiveChat->UserCurrent['ID']) > 0) : ?>
            <div class="d-flex flex-grow-1">
                <textarea name="user_message" id="" cols="30" rows="2" class="widget-message-private-form--textarea"></textarea>
                <div class="d-flex flex-column">
                    <!-- -->
                    <input type="hidden" class="action-messenger-send-image" value="themex_messenger_send_image" />
                    <input type="file" id="fileWidgetLoadPhoto" hidden name="user_message_photo" onchange="sendMessage('photo');">
                    <?php wp_nonce_field('user_message_photo', 'my_image_upload_nonce'); ?>
                    <!-- -->
                    <input type="hidden" name="user_recipient" value="<?= $LiveChat->UserCurrent['ID'] ?>" />
                    <input type="hidden" name="nonce" value="<?= wp_create_nonce(THEMEX_PREFIX . 'nonce'); ?>" />
                    <input type="hidden" class="action-messenger-send-message" value="themex_messenger_send_message" />
                    <input type="hidden" name="user" value="<?= $LiveChat->UserCurrent['ID'] ?>" />
                    <input type="hidden" name="active_user" value="<?= $LiveChat->UserActive['ID'] ?>" />
                    <input type="hidden" name="last-message-id" value="<?= $LiveChat->getIdLastMessage() ?>">
                </div>
            </div>
            <div class="widget-message-private-form--action">
                <label id="LabelUploadPhoto" for="fileWidgetLoadPhoto" class="widget-message-private-form--btn" title="Добавить фото">
                    <i class="fas fa-image"></i>
                </label>
                <button type="button" id="ButtonFormWidgetPrivateFormSubmit" class="widget-message-private-form--btn" onclick="sendMessage();">
                    <i class="fas fa-paper-plane"></i>
                </button>
            </div>
        <?php else : ?>
            <?php include 'widget-message-private--alert-danger.php'; ?>
        <?php endif ?>
    <?php else : ?>
        <div class="alert alert-danger flex-grow-1" style="text-align: center;">
            <?= ThemexInterface::renderMessageWritesMeForbidden($LiveChat->UserActive['ID']) ?>

        </div>
    <?php endif ?>
</form>