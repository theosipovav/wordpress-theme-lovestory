<?

$formData = [];

foreach (ThemexForm::$data["profile"] as $key => $field) {

    if ($field["name"] == "Цель знакомства"){
        $formData["cel-znakomstva"]["name"] = $field["name"];
        $formData["cel-znakomstva"]["options"] = [];
        foreach (explode(',', $field['options']) as $key => $value) {
            array_push($formData["cel-znakomstva"]["options"], trim($value));
        }
    }

}
?>

<form action="" method="GET" id="form-search-filter" class="form-site">
    <a href="#" id="ButtonFilter" class="btn">Фильтр</a>
    <div class="form-search-filter-content">

        <div class="form-search-filter-options">
            <div class="d-flex flex-column flex-grow-1">
                <div class="form-search-filter-options-part-1">
                    <div class="form-group">
                        <label for="form-gender"><b>Кого ищем</b></label>
                        <select class="form-control" id="form-gender" name="gender">
                            <?php
                            $array = ThemexCore::$components['genders'];
                            foreach ($array as $key => $value) {
                                if (isset($currentThemexUser['profile']['seeking'])) {
                                    if ($key == $gender) {
                                        echo "<option value='$key' selected>$value</option>";
                                    } else {
                                        echo "<option value='$key'>$value</option>";
                                    }
                                } else {

                                    echo "<option value='$key'>$value</option>";
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="age-from"><b>Возраст</b></label>
                        <div class="d-flex d-row align-items-end">
                            <label for="age-from" class="pr2">с</label>
                            <select class="form-control" id="form-age-from" name="age-from">
                                <?php
                                for ($i = 18; $i <= 80; $i++) {
                                    if ($i == $age_from) {
                                        echo "<option value='$i' selected='selected'>$i</option>";
                                    } else {
                                        echo "<option value='$i'>$i</option>";
                                    }
                                }
                                ?>
                            </select>
                            <label for="age-to" class="pr2 pl2">до</label>
                            <select class="form-control" id="form-age-to" name="age-to">
                                <?php
                                for ($i = 18; $i <= 80; $i++) {
                                    if ($i == $age_to) {
                                        echo "<option value='$i' selected='selected'>$i</option>";
                                    } else {
                                        echo "<option value='$i'>$i</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="form-country"><b>Страна</b></label>
                        <input type="text" class="form-control" id="form-country" name="country" value="<?= $country ?>" placeholder="Любая страна">
                    </div>
                    <div class="form-group">
                        <label for="form-city"><b>Город</b></label>
                        <input type="text" class="form-control" id="form-city" name="city" value="<?= $city ?>" placeholder="Любой город">
                    </div>
                    <div class="form-group">
                        <label for="form-cel-znakomstva"><b>Цель знакомства</b></label>
                        <select class="form-control" id="form-cel-znakomstva" name="cel-znakomstva">
                            <option value="0" <?php if ($cel_znakomstva == 0) echo 'selected'; ?>>Не выбрано</option>
                            <?php
                            foreach ($formData["cel-znakomstva"]["options"] as $key => $value) {
                                $optionsValue = $key + 1;
                                if ($optionsValue == $cel_znakomstva) {
                                    echo "<option value='$optionsValue' selected >$value</option>";
                                } else {
                                    echo "<option value='$optionsValue' >$value</option>";
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group" style="display:none;">
                        <label for="form-znak-zodiaka"><b>Знак Зодиака</b></label>
                        <select class="form-control" id="form-znak-zodiaka" name="znak-zodiaka">
                            <option value="0" <?php if ($znak_zodiaka == 0) echo 'selected'; ?>>Не выбрано</option>
                            <option value="1" <?php if ($znak_zodiaka == 1) echo 'selected'; ?>>Овен</option>
                            <option value="2" <?php if ($znak_zodiaka == 2) echo 'selected'; ?>> Телец</option>
                            <option value="3" <?php if ($znak_zodiaka == 3) echo 'selected'; ?>> Близнецы</option>
                            <option value="4" <?php if ($znak_zodiaka == 4) echo 'selected'; ?>> Рак</option>
                            <option value="5" <?php if ($znak_zodiaka == 5) echo 'selected'; ?>> Лев</option>
                            <option value="6" <?php if ($znak_zodiaka == 6) echo 'selected'; ?>> Дева</option>
                            <option value="7" <?php if ($znak_zodiaka == 7) echo 'selected'; ?>> Весы</option>
                            <option value="8" <?php if ($znak_zodiaka == 8) echo 'selected'; ?>> Скорпион</option>
                            <option value="9" <?php if ($znak_zodiaka == 9) echo 'selected'; ?>> Стрелец</option>
                            <option value="10" <?php if ($znak_zodiaka == 10) echo 'selected'; ?>> Козерог</option>
                            <option value="11" <?php if ($znak_zodiaka == 11) echo 'selected'; ?>> Водолей</option>
                            <option value="12" <?php if ($znak_zodiaka == 12) echo 'selected'; ?>> Рыбы</option>
                        </select>
                    </div>

                </div>
                <div class="form-search-filter-options-part-2">
                    <div class="form-group">
                        <label for="form-rost-sm"><b>Рост</b></label>
                        <input type="number" class="form-control" id="form-rost-sm" name="rost-sm" value="<?= $rost_sm ?>" placeholder="-" style="width: 70px;">
                    </div>
                    <div class="form-group">
                        <label for="form-ves-kg"><b>Вес</b></label>
                        <input type="number" class="form-control" id="form-ves-kg" name="ves-kg" value="<?= $ves_kg ?>" placeholder="-" style="width: 70px;">
                    </div>
                    <div class="form-group">
                        <label for="form-obrazovanie"><b>Образование</b></label>
                        <select class="form-control" id="form-obrazovanie" name="obrazovanie">
                            <option value="0" <?php if ($obrazovanie == 0) echo 'selected'; ?>>Не выбрано</option>
                            <option value="1" <?php if ($obrazovanie == 1) echo 'selected'; ?>>Неполное среднее</option>
                            <option value="2" <?php if ($obrazovanie == 2) echo 'selected'; ?>>Среднее</option>
                            <option value="3" <?php if ($obrazovanie == 3) echo 'selected'; ?>>Высшее</option>
                            <option value="4" <?php if ($obrazovanie == 4) echo 'selected'; ?>>Несколько высших</option>
                            <option value="5" <?php if ($obrazovanie == 5) echo 'selected'; ?>>Другое</option>
                            <option value="6" <?php if ($obrazovanie == 6) echo 'selected'; ?>>Не важно</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="form-otnoshenie-k-kureniyu"><b>Отношение к курению</b></label>
                        <select class="form-control" id="form-otnoshenie-k-kureniyu" name="otnoshenie-k-kureniyu">
                            <option value="0" <?php if ($otnoshenie_k_kureniyu == 0) echo 'selected'; ?>>Не выбрано</option>
                            <option value="1" <?php if ($otnoshenie_k_kureniyu == 1) echo 'selected'; ?>>Курю ежедневно</option>
                            <option value="2" <?php if ($otnoshenie_k_kureniyu == 2) echo 'selected'; ?>>Бросаю</option>
                            <option value="3" <?php if ($otnoshenie_k_kureniyu == 3) echo 'selected'; ?>>Курю в компании</option>
                            <option value="4" <?php if ($otnoshenie_k_kureniyu == 4) echo 'selected'; ?>>Не приемлю</option>
                            <option value="5" <?php if ($otnoshenie_k_kureniyu == 5) echo 'selected'; ?>>Другое</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="form-otnoshenie-k-alkogolyu"><b>Отношение к алкоголю</b></label>
                        <select class="form-control" id="form-otnoshenie-k-alkogolyu" name="otnoshenie-k-alkogolyu">
                            <option value="0" <?php if ($otnoshenie_k_alkogolyu == 0) echo 'selected'; ?>>Не выбрано</option>
                            <option value="1" <?php if ($otnoshenie_k_alkogolyu == 1) echo 'selected'; ?>>Не пью</option>
                            <option value="2" <?php if ($otnoshenie_k_alkogolyu == 2) echo 'selected'; ?>>Пью в компании иногда</option>
                            <option value="3" <?php if ($otnoshenie_k_alkogolyu == 3) echo 'selected'; ?>>Люблю выпить</option>
                            <option value="4" <?php if ($otnoshenie_k_alkogolyu == 4) echo 'selected'; ?>>Не важно</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-center flex-grow-1">
                <div class="form-group form-group-checkbox">
                    <div class="d-flex d-inline-flex align-items-center">
                        <input id="hiddenFormSearchPhotosYes" type="hidden" name="photos-yes" value="<?= $isFilterPhotoYes ?>">
                        <input id="checkboxFormSearchPhotosYes" type="checkbox" <?php if ($isFilterPhotoYes != 0) echo 'checked'; ?> class="form-search-filter-checkbox m0 mr1">
                        <label for="checkboxFormSearchPhotosYes"><b>Анкеты с фото</b></label>
                    </div>
                    <div class="d-flex d-inline-flex align-items-center">
                        <input id="hiddenFormSearchPhotosNo" type="hidden" name="photos-no" value="<?= $isFilterPhotoNo ?>">
                        <input id="checkboxFormSearchPhotosNo" type="checkbox" <?php if ($isFilterPhotoNo != 0) echo 'checked'; ?> class="form-search-filter-checkbox m0 mr1">
                        <label for="checkboxFormSearchPhotosNo"><b>Анкеты без фото</b></label>
                    </div>
                    <div class="d-flex d-inline-flex align-items-center">
                        <input id="hiddenFormSearchOnlineYes" type="hidden" name="online-yes" value="<?= $isFilterOnlineYes ?>">
                        <input id="checkboxFormSearchOnlineYes" type="checkbox" <?php if ($isFilterOnlineYes != 0) echo 'checked'; ?> class="form-search-filter-checkbox m0 mr1">
                        <label for="checkboxFormSearchOnlineYes"><b>Анеты онлайн</b></label>
                    </div>
                    <div class="d-flex d-inline-flex align-items-center">
                        <input id="hiddenFormSearchOnlineNo" type="hidden" name="online-no" value="<?= $isFilterOnlineNo ?>">
                        <input id="checkboxFormSearchOnlineNo" type="checkbox" <?php if ($isFilterOnlineNo != 0) echo 'checked'; ?> class="form-search-filter-checkbox m0 mr1">
                        <label for="checkboxFormSearchOnlineNo"><b>Анкеты оффлайн</b></label>
                    </div>
                    <div class="d-flex d-inline-flex align-items-center">
                        <input id="hiddenFormSearchAll" type="hidden" name="" value="0">
                        <input id="checkboxFormSearchAll" type="checkbox" <?php if ($isFilterAll != 0) echo 'checked'; ?> class="m0 mr1">
                        <label for="checkboxFormSearchAll"><b>Все анкеты</b></label>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group d-flex flex-row justify-content-center align-items-center">
            <a href="/search" class="button btn-search submit-button m1 mt3 pt3 pr5 pb3 pl5">
                <span class="button-icon icon-search"></span>ПОКАЗАТЬ ВСЕ
            </a>
            <button class="button btn-search submit-button m1 mt3 pt3 pr5 pb3 pl5" type="submit">
                <span class="button-icon icon-search"></span>ПОИСК
            </button>
        </div>
    </div>
</form>