<?php


/*
Template Name: Search
Template Post Type: post, page, product
*/

add_action('wp_enqueue_scripts', 'my_scripts_method');
function my_scripts_method()
{
    wp_enqueue_script('custom-script', get_template_directory_uri() . "/assets/js/template-search.js", array('jquery-ui-autocomplete'));
    wp_enqueue_script('api-maps', 'https://api-maps.yandex.ru/1.1/index.xml');
}

// Текущий пользователь
$currentUserId = get_current_user_id();
$currentThemexUser = ThemexUser::getUser(get_current_user_id());





//d(themex_sanitize_key($field["Цель знакомства"]));
//d($field["name"]);

//d(array_merge($items, explode(',', $field['options'])));

// Получаем профили с учетом фильтра
$gender = '';
$age_from = '18';
$age_to = '70';
$city = '';
$cel_znakomstva = '';
$rost_sm = '';
$ves_kg = '';
$znak_zodiaka = '';
$obrazovanie = '';
$otnoshenie_k_kureniyu = '';
$otnoshenie_k_alkogolyu = '';
$args = [
    'meta_query' => [],
];
// Обработка поля <Кого ищем>
if (isset($_GET['gender']) && !empty($_GET['gender'])) {
    $arr = [
        'key' => '_' . THEMEX_PREFIX . 'gender',
        'value' => $_GET['gender'],
    ];
    array_push($args['meta_query'], $arr);
    $gender = $_GET['gender'];
}
// Обработка поля <Возраст - c ...>
if (isset($_GET['age-from']) && !empty($_GET['age-from'])) {
    $arr = [
        'key' => '_' . THEMEX_PREFIX . 'age',
        'value' => $_GET['age-from'],
        'compare' => '>=',
    ];
    array_push($args['meta_query'], $arr);
    $age_from = $_GET['age-from'];
}
// Обработка поля <Возраст - до ...>
if (isset($_GET['age-to']) && !empty($_GET['age-to'])) {
    $arr = [
        'key' => '_' . THEMEX_PREFIX . 'age',
        'value' => $_GET['age-to'],
        'compare' => '<=',
    ];
    array_push($args['meta_query'], $arr);
    $age_to = $_GET['age-to'];
}
// Обработка поля <Страна>
if (isset($_GET['country']) && !empty($_GET['country'])) {
    $arr = [
        'key' => '_' . THEMEX_PREFIX . 'country',
        'value' => $_GET['country'],
    ];
    array_push($args['meta_query'], $arr);
    $country = $_GET['country'];
} else {
    $country = "";
}
// Обработка поля <Город>
if (isset($_GET['city']) && !empty($_GET['city'])) {
    $arr = [
        'key' => '_' . THEMEX_PREFIX . 'city',
        'value' => $_GET['city'],
    ];
    array_push($args['meta_query'], $arr);
    $city = $_GET['city'];
} else {
    $city = "";
}
// Обработка поля <Цель знакомства>
if (isset($_GET['cel-znakomstva']) && !empty($_GET['cel-znakomstva'])) {
    $arr = [
        'key' => '_' . THEMEX_PREFIX . 'cel-znakomstva',
        'value' => $_GET['cel-znakomstva'],
    ];
    array_push($args['meta_query'], $arr);
    $cel_znakomstva = $_GET['cel-znakomstva'];
}
// Обработка поля <Рост>
if (isset($_GET['rost-sm']) && !empty($_GET['rost-sm'])) {
    $arr = [
        'key' => '_' . THEMEX_PREFIX . 'rost-sm',
        'value' => $_GET['rost-sm'],
    ];
    array_push($args['meta_query'], $arr);
    $rost_sm = $_GET['rost-sm'];
}
// Обработка поля <Вес>
if (isset($_GET['ves-kg']) && !empty($_GET['ves-kg'])) {
    $arr = [
        'key' => '_' . THEMEX_PREFIX . 'ves-kg',
        'value' => $_GET['ves-kg'],
    ];
    array_push($args['meta_query'], $arr);
    $ves_kg = $_GET['ves-kg'];
}
/*
// Обработка поля <Знак Зодиака>
if (isset($_GET['znak-zodiaka']) && !empty($_GET['znak-zodiaka'])) {
    $arr = [
        'key' => '_' . THEMEX_PREFIX . 'znak-zodiaka',
        'value' => $_GET['znak-zodiaka'],
    ];
    array_push($args['meta_query'], $arr);
    $znak_zodiaka = $_GET['znak-zodiaka'];
}
*/
// Обработка поля <Образование>
if (isset($_GET['obrazovanie']) && !empty($_GET['obrazovanie'])) {
    $arr = [
        'key' => '_' . THEMEX_PREFIX . 'obrazovanie',
        'value' => $_GET['obrazovanie'],
    ];
    array_push($args['meta_query'], $arr);
    $obrazovanie = $_GET['obrazovanie'];
}
// Обработка поля <Отношение к курению>
if (isset($_GET['otnoshenie-k-kureniyu']) && !empty($_GET['otnoshenie-k-kureniyu'])) {
    $arr = [
        'key' => '_' . THEMEX_PREFIX . 'otnoshenie-k-kureniyu',
        'value' => $_GET['otnoshenie-k-kureniyu'],
    ];
    array_push($args['meta_query'], $arr);
    $otnoshenie_k_kureniyu = $_GET['otnoshenie-k-kureniyu'];
}
// Обработка поля <Отношение к алкоголю>
if (isset($_GET['otnoshenie-k-alkogolyu']) && !empty($_GET['otnoshenie-k-alkogolyu'])) {
    $arr = [
        'key' => '_' . THEMEX_PREFIX . 'otnoshenie-k-alkogolyu',
        'value' => $_GET['otnoshenie-k-alkogolyu'],
    ];
    array_push($args['meta_query'], $arr);
    $otnoshenie_k_alkogolyu = $_GET['otnoshenie-k-alkogolyu'];
}

$args['number'] = 50;


$isSearchStop = 0;

/** Обработка параметра поиска по фото  */
$isFilterPhotoYes = 1;
$isFilterPhotoNo = 1;
if (isset($_GET['photos-yes']) && $_GET['photos-yes'] == '0') {
    $isFilterPhotoYes = 0;
}
if (isset($_GET['photos-no']) && $_GET['photos-no'] == '0') {
    $isFilterPhotoNo = 0;
}
if ($isFilterPhotoYes == 1 && $isFilterPhotoNo == 1) {
    // Фильтр не применяется
} else {
    if ($isFilterPhotoYes == 0 && $isFilterPhotoNo == 0) {
        $isSearchStop = 1;
    } else {
        if ($isFilterPhotoYes == 1) {
            // Показываем анкеты с фото
            $arr = [
                'key' => '_' . THEMEX_PREFIX . 'avatar',
                'compare' => 'EXIST',
            ];
            array_push($args['meta_query'], $arr);
        } else {
            // Показываем анкеты без фото
            $arr = [
                'key' => '_' . THEMEX_PREFIX . 'avatar',
                //'value' => "",
                'compare' => 'NOT EXISTS',
            ];
            array_push($args['meta_query'], $arr);
        }
    }
}
/** Обработка параметра поиска по онлайну  */
$isFilterOnlineYes = 1;
$isFilterOnlineNo = 1;
if (isset($_GET['online-yes'])  && $_GET['online-yes'] == 0) {
    $isFilterOnlineYes = 0;
}
if (isset($_GET['online-no']) &&  $_GET['online-no'] == 0) {
    $isFilterOnlineNo = 0;
}
if ($isFilterOnlineYes == 1 && $isFilterOnlineNo == 1) {
    // Фильтр не применяется
} else {
    if ($isFilterOnlineYes == 0 && $isFilterOnlineNo == 0) {
        $isSearchStop = 1;
    } else {
        $onlineIds = [];
        foreach ($_SESSION['users'] as $key => $value) {
            $onlineIds[] = $key;
        }
        if ($isFilterOnlineYes == 1) {
            // Показываем анкеты онлайн
            $args['include'] = $onlineIds;
        } else {
            // Показываем анкеты оффлайн
            $args['exclude'] = $onlineIds;
        }
    }
}
if ($isSearchStop == 1) {
    $users = [];
} else {
    $users = get_users($args);
}


if ($isFilterPhotoYes == 1 && $isFilterPhotoNo == 1 && $isFilterOnlineYes == 1 && $isFilterOnlineNo == 1) {
    $isFilterAll = 1;
} else {
    $isFilterAll = 0;
}
?>
<?php get_header(); ?>
<div class="d-flex flex-column">
    <?php while (have_posts()) : the_post(); ?>
        <?php the_content(); ?>
    <?php endwhile; // End of the loop. 
    ?>
    <?php /* Форма поиска */ ?>
    <div class="section-form-filter">
        <?php
        include(locate_template('template-search--form-filter.php'));
        ?>
    </div>
    <hr>
    <?php


    ?>

    <div class="section-form-profiles">
        <?php if (!empty($users)) { ?>
            <div class="profiles-listing clearfix">
                <?php
                $counter = 0;
                foreach ($users as $user) {
                    ThemexUser::$data['active_user'] = ThemexUser::getUser($user->ID);
                    $counter++;
                ?>
                    <div class="column fourcol <?php if ($counter == 3) { ?>last<?php } ?>">
                        <?php
                        get_template_part('content', 'profile-grid-v2');
                        ?>
                    </div>
                    <?php
                    if ($counter == 3) {
                        $counter = 0;
                    ?>
                        <div class="clear"></div>
                    <?php } ?>
                <?php } ?>
            </div>
            <!-- /profiles -->
        <?php } else { ?>
            <h3>Никаких профилей не найдено. Попробуйте другой поиск?</h3>
            <p>К сожалению, ни один профиль не соответствует вашему поиску. Повторите попытку с другими параметрами.</p>
        <?php } ?>
    </div>
    <hr>
    <div class="d-flex justify-content-center">
        <div class="btn btn-loadmore" style="display: none">Загрузить еще</div>
    </div>
</div>
<aside class="sidebar column fourcol last">
    <?php get_sidebar(); ?>
</aside>

<div class="remodal" data-remodal-id="modal-form-search-payment" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
    <button data-remodal-action="close" class="remodal-close" aria-label="Закрыть"></button>
    <div>
        <h2 id="modal1Title">Необходим VIP статус!</h2>
        <p id="modal1Desc">
            ...
        </p>
        <div class="module-payment-example">
            <?php
            $usersForExample = get_users();
            for ($j = 0; $j < 6; $j++) {
                $userId = $usersForExample[$j]->ID;
                include(locate_template('module-top-photos--slide-example.php'));
            }
            ?>
        </div>
        <?php get_template_part('module-payment--paypal'); ?>
        <div class="remodal-group-btns">
            <a class="remodal-cancel btn-payment-cancel" href="#">Назад</a>
            <a class="remodal-confirm btn-payment" href="/product/top-photo/">Перейти на страницу оплаты</a>
        </div>
    </div>
</div>
<?php get_footer(); ?>