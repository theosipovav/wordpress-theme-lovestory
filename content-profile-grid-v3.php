<div class="profile-preview">
    <?php
    $isBirthday = false;
    if (validateDate(ThemexUser::$data['active_user']['profile']['age'])) {
        $birthdayDateTime = new DateTime(ThemexUser::$data['active_user']['profile']['age'], new DateTimeZone('EUROPE/Moscow'));
        $curDateTime = new DateTime("now", new DateTimeZone('EUROPE/Moscow'));
        if ($curDateTime->format("d") == $birthdayDateTime->format("d") && $curDateTime->format("m") == $birthdayDateTime->format("m")) {
            $isBirthday = true;
        }
    }
    ?>

    <?php if ($isBirthday == true) { ?>
        <div class="profile-birthday">
            <img src="<?= get_template_directory_uri() . "/assets/img/birthday_" . rand(1, 3) . ".svg" ?>" alt="">
            <a class="colorbox inline" href="#gifts_listing" title="У пользователя сегодня ДЕНЬ РОЖДЕНИЯ! Не забудьте поздравить его!" data-value="<?= ThemexUser::$data['active_user']["ID"] ?>"></a>
        </div>
    <?php } ?>

    <div class="profile-image">
        <a href="<?php echo ThemexUser::$data['active_user']['profile_url']; ?>">
            <?= ThemexUser::getAvatar2(ThemexUser::$data['active_user']['ID'], 200); ?>
        </a>
    </div>
    <?php if (is_page_template('template-search.php') || is_page_template('template-profiles.php') || ThemexCore::isRewriteRule('message') || ThemexCore::isRewriteRule('chat') || ThemexUser::isUserFilter()) { ?>
        <div class="profile-text">
            <h5><?php get_template_part('module', 'status'); ?><a href="<?php echo ThemexUser::$data['active_user']['profile_url']; ?>"><?php echo ThemexUser::$data['active_user']['profile']['full_name']; ?></a></h5>
            <p><?php echo ThemexUser::getExcerpt(ThemexUser::$data['active_user']['profile']); ?></p>
        </div>
    <?php } ?>
</div>