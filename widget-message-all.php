<?php
$LiveChat->recipients = ThemexUser::getRecipients(ThemexUser::$data['user']['ID']);

?>

<div id="WidgetMessageAll" class="widget-message-all widget-message-content-active" user-id=<?= $LiveChat->UserCurrent['ID'] ?>>
    <ul class="widget-message-all-list">
        <?php

        foreach ($LiveChat->recipients  as $recipient) {
            $LiveChat->UserActive = ThemexUser::getUser($recipient['ID']);
            include "widget-message-all--item.php";
            $LiveChat->UserActive = null;
        }
        ?>
    </ul>
    <?php
    ThemexUser::refresh();
    ?>
</div>