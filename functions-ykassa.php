<?php


/**
 *  https://commonlove.st/?yandex_money=callback
 */
require __DIR__ . '/lib/autoload.php';

use YandexCheckout\Model\Notification\NotificationSucceeded;
use YandexCheckout\Model\Notification\NotificationWaitingForCapture;
use YandexCheckout\Client;
use YandexCheckout\Model\NotificationEventType;



/*
$client = new Client();
$client->setAuthToken('AgAAAAAGvU8EAAZt0WMqkyk_OUihlB8pJyXvaWY');
$response = $client->addWebhook([
    "event" => NotificationEventType::PAYMENT_SUCCEEDED,
    "url"   => "https://commonlove.ru/?webhook=1",
]);
*/

function yaCallback()
{
    if (!empty($_GET["yandex_money"])) {

        $source = file_get_contents('php://input');
        $requestBody = json_decode($source, true);
        try {
            $notification = ($requestBody['event'] === NotificationEventType::PAYMENT_SUCCEEDED)
                ? new NotificationSucceeded($requestBody)
                : new NotificationWaitingForCapture($requestBody);
        } catch (Exception $e) {
            // Обработка ошибок при неверных данных
        }
        // Получите объект платежа
        $payment = $notification->getObject();

        $idPayment = $payment->getId();
        $metadataPayment = $payment->metadata;
        $idUser = $metadataPayment->wp_user_id;
        $statusPayment = $payment->status;
        $idOrder = $payment->description;

        /*
        $out = [
            $payment->getId(), // id
            $payment->metadata, // meta
            $payment->paid,
            $payment->status,
            $payment->description,
            $idUser,
        ];
        $text = json_encode($out, true);
        $fp = fopen("yandex_money-" . GUID() . ".json", "w");
        fwrite($fp, $text);
        fclose($fp);
        return;
        $idOrder = "1348";
        $statusPayment = 'succeeded';

            */


        if ($statusPayment != 'succeeded') {
            return;
        }
        $WC_Order = new WC_Order();
        $WC_Order->set_id($idOrder);
        foreach ($WC_Order->get_items() as $key => $item) {
            $data = $item;
            $product_id = $data["product_id"];
            $coins = get_post_meta($product_id, '_count_field', true);
            $coinsCurrent = ThemexUser::paymentGet($idUser);
            if ($coinsCurrent == null || $coinsCurrent == 0) {
                $coinsAdd = $coins;
            } else {
                $coinsAdd = $coinsCurrent + $coins;
            }
            update_metadata('user', $idUser, 'balance_coins', $coinsAdd, $coinsCurrent);
            $coinsCurrent = get_metadata('user', $idUser, 'balance_coins', 1);
        }
    }
}
yaCallback();
