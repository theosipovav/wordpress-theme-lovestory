<?php
// Загрузка CSS плагина
wp_enqueue_style("carousel-top-photo", get_template_directory_uri() . "/assets/css/carousel-top-photo.min.css");
// Загрузка JS плагина
wp_enqueue_script('carousel-top-photo', get_template_directory_uri() . '/assets/js/carousel-top-photo.js', [], false, true);

$options = get_option('settings_top_photo');
$themexUsers = [];
$idLastTopPhoto = -1;


foreach (get_posts(['post_type' => 'top_photo', 'numberposts' => -1]) as $key => $p) {

    if ($idLastTopPhoto == -1) {
        $idLastTopPhoto = $p->ID;
    }

    if (count($themexUsers) == intval($options['top_photo_field_4'])) {
        break;
    }

    $themexUser = ThemexUser::getUser($p->post_author, true);
    if (ThemexUser::$data['user']['profile']['city'] != '') {
        if (ThemexUser::$data['user']['profile']['city'] == $themexUser['profile']['city']) {
            $themexUsers[] = $themexUser;
        }
    } else {
        $themexUsers[] = $themexUser;
    }
}



?>
<div class="module-top-photos" style="opacity: 0;" id="module-top-photos-1">
    <?php
    if (is_user_logged_in()) {
        get_template_part('module-top-photos--subscribe');
    }

    ?>


    <div class="carousel-top-photo" <?php if (!is_user_logged_in()) echo "style='margin-left: 0;'"; ?> data-current-user="<?= ThemexUser::$data['user']["ID"] ?>" data-last-top-photo-id="<?= $idLastTopPhoto ?>">
        <?php
        foreach ($themexUsers as $themexUser) {
            $userId = $themexUser["ID"];
            $url = $themexUser['profile_url'];
            $profile = $themexUser['profile'];
            $avatar = get_avatar_url($userId);
            $name = $profile["name"];
            if (validateDate($profile["age"], "Y-m-d")) {
                $dateTime = new DateTime($profile["age"]);
                $age = getAge($dateTime->format("Y"), $dateTime->format("m"), $dateTime->format("d"));
            } else {
                $age = "???";
            }
            $country = $profile["country"];
            if ($country == 'RU') {
                $country = 'Россия';
            }
            $city = $profile["city"];
            $status = $themexUser['status']['value'];
            $about = $profile["description"];
            include(locate_template('module-top-photos--slide.php'));
        }
        ?>
    </div>
    <div class="remodal" data-remodal-id="modal-payment" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
        <button data-remodal-action="close" class="remodal-close" aria-label="Закрыть"></button>
        <div>
            <h2 id="modal1Title"><?= $options['top_photo_field_2'] ?></h2>
            <p id="modal1Desc"><?= $options['top_photo_field_3'] ?></p>
            <div class="module-payment-example">
                <div class="slide"><img src="<?= get_template_directory_uri() ?>/assets/img/carousel-top-photo--photo-1.jpg"></div>
                <div class="slide"><img src="<?= get_template_directory_uri() ?>/assets/img/carousel-top-photo--photo-2.jpg"></div>
                <div class="slide"><img src="<?= get_template_directory_uri() ?>/assets/img/carousel-top-photo--photo-3.jpg"></div>
                <div class="slide"><img src="<?= get_template_directory_uri() ?>/assets/img/carousel-top-photo--photo-4.jpg"></div>
                <div class="slide"><img src="<?= get_template_directory_uri() ?>/assets/img/carousel-top-photo--photo-5.jpg"></div>
                <div class="slide"><img src="<?= get_template_directory_uri() ?>/assets/img/carousel-top-photo--photo-6.jpg"></div>
                <div class="slide"><img src="<?= get_template_directory_uri() ?>/assets/img/carousel-top-photo--photo-7.jpg"></div>
                <div class="slide"><img src="<?= get_template_directory_uri() ?>/assets/img/carousel-top-photo--photo-8.jpg"></div>
                <div class="slide"><img src="<?= get_template_directory_uri() ?>/assets/img/carousel-top-photo--photo-9.jpg"></div>
            </div>
            <?php get_template_part('module-payment--paypal'); ?>
            <div class="remodal-group-btns">
                <a class="remodal-cancel btn-payment-cancel" href="#">Назад</a>
                <?php
                $query = new WP_Query([
                    'post_type' => 'product',
                    'post_name' => 'top-photo',
                    'hierarchical' => 0,
                    'order' => 'ASC',
                    'product_cat' => 'service',
                ]);
                ?>
                <a class="remodal-confirm btn-payment" href="<?= $options['top_photo_field_0'] ?>">Перейти на страницу оплаты</a>
            </div>
        </div>
    </div>
</div>