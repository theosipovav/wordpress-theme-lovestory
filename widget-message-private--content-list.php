<?php foreach ($LiveChat->messages as $message) : ?>
    <?php
    if ($message->user_id == $LiveChat->UserCurrent['ID']) {
        $css = 'widget-message-private-listed-message-user';
        $fullName = $LiveChat->UserCurrent["profile"]["full_name"];
    } else {
        $css = 'widget-message-private-listed-message-active-user';
        $fullName = $LiveChat->UserActive["profile"]["full_name"];
    }
    ?>
    <li class="widget-message-private-listed-message <?= $css ?>">
        <div class="widget-message-private-message-header">
            <div class="widget-message-private-message-data"><?= $message->comment_date ?></div>
            <div class="widget-message-private-message-author"><a href="<?= get_author_posts_url($message->user_id); ?>"><?= $fullName ?></a></div>
            <i class="widget-message-private-message-icon fas fa-circle"></i>
        </div>
        <div class="widget-message-private-message-text"><?= $message->comment_content ?></div>
    </li>

<?php endforeach ?>