<?php

/**
 * Модальное окно авторизации
 */
?>
<div id="SModalAuthorization" class="smodal">
    <span class="smodal-close"></span>
    <form class="ajax-form form-authorization" action="<?php echo AJAX_URL; ?>" method="POST">
        <div class="form-group justify-content-center">
            <div class="d-flex justify-content-center logo">
                <img src="<?= ThemexCore::getOption('logo', THEME_URI . 'images/logo.png'); ?>" alt="<?php bloginfo('name'); ?>" />
            </div>
        </div>
        <div class="form-group">
            <div class="message"></div>
        </div>
        <div class="form-group">
            <label for="InputAuthorizationLogin" class="form-control">Электронная почта или логин</label>
            <input type="text" name="user_login" id="InputAuthorizationLogin" class="form-control" placeholder="<?php _e('Пользователь', 'lovestory'); ?>" value="" />
        </div>
        <div class="form-group">
            <label for="InputAuthorizationPassword" class="form-control">Пароль</label>
            <span class="password-view" data-target="InputAuthorizationPassword"><i class="fas fa-eye"></i></span>
            <input type="password" name="user_password" class="form-control" id="InputAuthorizationPassword" placeholder="<?php _e('Пароль', 'lovestory'); ?>" value="" />
        </div>
        <div class="form-group flex-row justify-content-center" style="padding: 5px 0;">
            <?php
            if (ThemexCore::checkOption('user_captcha')) {
                echo renderGoogleRecaptcha();
            }
            ?>
        </div>
        <div class="form-group flex-row justify-content-center">
            <div class="d-flex flex-column flex-grow-1">
                <div class="d-flex flex-row">
                    Вход через соц. сети
                </div>
                <div class="d-flex flex-row">
                    <?= do_shortcode('[miniorange_social_login]'); ?>
                </div>
            </div>
            <div class="d-flex flex-row justify-content-end align-items-end">
                <a href="#" class="btn btn-cancel">Назад</a>
                <a href="#" class="button submit-button"><?php _e('Войти', 'lovestory'); ?></a>
            </div>
        </div>
        <div class="form-group flex-row justify-content-end">
            <a href="#" class="btn btn-link btn-auth-reset" data-target="FormAuthorizationReset" title="<?php _e('Восстановление пароля', 'lovestory'); ?>">
                Забыли пароль?
            </a>
        </div>
        <input type="hidden" name="user_action" value="login_user" />
        <input type="hidden" class="nonce" value="<?php echo wp_create_nonce(THEMEX_PREFIX . 'nonce'); ?>" />
        <input type="hidden" class="action" value="<?php echo THEMEX_PREFIX; ?>update_user" />
    </form>
    <form id="FormAuthorizationReset" class="ajax-form form-authorization form-authorization-reset" action="<?php echo AJAX_URL; ?>" method="POST">
        <div class="form-group">
            <h3>Восстановление пароля</h3>
        </div>
        <div class="form-group">
            <div class="message"></div>
        </div>
        <div class="form-group">
            <label for="InputAuthorizationReset" class="form-control">Электронная почта</label>
            <input type="text" name="user_email" id="InputAuthorizationReset" placeholder="<?php _e('Email', 'lovestory'); ?>" value="" />
        </div>
        <div class="form-group flex-row justify-content-center">
            <a href="#" class="button submit-button"><?php _e('Сброс пароля', 'lovestory'); ?></a>
        </div>
        <input type="hidden" name="user_action" value="reset_password" />
        <input type="hidden" class="nonce" value="<?php echo wp_create_nonce(THEMEX_PREFIX . 'nonce'); ?>" />
        <input type="hidden" class="action" value="<?php echo THEMEX_PREFIX; ?>update_user" />
    </form>
</div>
<?php
/**
 * Модальное окно регистрации
 */

?>
<div id="SModalRegistration" class="smodal container">
    <span class="smodal-close"></span>
    <div class="d-flex flex-column">
        <h3>Регистрация</h3>
        <form class="ajax-form form-authorization form-registration" action="<?php echo AJAX_URL; ?>" method="POST">
            <img class="smodal-registration-logo" src="<?= ThemexCore::getOption('logo', THEME_URI . 'images/logo.png'); ?>" alt="">

            <div class="form-group">
                <label for="InputRegistrationLogin" class="form-control">Логин</label>
                <input type="text" name="user_login" id="InputRegistrationLogin" class="form-control" placeholder="Введите логин для входа на сайт" value="" />
            </div>
            <div class="form-group">
                <label for="InputRegistrationEmail" class="form-control">Электронная почта</label>
                <input type="text" name="user_email" id="InputRegistrationEmail" class="form-control" placeholder="Укажите свою почту" value="" />
            </div>
            <div class="form-group">
                <label for="InputRegistrationPassword" class="form-control">Пароль</label>
                <span class="password-view" data-target="InputRegistrationPassword"><i class="fas fa-eye"></i></span>

                <input type="password" name="user_password" id="InputRegistrationPassword" class="form-control" placeholder="Введите пароль для входа на сайт" value="" />
            </div>
            <div class="form-group">
                <span class="password-view" data-target="InputRegistrationPasswordRepeat"><i class="fas fa-eye"></i></span>
                <input type="password" name="user_password_repeat" id="InputRegistrationPasswordRepeat" class="form-control" placeholder="Повторите пароль" value="" />
            </div>
            <div class="form-group">
                <div class="checkbox-control">
                    <input type="checkbox" name="user_agreement" id="CheckboxAgreement">
                    <a href="agreement/" target="_blank">Я принимаю Пользовательское соглашение</a>
                </div>
            </div>
            <div class="form-group flex-row justify-content-center" style="padding: 10px 0;">
                <?php
                if (ThemexCore::checkOption('user_captcha')) {
                    echo renderGoogleRecaptcha();
                }
                ?>
            </div>
            <div class="form-group flex-row justify-content-end">
                <a href="#" class="btn btn-cancel">Назад</a>
                <a href="#" class="button submit-button">Зарегистрироваться</a>
                <div class="loader"></div>
                <input type="hidden" name="user_action" value="register_user" />
                <input type="hidden" class="nonce" value="<?php echo wp_create_nonce(THEMEX_PREFIX . 'nonce'); ?>" />
                <input type="hidden" class="action" value="<?php echo THEMEX_PREFIX; ?>update_user" />
            </div>
            <div class="form-group flex-row justify-content-end">
                Есть аккаунт?<a href="#" id="ButtonAuthorizationLoginV2" data-smodal-target="SModalAuthorization" class="btn btn-link" style="margin-left: .5em;"><b>Вход</b></a>
            </div>
            <div class="form-group">
                <div class="alert-danger message"></div>
            </div>
            <div class="d-flex flex-column flex-grow-1 justify-content-end">
                <h3 class="h3">Регистрация через соцсети:</h3>
                <?= do_shortcode('[miniorange_social_login shape="round" theme="default" space="4" size="50"]'); ?>
            </div>
        </form>
    </div>
</div>