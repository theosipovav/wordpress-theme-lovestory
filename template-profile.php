<?php

add_action('wp_enqueue_scripts', 'my_scripts_method');
function my_scripts_method()
{
	$script_url = plugins_url('/js/newscript.js', __FILE__);
	wp_enqueue_script('custom-script', get_template_directory_uri() . "/assets/js/template-profile.js", array('jquery-ui-autocomplete'));
}

function my_scripts_yandex()
{
	wp_enqueue_script('api-maps', 'https://api-maps.yandex.ru/1.1/index.xml');
}
add_action('wp_enqueue_scripts', 'my_scripts_yandex');
get_header();
?>
<?php get_sidebar('profile-left'); ?>


<div class="full-profile fivecol column">
	<div class="section-title">
		<h2><?php _e('Анкета', 'lovestory'); ?></h2>
	</div>
	<form class="formatted-form" action="" method="POST">
		<div class="message">
			<?php ThemexInterface::renderMessages(isset($_POST['success']) ? $_POST['success'] : false); ?>
		</div>
		<table class="profile-fields">
			<tbody>
				<th class="form-control-title" colspan="2">Основная информация</th>
				</tr>
				<?php if (!ThemexCore::checkOption('user_name')) { ?>
					<tr>
					<tr>
						<th>Имя</th>
						<td>
							<div class="field-wrap">
								<input type="text" name="first_name" size="50" value="<?php echo ThemexUser::$data['user']['profile']['name']; ?>" />
							</div>
						</td>
					</tr>
				<?php } ?>
				<?php if (!ThemexCore::checkOption('user_gender')) { ?>
					<tr>
						<th>Я</th>
						<td>
							<div class="select-field">
								<span></span>
								<?php
								echo ThemexInterface::renderOption(array(
									'id' => 'gender',
									'type' => 'select',
									'value' => !empty(ThemexUser::$data['user']['profile']['gender']) ? ThemexUser::$data['user']['profile']['gender'] : 'man',
									'options' => ThemexCore::$components['genders'],
									'wrap' => false,
								));
								?>
							</div>
						</td>
					</tr>
					<tr>
						<th>Кого ищем</th>
						<td>
							<div class="select-field">
								<span></span>
								<?php
								echo ThemexInterface::renderOption(array(
									'id' => 'seeking',
									'type' => 'select',
									'value' => !empty(ThemexUser::$data['user']['profile']['seeking']) ? ThemexUser::$data['user']['profile']['seeking'] : 'woman',
									'options' => ThemexCore::$components['genders_alt'],
									'wrap' => false,
								));
								?>
							</div>
						</td>
					</tr>

				<?php } ?>
				<?php if (!ThemexCore::checkOption('user_age')) { ?>
					<tr>
						<th>Мой возраст</th>
						<td>
							<div class="d-flex">
								<input type="date" autocomplete="off" id="age" name="age" min="0" max="99" value="<?php echo ThemexUser::$data['user']['profile']['age']; ?>" style="width: 100%;" />
								<?php
								/*
								echo ThemexInterface::renderOption(array(
									'id' => 'age',
									'type' => 'select_age',
									'value' => ThemexUser::$data['user']['profile']['age'],
									'wrap' => false,
								));
								*/
								?>
							</div>
						</td>
					</tr>
				<?php } ?>
				<tr>
					<th class="th-title">С кем общаюсь</th>
					<td>
						<?php
						echo ThemexInterface::selectCheckbox(array(
							'text' => 'Выберите пол',
							'values' => [
								'write-me-man' => [
									'label' => 'Мужчины',
									'value' => ThemexUser::$data['user']['profile']['write-me-man']
								],
								'write-me-woman' => [
									'label' => 'Женщины',
									'value' => ThemexUser::$data['user']['profile']['write-me-woman']
								],
								'write-me-para' => [
									'label' => 'Пара',
									'value' => ThemexUser::$data['user']['profile']['write-me-para']
								],
								'write-me-mm' => [
									'label' => 'М+М',
									'value' => ThemexUser::$data['user']['profile']['write-me-mm']
								],
								'write-me-zhzh' => [
									'label' => 'Ж+Ж',
									'value' => ThemexUser::$data['user']['profile']['write-me-zhzh']
								],
							],
							'ID' => 'checkboxes001',
						));

						?>
					</td>
				</tr>
				<tr>
					<th class="th-title">Желаемый возраст пользователей</th>
					<td>
						<div class="write-me-age write-me-age-pre">
							<div class="select-field">
								<span style="padding-left: 3em;"></span>
								<select name="write-me-age-pre">
									<?php
									for ($i = 18; $i < 100; $i++) {
										if ($i == ThemexUser::$data['user']['profile']['write-me-age-pre']) {
											echo '<option selected value="' . $i . '">' . $i . '</option>';
										} else {
											echo '<option value="' . $i . '">' . $i . '</option>';
										}
									}
									?>
								</select>
							</div>


						</div>
						<div class="write-me-age write-me-age-after">
							<div class="select-field">
								<span style="padding-left: 3em;"></span>
								<select name="write-me-age-after">
									<?php
									if (!isset((ThemexUser::$data['user']['profile']['write-me-age-after']))) ThemexUser::$data['user']['profile']['write-me-age-after'] = 99;
									if (ThemexUser::$data['user']['profile']['write-me-age-after'] == '') ThemexUser::$data['user']['profile']['write-me-age-after'] = 99;
									if (ThemexUser::$data['user']['profile']['write-me-age-after'] < 18) ThemexUser::$data['user']['profile']['write-me-age-after'] = 99;
									if (ThemexUser::$data['user']['profile']['write-me-age-after'] > 100) ThemexUser::$data['user']['profile']['write-me-age-after'] = 99;
									for ($i = 18; $i < 100; $i++) {
										if ($i == ThemexUser::$data['user']['profile']['write-me-age-after']) {
											echo '<option selected value="' . $i . '">' . $i . '</option>';
										} else {
											echo '<option value="' . $i . '">' . $i . '</option>';
										}
									}
									?>
								</select>
							</div>
						</div>
					</td>
				</tr>
				<?php if (!ThemexCore::checkOption('user_location')) { ?>
					<tr>
						<th>Страна</th>
						<td>
							<div class="field-wrap d-flex flex-column">
								<input id="InputUserProfileCountry" type="text" name="country" size="50" value="<?php echo ThemexUser::$data['user']['profile']['country']; ?>" />
							</div>
							<div id="ContainerInputHelpCountry" class="field-input-help">
								<div id="HelpInputUserProfileCountry" class="field-input-help-value"></div>
								<button type="button" class="btn btn-primary field-input-help-btn" data-from="HelpInputUserProfileCountry" data-for="InputUserProfileCountry" data-field="ContainerInputHelpCountry">Да</button>
							</div>
						</td>
					</tr>
					<tr>
						<th>Город</th>
						<td>
							<div class="field-wrap d-flex flex-column">
								<input id="InputUserProfileCity" type="text" name="city" size="50" value="<?php echo ThemexUser::$data['user']['profile']['city']; ?>" />
							</div>
							<div id="containerInputHelpCity" class="field-input-help">
								<div id="HelpInputUserProfileCity" class="field-input-help-value"></div>
								<button type="button" class="btn btn-primary field-input-help-btn" data-from="HelpInputUserProfileCity" data-for="InputUserProfileCity" data-field="containerInputHelpCity">Да</button>
							</div>
						</td>
					</tr>
					<tr>
						<th class="form-control-title" colspan="2">Дополнительная информация</th>
					</tr>
				<?php } ?>
				<?php

				ThemexForm::renderData('profile', array(
					'edit' =>  true,
					'before_title' => '<tr><th>',
					'after_title' => '</th>',
					'before_content' => '<td>',
					'after_content' => '</td></tr>',
				), ThemexUser::$data['user']['profile']);

				?>
			</tbody>
		</table>

		<a href="#" class="button submit-button"><?php _e('Сохранить', 'lovestory'); ?></a>
		<?php if (!ThemexCore::checkOption('user_remove')) { ?>
			<a href="#remove_profile" class="button profile-remove-button secondary colorbox inline" title="Удалить Анкету">
				<span class="button-icon icon-remove nomargin"></span>
			</a>
		<?php } ?>
		<input type="hidden" name="update" value="1" />
		<input type="hidden" name="user_action" value="update_profile" />
		<input type="hidden" name="nonce" value="<?= wp_create_nonce(THEMEX_PREFIX . 'nonce'); ?>" />
	</form>
	<?php if (!ThemexCore::checkOption('user_remove')) { ?>
		<div class="hidden">
			<section id="remove_profile" class="popup small">
				<form class="formatted-form" action="" method="POST">
					<p><?php _e('Вы уверены, что хотите навсегда удалить свою анкету?', 'lovestory'); ?></p>
					<a href="#" class="button submit-button"><?php _e('Удалить анкету', 'lovestory'); ?></a>
					<input type="hidden" name="user_action" value="remove_profile" />
					<input type="hidden" name="nonce" value="<?php echo wp_create_nonce(THEMEX_PREFIX . 'nonce'); ?>" />
				</form>
			</section>
		</div>
	<?php } ?>
</div>
<?php get_sidebar('profile-right'); ?>
<?php get_footer(); ?>