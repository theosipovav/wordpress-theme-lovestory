<?php get_header(); ?>
<div class="woocommerce" id="container">
	<div id="content" class=" d-flex flex-column">
		<?php

		if (is_singular('product')) {

			while (have_posts()) :
				the_post();
				wc_get_template_part('content', 'single-product');
			endwhile;
		} else {
		?>

			<div>
				<?php if (woocommerce_product_loop()) : ?>
					<?php woocommerce_product_loop_start(); ?>
					<?php if (wc_get_loop_prop('total')) : ?>
						<?php while (have_posts()) : ?>
							<?php the_post(); ?>
							<?php wc_get_template_part('content', 'product'); ?>
						<?php endwhile; ?>
					<?php endif; ?>
					<?php woocommerce_product_loop_end(); ?>
			</div>
	<?php
				else :
					do_action('woocommerce_no_products_found');
				endif;
			}
	?>
	</div>
</div>
<!-- /woocommerce -->
<?php get_footer(); ?>