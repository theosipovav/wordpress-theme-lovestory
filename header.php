<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php wp_title(); ?> - <?php bloginfo('name'); ?></title>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="<?php echo THEME_URI; ?>js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
	<script src="https://api-maps.yandex.ru/2.1/?apikey=e7021e98-6cc8-4137-9b0f-5693bbd3fa4e&lang=ru_RU" type="text/javascript">
	</script>
	<link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri() ?>/assets/lib/bootstrap-4/css/bootstrap-grid.min.css" />
	<link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri() ?>/assets/lib/fontawesome-free-5.13.0-web/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri() ?>/assets/lib/jquery-ui-1.11.4/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri() ?>/assets/css/tooltipster.bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri() ?>/assets/css/remodal.css">
	<link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri() ?>/assets/css/remodal-default-theme.css">
	<link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri() ?>/assets/css/style.min.css">
	<link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri() ?>/assets/css/payment.min.css">
</head>

<?php

wp_enqueue_script('siema', get_template_directory_uri() . '/assets/js/siema.min.js', [], false, true);
accessToRegistered();

?>

<body <?php body_class(); ?>>
	<div class="data-hidden" data-user="<?= get_current_user_id() ?>"></div>
	<?php if (!is_user_logged_in()) {
		get_template_part('module-reg');
	} ?>
	<div class="site-wrap">
		<div class="header-wrap">
			<header class="site-header container">
				<div class="site-logo left">
					<a href="<?php echo SITE_URL; ?>" rel="home">
						<img src="<?php echo ThemexCore::getOption('logo', THEME_URI . 'images/logo.png'); ?>" alt="<?php bloginfo('name'); ?>" />
					</a>
				</div>
				<!-- /logo -->
				<div class="header-options">
					<?php if (is_user_logged_in()) { ?>
						<div class="d-flex justify-content-center">

							<a href="/likes/" class="btn btn-header-icon">
								<img src="<?= get_template_directory_uri() ?>/assets/img/header-heart.svg" alt="Знакомства">
								<span class="btn-header-icon-label">Знакомства</span>
							</a>
							<a href="/search/" class="btn btn-header-icon">
								<img src="<?= get_template_directory_uri() ?>/assets/img/header-search.svg" alt="Поиск">
								<span class="btn-header-icon-label">Поиск</span>
							</a>
							<a href="/adverts/" class="btn btn-header-icon">
								<img src="<?= get_template_directory_uri() ?>/assets/img/header-megaphone.svg" alt="Объявления">
								<span class="btn-header-icon-label">Объявления</span>
							</a>

							<a href="<?php echo wp_logout_url(SITE_URL); ?>" class="btn btn-header-icon">
								<img src="<?= get_template_directory_uri() ?>/assets/img/header-logout.svg" alt="Выход">
								<span class="btn-header-icon-label">Выход</span>
							</a>
							<a href="<?php echo ThemexUser::$data['user']['profile_url']; ?>" class="btn btn-header-icon" alt="Профиль">
								<img src="<?= get_template_directory_uri() ?>/assets/img/header-profile.svg" alt="Профиль">
								<span class="btn-header-icon-label">Профиль</span>

								<div class="header-options-msg-notification <?php if (ThemexUser::countMessages(ThemexUser::$data['user']['ID']) != "") echo 'show' ?>" title="У вас новые сообщения">
									<span class="value">
										<?= ThemexUser::countMessages(ThemexUser::$data['user']['ID']) ?>
									</span>
								</div>
								<div class="header-options-notification-guests-new <?php if (ThemexUser::countGuestsNew(ThemexUser::$data['user']['ID']) > 0) echo 'show' ?>" title="У вас новые гости">
									<span class="value">
										<?= ThemexUser::countGuestsNew(ThemexUser::$data['user']['ID']) ?>
									</span>
								</div>
							</a>
						</div>
					<?php } else { ?>
						<div class="d-flex justify-content-center">
							<button id="ButtonAuthorizationLogin" class="btn btn-header-icon" data-smodal-target="SModalAuthorization">
								<img src="<?= get_template_directory_uri() ?>/assets/img/header-login.svg" alt="Войти">
								<span class="btn-header-icon-label">Войти</span>
							</button>
							<button id="ButtonAuthorizationRegistration" class="btn btn-header-icon" data-smodal-target="SModalRegistration">
								<img src="<?= get_template_directory_uri() ?>/assets/img/header-reg.svg" alt="Регистрация">
								<span class="btn-header-icon-label">Регистрация</span>
							</button>
						</div>
					<?php } ?>
				</div>
				<!-- /options -->
				<?php
				/*
				<nav class="header-menu right" style="margin-right: 25px;">
					<?php wp_nav_menu(array('theme_location' => 'main_menu', 'container_class' => 'menu')); ?>
					<div class="mobile-menu hidden">
						<div class="select-field">
							<span></span>
							<?php ThemexInterface::renderSelectMenu('main_menu'); ?>
						</div>
					</div>
				</nav>
				*/
				?>
				<!-- /menu -->
			</header>
			<div class="container-fluid">
				<?php get_template_part('module-top-photos'); ?>
			</div>
			<!-- /header -->
			<?php if (is_front_page() && is_page()) { ?>
				<?php get_template_part('module', 'slider'); ?>
				<?php if (!ThemexCore::checkOption('search_bar') && !ThemexCore::checkOption('user_gender')) { ?>
					<div class="header-content-wrap overlay-wrap">
						<div class="header-content container">
							<?php get_template_part('module', 'search-bar'); ?>
						</div>
					</div>
				<?php } ?>
			<?php } else { ?>
				<div class="header-content-wrap">
					<div class="header-content container">
						<h1 class="page-title"><?php ThemexInterface::renderPageTitle(); ?></h1>
					</div>
				</div>
			<?php } ?>
			<!-- /content -->
		</div>
		<div class="content-wrap">
			<section class="site-content container clearfix">