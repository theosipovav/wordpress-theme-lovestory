<?php
//Error reporting
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_COMPILE_ERROR);



//Define constants
define('SITE_URL', home_url() . '/');
define('AJAX_URL', admin_url('admin-ajax.php'));
define('THEME_PATH', get_template_directory() . '/');
define('CHILD_PATH', get_stylesheet_directory() . '/');
define('THEME_URI', get_template_directory_uri() . '/');
define('CHILD_URI', get_stylesheet_directory_uri() . '/');
define('THEMEX_PATH', THEME_PATH . 'framework/');
define('THEMEX_URI', THEME_URI . 'framework/');
define('THEMEX_PREFIX', 'themex_');

//Set content width
$content_width = 940;

//Load language files
load_theme_textdomain('lovestory', THEME_PATH . 'languages');

//Include theme functions
include(THEMEX_PATH . 'functions.php');

//Include configuration
include(THEMEX_PATH . 'config.php');

//Include core class
include(THEMEX_PATH . 'classes/themex.core.php');



// AJAX функции
include_once(THEME_PATH . 'functions-ajax.php');


include_once 'framework/classes/themex.livechat.php';
include_once 'framework/classes/themex.topphoto.php';

//Create theme instance
$themex = new ThemexCore($config);

//убираем везде лого wordpress
function wps_admin_bar()
{
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('wp-logo');
    $wp_admin_bar->remove_menu('about');
    $wp_admin_bar->remove_menu('wporg');
    $wp_admin_bar->remove_menu('documentation');
    $wp_admin_bar->remove_menu('support-forums');
    $wp_admin_bar->remove_menu('feedback');
    $wp_admin_bar->remove_menu('view-site');
}
add_action('wp_before_admin_bar_render', 'wps_admin_bar');

// включим регистрацию реколл когда в настройках вордпресса она отключена
function dd3_open_rcl_register()
{
    $option = 1;
    return $option;
}
add_filter('rcl_users_can_register', 'dd3_open_rcl_register');

/* Отключаем админ панель для всех, кроме администраторов. */
if (!current_user_can('administrator')) :
    show_admin_bar(false);
endif;

add_action("adverts_sh_manage_actions_after", "the_excerpt");

add_filter("adverts_form_load", "customize_adverts_add");
function customize_adverts_add($form)
{
    if ($form['name'] != "advert") {
        return $form;
    }
    foreach ($form["field"] as $key => $field) {
        if ($field["name"] == "advert_category") {
            $form["field"][$key]["is_required"] = true;
            $form["field"][$key]["validator"][] = array("name" => "is_required");
        }
    }
    return $form;
}

// Enable shortcodes in text widgets
add_filter('widget_text', 'do_shortcode');

/*
 * Plugin Name: Limit Active Listings.
 * Plugin URI: http://wpadverts.com/
 * Description: Limit the number of concurrent active listings user can have on site.
 * Author: Greg Winiarski
 */
add_action("init", "limit_user_active_listings_init", 20);
/**
 * Replace the [adverts_add] shortcode with a custom shortcode
 * 
 * The custom shortcode will not render the [adverts_add] if the user reached
 * maximum number of concurrent active listings
 * 
 * @since 1.0
 * @return void
 */
function limit_user_active_listings_init()
{

    remove_shortcode("adverts_add");
    add_shortcode("adverts_add", "limit_user_active_listings_shortcode");
}
/**
 * New [adverts_add] shortcode
 * 
 * Shows the error if user reached max number of active listings otherwise shows 
 * [adverts_add] shortcode.
 * 
 * @see shortcode_adverts_add()
 * 
 * @param array $atts   Shortcode attributes
 * @return string       HTML for the shortcode
 */
function limit_user_active_listings_shortcode($atts)
{
    // Change Maximum number of allowed active ads below
    $max = 3;

    $flash = array(
        "error" => array(),
        "info" => array()
    );
    $args = array(
        'post_type' => 'advert',
        'post_status' => array('publish', 'expired', 'pending'),
        'author' => get_current_user_id(),
        'date_query' => array('after' => date("Y-m-d H:i:s", current_time('timestamp') - 24 * 3600))
    );

    $query = new WP_Query($args);
    if ($query->found_posts >= $max) {
        $message = __('You reached maximum active ads limit. You cannot have more than %d active Ads at once.', "limit-user-active-listings");

        $flash["error"][] = array(
            "message" => sprintf($message, $max),
            "icon" => "adverts-icon-attention-alt"
        );

        ob_start();
        adverts_flash($flash);
        return ob_get_clean();
    }
    return shortcode_adverts_add($atts);
}


/**
 * Добавлено новое
 * 
 * TheOsipovAV
 * theosipovav@yandex.ru
 * theosipovav@gmail.com
 * 
 */

add_action('show_user_profile', 'add_extra_social_links');
add_action('edit_user_profile', 'add_extra_social_links');
function add_extra_social_links($user_id)
{
    printf('<div class="form-group p2">');
    printf('<div class="d-flex">');
    if (get_user_meta($user_id->ID, "_themex_write-me-man", true) == 'on') {
        printf('<input type="checkbox" checked class="form-control mr2" name="write-me-man" id="write-me-man">');
    } else {
        printf('<input type="checkbox" class="form-control mr2" name="write-me-man" id="write-me-man">');
    }

    printf('<label for="write-me-man">Мужчина</label>');
    printf('</div>');
    printf('<div class="d-flex">');
    if (get_user_meta($user_id->ID, "_themex_write-me-woman", true) == 'on') {
        printf('<input type="checkbox" checked class="form-control mr2" name="write-me-woman" id="write-me-woman">');
    } else {
        printf('<input type="checkbox" class="form-control mr2" name="write-me-woman" id="write-me-woman">');
    }
    printf('<label for="write-me-woman">Женщина</label>');
    printf('</div>');
    printf('<div class="d-flex">');
    if (get_user_meta($user_id->ID, "_themex_write-me-para", true) == 'on') {
        printf('<input type="checkbox" checked class="form-control mr2" name="write-me-para" id="write-me-para">');
    } else {
        printf('<input type="checkbox" class="form-control mr2" name="write-me-para" id="write-me-para">');
    }
    printf('<label for="write-me-para">пара</label>');
    printf('</div>');
    printf('<div class="d-flex">');
    if (get_user_meta($user_id->ID, "_themex_write-me-mm", true) == 'on') {
        printf('<input type="checkbox" checked class="form-control mr2" name="write-me-mm" id="write-me-mm">');
    } else {
        printf('<input type="checkbox" class="form-control mr2" name="write-me-mm" id="write-me-mm">');
    }
    printf('<label for="write-me-mm">м+м</label>');
    printf('</div>');
    printf('<div class="d-flex">');
    if (get_user_meta($user_id->ID, "_themex_write-me-zhzh", true) == 'on') {
        printf('<input type="checkbox" checked class="form-control mr2" name="write-me-zhzh" id="write-me-zhzh">');
    } else {
        printf('<input type="checkbox" class="form-control mr2" name="write-me-zhzh" id="write-me-zhzh">');
    }
    printf('<label for="write-me-zhzh">ж+ж</label>');
    printf('</div>');
    printf('</div>');
}



/**
 * Отладка. Вывод информации по переменной - в.1
 */
function d($value)
{
    echo '<div class="debug-d">';
    echo "<pre>" . print_r($value, true) . "</pre>";
    echo '</div>';
}
/**
 * Отладка. Вывод информации по переменной - в.2
 */
function dd($value)
{
    echo "<pre>" . print_r($value, true) . "</pre>";
    exit();
}
/**
 * Отладка. Вывод информации по переменной - в.3
 */
function ddd($value)
{
    echo "<pre>" . htmlentities(print_r($value, true)) . "</pre>";
    exit();
}

/**
 * 
 */
function validateDate($date, $format = 'Y-m-d')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}


/**
 * Получить возраст по дате рождения
 */
function getAge($y, $m, $d)
{
    if ($m > date('m') || $m == date('m') && $d > date('d'))
        return (date('Y') - $y - 1);
    else
        return (date('Y') - $y);
}




/** Подгрузка новых анкет для страницы поиска */
// Для не авторизованных пользователей
//
/**
 * Загрузка новых анкет для страницы поиска
 * Для не авторизованных пользователей
 */
function ajaxLoadMore()
{
    $userId = $_POST['id'];
    $res = array();
    array_push($res, ThemexUser::notificationNewMessage($userId));
    array_push($res, ThemexUser::countMessages($userId));
    print_r(json_encode($res));
    die;
}
add_action('wp_ajax_actionLoadMore', 'ajaxLoadMore');


/**
 * Загрузка новых анкет неавторизованным для страницы поиска
 */
add_action('wp_ajax_template_profile', 'ajaxTemplateProfile');
function ajaxTemplateProfile()
{
    if (isset($_GET["reg_country"]) && $_GET["reg_country"] !== "") {
        $regCity = $_GET["reg_country"];
        $json = file_get_contents(get_template_directory_uri() . "/assets/data/city.json");
        $res = [];
        $count = 0;
        $countMax = 15;

        foreach (json_decode($json, true) as $key => $value) {
            if (mb_stripos(" " . $value["name"] . " ", $regCity) != false) {
                array_push($res, $value["name"]);
                $count++;
                if ($count > $countMax) break;
            }
        }

        print_r(json_encode($res));
    }


    if (isset($_GET["reg_city"]) && $_GET["reg_city"] !== "") {
        $regCity = $_GET["reg_city"];
        $json = file_get_contents(get_template_directory_uri() . "/assets/data/cities.json");
        $res = [];
        $count = 0;
        $countMax = 15;
        foreach (json_decode($json, true) as $key => $value) {
            if (mb_stripos(" " . $value["city"] . " ", $regCity) != false) {
                array_push($res, $value["city"]);
                $count++;
                if ($count > $countMax) break;
            }
        }

        print_r(json_encode($res));
    }
    die;
}



add_action('wp_ajax_coins', 'ajaxAddCoins');
function ajaxAddCoins()
{
    if (isset($_GET["reg_country"]) && $_GET["reg_country"] !== "") {
        $regCity = $_GET["reg_country"];
        $json = file_get_contents(get_template_directory_uri() . "/assets/data/city.json");
        $res = [];
        $count = 0;
        $countMax = 15;

        foreach (json_decode($json, true) as $key => $value) {
            if (mb_stripos(" " . $value["name"] . " ", $regCity) != false) {
                array_push($res, $value["name"]);
                $count++;
                if ($count > $countMax) break;
            }
        }

        print_r(json_encode($res));
    }


    if (isset($_GET["reg_city"]) && $_GET["reg_city"] !== "") {
        $regCity = $_GET["reg_city"];
        $json = file_get_contents(get_template_directory_uri() . "/assets/data/cities.json");
        $res = [];
        $count = 0;
        $countMax = 15;
        foreach (json_decode($json, true) as $key => $value) {
            if (mb_stripos(" " . $value["city"] . " ", $regCity) != false) {
                array_push($res, $value["city"]);
                $count++;
                if ($count > $countMax) break;
            }
        }

        print_r(json_encode($res));
    }
    die;
}



function GUID()
{
    if (function_exists('com_create_guid') === true) {
        return trim(com_create_guid(), '{}');
    }

    return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
}
// 
// 
// reCAPTCHA v3 от GOOGLE
// https://www.google.com/recaptcha/admin/site/352208705/setup
// 
// recaptcha-v2-commonlove.st
// Ключ сайта: 6LcGaf4UAAAAAMc43ogE1hW-zlSGqNX-qhW3nwO-
// Секретный ключ: 6LcGaf4UAAAAADCEvYL1EpWKH03BpxrmANhYUIQJ
// 
// recaptcha-v2-commonlove.ru
// Ключ сайта: 6Ldwdf4UAAAAABraS_e41489XoRSdpZB_mbl17oj
// Секретный ключ: 6Ldwdf4UAAAAAGeRulBjKQ4N2D1NrHDGOUtkatAo


/**
 * Подключение и подключение reCAPTCHA (api.js)
 * version - null, in footer - false
 */
function addGoogleRecaptcha()
{
    wp_register_script('recaptcha', 'https://www.google.com/recaptcha/api.js', array(), null, false);
    wp_enqueue_script('recaptcha');
}
add_action('wp_enqueue_scripts', 'addGoogleRecaptcha', 5, 1);
/**
 * 
 */
function renderGoogleRecaptcha()
{
    $recaptchaKeySite = '6Ldwdf4UAAAAABraS_e41489XoRSdpZB_mbl17oj';
    return "<div class='g-recaptcha' data-sitekey='$recaptchaKeySite'></div>";
}



function accessToRegistered()
{
    $uri = $_SERVER['REQUEST_URI'];
    if ($uri == '' || $uri == '/' || $uri == '//') {
        return;
    }
    if (!is_user_logged_in()) {
        switch ($_SERVER['REQUEST_URI']) {
            case "":
            case "/":
            case "//":
            case "/about/":
            case "/privacy/":
            case "/agreement/":
                // Разрешен доступ
                break;
            default:
                // Перенаправление на стартовую страницу
                wp_redirect(get_site_url());
                die();
                break;
        }
    }
}











// отключаем авто-обновления по типу
add_filter('auto_update_core', '__return_false');           // обновление ядра
add_filter('auto_update_theme', '__return_false');          // обновление тем
add_filter('auto_update_plugin', '__return_false');         // обновление плагинов
add_filter('auto_update_translation', '__return_false');    // обновление файлов перевода
add_filter('allow_minor_auto_core_updates', '__return_false');      // авто-обновление минорных версий (версии внутри ветки)
add_filter('allow_major_auto_core_updates', '__return_false');      //  авто-обновление мажорных версий (версии между ветками)
add_filter('allow_dev_auto_core_updates', '__return_false');        // авто-обновление версий разработчиков 
add_theme_support('title-tag');




/* добавление поля в профиле*/
add_action('show_user_profile', 'add_extra_social_links123');
add_action('edit_user_profile', 'add_extra_social_links123');
add_action('personal_options_update', 'save_extra_social_links');
add_action('edit_user_profile_update', 'save_extra_social_links');

function add_extra_social_links123($user)
{
    echo '<h3>Баланс пользователя</h3>';
    echo '<table class="form-table">';
    echo '<tbody>';
    echo '<tr>';
    echo '<th>';
    echo '<label for="balance_coins">Лафчики</label>';
    echo '</th>';
    echo '<td>';
    echo '<input type="number" id="balance_coins" name="balance_coins" value="' . esc_attr(get_the_author_meta('balance_coins', $user->ID)) . '" class="regular-text" />';
    echo '</td>';
    echo '</tr>';
    echo '</tbody>';
    echo '</table>';
}

function save_extra_social_links($user_id)
{
    update_user_meta($user_id, 'balance_coins', sanitize_text_field($_POST['balance_coins']));
}




/** Регистрация нового типа записи "top_photo" - "Фотолинейка"
 */
function createPostTypeTopPhoto()
{
    register_post_type('top_photo', array(
        'labels'             => array(
            'name'               => 'Фотолинейка',
            'singular_name'      => 'Фотолинейка',
            'add_new'            => 'Добавить пользователя',
            'add_new_item'       => 'Добавить пользователя',
            'edit_item'          => 'Редактировать',
            'new_item'           => 'Новый пользователь',
            'view_item'          => 'Посмотреть',
            'search_items'       => 'Найти',
            'not_found'          => 'Не найдено',
            'parent_item_colon'  => '',
            'menu_name'          => 'Фотолинейка'
        ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => true,
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array('title', 'author', 'excerpt')
    ));
}
add_action('init', 'createPostTypeTopPhoto');


function wpschool_api_add_admin_menu()
{
    add_options_page('Фотолинейка', 'Фотолинейка', 'manage_options', 'settings-page-top-photo', 'wpschool_api_options_page');
}

function wpschool_api_settings_init()
{
    register_setting('setting_top_photo', 'settings_top_photo');
    add_settings_section(
        'wpschool_api_wpschoolCustom_section',
        __('Основные настройки', 'wordpress'),
        'wpschool_api_settings_section_callback',
        'setting_top_photo'
    );

    add_settings_field(
        'top_photo_field_0',
        __('Ссылка на страницу оплаты услуги', 'wordpress'),
        'top_photo_field_0_render',
        'setting_top_photo',
        'wpschool_api_wpschoolCustom_section'
    );
    add_settings_field(
        'top_photo_field_1',
        __('Ссылка на страницу при недостатке средств', 'wordpress'),
        'top_photo_field_1_render',
        'setting_top_photo',
        'wpschool_api_wpschoolCustom_section'
    );
    add_settings_field(
        'top_photo_field_2',
        __('Заголовок', 'wordpress'),
        'top_photo_field_2_render',
        'setting_top_photo',
        'wpschool_api_wpschoolCustom_section'
    );
    add_settings_field(
        'top_photo_field_3',
        __('Подзаголовок', 'wordpress'),
        'top_photo_field_3_render',
        'setting_top_photo',
        'wpschool_api_wpschoolCustom_section'
    );
    add_settings_field(
        'top_photo_field_4',
        __('Количество фото в линейки', 'wordpress'),
        'top_photo_field_4_render',
        'setting_top_photo',
        'wpschool_api_wpschoolCustom_section'
    );
}

function top_photo_field_0_render()
{
    $options = get_option('settings_top_photo');
    echo "<input type='url' name='settings_top_photo[top_photo_field_0]' value='" . $options['top_photo_field_0'] . "' style='width: 100%;'/>";
}
function top_photo_field_1_render()
{
    $options = get_option('settings_top_photo');
    echo "<input type='url' name='settings_top_photo[top_photo_field_1]' value='" . $options['top_photo_field_1'] . "' style='width: 100%;'/>";
}
function top_photo_field_2_render()
{
    $options = get_option('settings_top_photo');
    echo "<input type='text' name='settings_top_photo[top_photo_field_2]' value='" . $options['top_photo_field_2'] . "' style='width: 100%;'/>";
}
function top_photo_field_3_render()
{
    $options = get_option('settings_top_photo');
    echo "<input type='text' name='settings_top_photo[top_photo_field_3]' value='" . $options['top_photo_field_3'] . "' style='width: 100%;'/>";
}
function top_photo_field_4_render()
{
    $options = get_option('settings_top_photo');
    echo "<input type='number' name='settings_top_photo[top_photo_field_4]' value='" . $options['top_photo_field_4'] . "' style='width: 100%;'/>";
}

function wpschool_api_settings_section_callback()
{
    echo __('Настройки для фотолинейки', 'wordpress');
}

function wpschool_api_options_page()
{
    echo "<form action='options.php' method='post'>";
    echo "<h2>Фотолинейка</h2>";
    settings_fields('setting_top_photo');
    do_settings_sections('setting_top_photo');
    submit_button();
    echo "</form>";
}
add_action('admin_menu', 'wpschool_api_add_admin_menu');
add_action('admin_init', 'wpschool_api_settings_init');



include('functions-wc.php');

include('functions-ykassa.php');





// 08.08.2020
/**
 * Лайки. Получить список пользователей
 */
function ajaxLoadProfilesForLikes()
{
    if (isset($_GET["user"]) && $_GET["user"] !== "") {
        $currentThemexUser = ThemexUser::getUser($_GET["user"], true);
        $exclude = [];
        if (is_array($currentThemexUser['favorites']) && count($currentThemexUser['favorites']) > 0) {
            foreach ($currentThemexUser['favorites'] as $key => $value) {
                $exclude[] = $value["ID"];
            }
        }
        if (is_array($currentThemexUser['ignores']) && count($currentThemexUser['ignores']) > 0) {
            foreach ($currentThemexUser['ignores'] as $key => $value) {
                $exclude[] = $value["ID"];
            }
        }
        $meta_query = [];
        $meta_query[] = [
            'key' => '_themex_city',
            'value' => $currentThemexUser['profile']['city'],
            'compare' => 'LIKE',
        ];
        $meta_query[] = [
            'key' => '_themex_gender',
            'value' => $currentThemexUser['profile']['seeking'],
            'compare' => 'LIKE',
        ];
        $meta_query[] = [
            'key' => '_themex_seeking',
            'value' => $currentThemexUser['profile']['gender'],
            'compare' => 'LIKE',
        ];
        $dtAfter = new DateTime();
        $dtAfter->modify("-" . $currentThemexUser['profile']['write-me-age-after'] . " year");
        $meta_query[] = [
            'key' => '_themex_age',
            'value' => $dtAfter->format("Y-m-d"),
            'compare' => '>=',
            'type' => 'DATE'
        ];
        $dtPre = new DateTime();
        $dtPre->modify("-" . $currentThemexUser['profile']['write-me-age-pre'] . " year");
        $meta_query[] = [
            'key' => '_themex_age',
            'value' => $dtPre->format("Y-m-d"),
            'compare' => '<=',
            'type' => 'DATE'
        ];
        $users = get_users([
            'meta_query'   => $meta_query,
            'exclude'      => $exclude,
            'orderby'      => 'name',
            'order'        => 'DESC',
            'number'       => 10,
            'paged'        => 1,
            'date_query'   => array()
        ]);
        $res = "";
        foreach ($users as $user) {
            $userActive = ThemexUser::getUser($user->ID);
            $userId = $userActive["ID"];
            $profile = $userActive['profile'];
            $name = $profile["name"];
            if (validateDate($profile["age"], "Y-m-d")) {
                $dateTime = new DateTime($profile["age"]);
                $age = getAge($dateTime->format("Y"), $dateTime->format("m"), $dateTime->format("d"));
            } else {
                $age = "???";
            }
            $country = $profile["country"];
            if ($country == 'RU') {
                $country = 'Россия';
            }
            $city = $profile["city"];
            $about = $profile["description"];
            $res .= '<div class="tinder--card" data-user="' . $userId . '">';
            $res .= get_avatar($userId, 600);
            $res .= '<h3><span>' . $name . '</span>,<span>' . $age . '</span></h3>';
            $res .= '<p><span>' . $country . '</span>, <span>' . $city . '</span><span>' . $about . '</span></p>';
            $res .= '</div>';
        }
        echo $res;
    }
    die;
}
add_action('wp_ajax_load_profiles_for_likes', 'ajaxLoadProfilesForLikes');

/**
 * Лайки. Добавить лайк пользователю
 */
function ajaxAddLikeForLikes()
{
    if (!isset($_GET["user"]) && $_GET["user"] == "") {
        die;
    }
    if (!isset($_GET["userActive"]) && $_GET["userActive"] == "") {
        die;
    }
    $userId = $_GET["user"];
    $userActiveId = $_GET["userActive"];

    $user = ThemexUser::getUser($userId, true);

    $favorites = [];
    if (is_array($user['favorites'])) {
        $favorites = $user['favorites'];
    }

    array_unshift($favorites, array(
        'ID' => $userActiveId,
        'date' => current_time('timestamp'),
    ));
    ThemexCore::updateUserMeta($userId, 'favorites', $favorites);
    die;
}
add_action('wp_ajax_add_like_for_likes', 'ajaxAddLikeForLikes');

/**
 * Лайки. Добавить дизлайк пользователю
 */
function ajaxAddDislikeForLikes()
{
    if (!isset($_GET["user"]) && $_GET["user"] == "") {
        die;
    }
    if (!isset($_GET["userActive"]) && $_GET["userActive"] == "") {
        die;
    }
    $userId = $_GET["user"];
    $userActiveId = $_GET["userActive"];
    $user = ThemexUser::getUser($userId, true);
    $ignores = [];
    if (is_array($user['ignores'])) {
        $ignores = $user['ignores'];
    }
    array_unshift($ignores, array(
        'ID' => $userActiveId,
        'date' => current_time('timestamp'),
    ));
    ThemexCore::updateUserMeta($userId, 'ignores', $ignores);
    die;
}
add_action('wp_ajax_add_dislike_for_likes', 'ajaxAddDislikeForLikes');




function ajaxWidgetMessage()
{

    $user = ThemexUser::getUser(get_current_user_id(), true);
    $userActive = ThemexUser::getUser($_GET['id'], true);
    $messages = ThemexUser::getMessages($user['ID'], $userActive['ID'], $page = null);
    echo '<ul class="bordered-list-message">';


    $lastMessageId = -1;
    foreach ($messages as $message) {
        $lastMessageId = $message->comment_ID;
        $GLOBALS['comment'] = $message;
        if ($message->user_id == ThemexUser::$data['user']['ID']) {
            get_template_part('content', 'message-me');
        } else {
            get_template_part('content', 'message');
        }
    }
    echo "</ul>";

    die;
}
add_action('wp_ajax_widget_message', 'ajaxWidgetMessage');
