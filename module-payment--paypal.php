<div class="module-payment-paypal">
    <div class="premium-plus">
        <ul>
            <li>
                <img class="premium-plus--icon" src="<?= get_template_directory_uri() ?>/assets/img/premium-plus--icon-1.svg" alt="" srcset=""> <span class="premium-plus--text">Займите первое место!</span>
            </li>
            <li>
                <img class="premium-plus--icon" src="<?= get_template_directory_uri() ?>/assets/img/premium-plus--icon-2.svg" alt="" srcset=""> <span class="premium-plus--text">Станьте известнее!</span>
            </li>
            <li>
                <img class="premium-plus--icon" src="<?= get_template_directory_uri() ?>/assets/img/premium-plus--icon-3.svg" alt="" srcset=""> <span class="premium-plus--text">Появляйтесь в Знакомствах чаще!</span>
            </li>
        </ul>
    </div>
</div>