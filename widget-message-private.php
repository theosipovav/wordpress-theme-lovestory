<div id="WidgetMessagePrivate" class="widget-message-private ">
    <?php
    //$LiveChat->userActive = ThemexUser::getUser(1, true);
    ?>
    <?php
    $userCurrent = ThemexUser::getUser(ThemexUser::$data['user']["ID"], true);
    ?>
    <div class="widget-message-private-header" data-status="0">
        <?php include 'widget-message-private--content-header.php'; ?>
    </div>
    <ul class="widget-message-private-list" data-status="0">
        <?php include 'widget-message-private--content-list.php'; ?>
    </ul>
    <div class="widget-message-private-form-container d-flex">
        <?php include 'widget-message-private--content-form.php'; ?>
    </div>
</div>