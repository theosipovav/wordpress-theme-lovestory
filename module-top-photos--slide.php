<div class="slide">
    <?php if (is_user_logged_in()) { ?>
        <a href="<?= $url ?>" class="tooltip slide-img" data-tooltip-content="#tooltip_content_<?= $userId ?>">
            <?= get_avatar($userId, 175); ?>
        </a>
    <?php } else { ?>
        <a href="#" class="tooltip slide-img btn-top-photo-registration" data-smodal-target="SModalRegistration" data-tooltip-content="#tooltip_content_<?= $i ?>">
            <?= get_avatar($userId, 175); ?>
        </a>
    <?php } ?>
    <div class="tooltip_templates">

        <div id="tooltip_content_<?= $userId ?>" class="tooltip_content slide-info">
            <div class="slide-info--name">
                <?php
                if ($status == 'offline') {
                    echo "<span class='slide-info--status' title='Не в сети'></span>";
                } else {
                    echo "<span class='slide-info--status online' title='Сейчас на сайте'></span>";
                }
                ?>
                <span><?= $name ?></span>,
                <span><?= $age ?></span>
            </div>
            <div class="slide-info--location">
                <span><?= $country ?></span>, <span><?= $city ?></span>
            </div>
            <div class="slide-info--add">
                <span><?= $about ?></span>
            </div>
        </div>
    </div>
</div>