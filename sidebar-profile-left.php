<aside class="column threecol">
	<div class="profile-preview">
		<div class="profile-image">
			<?= get_avatar(ThemexUser::$data['user']['ID'], 200); ?>
		</div>
		<div class="profile-options clearfix">
			<form action="" enctype="multipart/form-data" method="POST" class="upload-form">
				<label for="user_avatar" class="button small"><?php _e('Обновить Фото', 'lovestory'); ?></label>
				<input type="file" id="user_avatar" name="user_avatar" class="shifted" />
				<input type="hidden" name="user_action" value="update_avatar" />
				<input type="hidden" name="nonce" value="<?php echo wp_create_nonce(THEMEX_PREFIX . 'nonce'); ?>" />
			</form>
		</div>
	</div>
	<div class="">
		<?php get_template_part('content', 'profile-views-mounts'); ?>
	</div>
	<div class="widget profile-menu">
		<ul>
			<li <?php if (get_query_var('author')) { ?>class="current" <?php } ?>><a href="<?php echo ThemexUser::$data['user']['profile_url']; ?>"><?php _e('Моя анкета', 'lovestory'); ?></a></li>
			<?php if (ThemexUser::$data['user']['membership']['ID'] >= 0) { ?>
				<li <?php if (get_query_var('memberships')) { ?>class="current" <?php } ?>><a href="<?php echo ThemexUser::$data['user']['memberships_url']; ?>"><?php _e('Мой статус', 'lovestory'); ?></a></li>
			<?php } ?>
			<li <?php if (get_query_var('settings')) { ?>class="current" <?php } ?>><a href="<?php echo ThemexUser::$data['user']['settings_url']; ?>"><?php _e('Мои настройки', 'lovestory'); ?></a></li>

		</ul>
	</div>
	<!-- widget-balance -->
	<?= ThemexInterface::renderWidgetBalance(ThemexUser::$data['user']['ID']) ?>
	<!-- >> widget-balance -->
	<!-- widget-available-messages -->
	<?= ThemexInterface::renderWidgetAvailableMessages(ThemexUser::$data['user']['ID']) ?>
	<!-- // widget-available-messages -->

</aside>