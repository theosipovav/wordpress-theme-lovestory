<?php
$counterView = ThemexUser::$data['active_user']['profile']['counter-view'];
$counterViewData = ThemexUser::$data['active_user']['profile']['counter-view-data'];
if ($counterView == null || $counterView == '' || !isset($counterView)) {
    $counterView = 0;
    $counterViewData = (new DateTime())->format("m");
}
if (ThemexUser::$data['user']["ID"] != ThemexUser::$data['active_user']["ID"]) {

    $counterViewData = ThemexUser::$data['active_user']['profile']['counter-view-data'];
    if ($counterViewData != (new DateTime())->format("m")) {
        $counterView = 0;
        $counterViewData = (new DateTime())->format("m");
    } else {
        $counterView = $counterView + 1;
    }
    $activeUserId = ThemexUser::$data['active_user']["ID"];
    ThemexCore::updateUserMeta($activeUserId, 'counter-view', $counterView);
    ThemexCore::updateUserMeta($activeUserId, 'counter-view-data', $counterViewData);
}
?>
<div class="views-mounts">
    <div class="views-mounts--title">
        Просмотров за месяц
    </div>
    <div class="views-mounts--count">
        <?= $counterView ?>
    </div>
</div>