("use strict");

var tinderContainer = document.querySelector(".tinder");
var allCards = document.querySelectorAll(".tinder--card");
var nope = document.getElementById("nope");
var love = document.getElementById("love");

function initCards(card, index) {
  var newCards = document.querySelectorAll(".tinder--card:not(.removed)");
  if (newCards.length <= 0) {
    var userId = $(".data-hidden").attr("data-user");
    $.ajax({
      url: "/wp-admin/admin-ajax.php?action=load_profiles_for_likes&user=" + userId,
      dataType: "html",
      success: function (data) {
        if (data != "") {
          $(".tinder--cards").html(data);
          allCards = document.querySelectorAll(".tinder--card");
          initHammer();
        } else {
          $(".tinder--cards").html('<div class="tinder--card tinder--card-no"><img src="/wp-content/themes/lovestory/images/avatar.png" class="avatar" width="600" alt=""><h3>Никаких профилей не найдено.</h3><p>Повторите попытку позже.</p></div>');
          $(".tinder").addClass("tinder-no-profiles");
        }
      },
      error: function (jqXHR, exception) {
        console.log(jqXHR);
        console.log(exception);
      },
    });
  }

  newCards.forEach(function (card, index) {
    card.style.zIndex = allCards.length - index;
    card.style.transform = "scale(" + (20 - index) / 20 + ") translateY(-" + 30 * index + "px)";
    card.style.opacity = (10 - index) / 10;
  });

  tinderContainer.classList.add("loaded");
}

function initHammer() {
  allCards.forEach(function (el) {
    var hammertime = new Hammer(el);

    hammertime.on("pan", function (event) {
      el.classList.add("moving");
    });

    hammertime.on("pan", function (event) {
      if (event.deltaX === 0) return;
      if (event.center.x === 0 && event.center.y === 0) return;

      tinderContainer.classList.toggle("tinder_love", event.deltaX > 0);
      tinderContainer.classList.toggle("tinder_nope", event.deltaX < 0);

      var xMulti = event.deltaX * 0.03;
      var yMulti = event.deltaY / 80;
      var rotate = xMulti * yMulti;

      event.target.style.transform = "translate(" + event.deltaX + "px, " + event.deltaY + "px) rotate(" + rotate + "deg)";
    });

    hammertime.on("panend", function (event) {
      el.classList.remove("moving");

      var moveOutWidth = document.body.clientWidth;
      var keep = Math.abs(event.deltaX) < 80 || Math.abs(event.velocityX) < 0.5;

      event.target.classList.toggle("removed", !keep);

      if (keep) {
        event.target.style.transform = "";
      } else {
        var endX = Math.max(Math.abs(event.velocityX) * moveOutWidth, moveOutWidth);
        var toX = event.deltaX > 0 ? endX : -endX;
        var endY = Math.abs(event.velocityY) * moveOutWidth;
        var toY = event.deltaY > 0 ? endY : -endY;
        var xMulti = event.deltaX * 0.03;
        var yMulti = event.deltaY / 80;
        var rotate = xMulti * yMulti;

        event.target.style.transform = "translate(" + toX + "px, " + (toY + event.deltaY) + "px) rotate(" + rotate + "deg)";
        initCards();
      }

      if (!keep) {
        if ($(".tinder").hasClass("tinder_love")) {
          var userActiveId = $(event.target).attr("data-user");
          var userId = $(".data-hidden").attr("data-user");
          addLike(userId, userActiveId);
        }
        if ($(".tinder").hasClass("tinder_nope")) {
          var userActiveId = $(event.target).attr("data-user");
          var userId = $(".data-hidden").attr("data-user");
          addDislike(userId, userActiveId);
        }
      }
      tinderContainer.classList.remove("tinder_love");
      tinderContainer.classList.remove("tinder_nope");
    });
  });
}

initCards();
initHammer();

function addLike(userId, userActiveId) {
  $.ajax({
    url: "/wp-admin/admin-ajax.php?action=add_like_for_likes&user=" + userId + "&userActive=" + userActiveId,
    dataType: "text",
    success: function (data) { },
    error: function (jqXHR, exception) {
      console.log(jqXHR);
      console.log(exception);
    },
  });
}
function addDislike(userId, userActiveId) {
  $.ajax({
    url: "/wp-admin/admin-ajax.php?action=add_dislike_for_likes&user=" + userId + "&userActive=" + userActiveId,
    dataType: "text",
    success: function (data) { },
    error: function (jqXHR, exception) {
      console.log(jqXHR);
      console.log(exception);
    },
  });
}

function createButtonListener(love) {
  return function (event) {
    var cards = document.querySelectorAll(".tinder--card:not(.removed)");
    var moveOutWidth = document.body.clientWidth * 1.5;
    if (!cards.length) return false;
    var card = cards[0];
    card.classList.add("removed");
    var userActiveId = $($(".tinder--card")[0]).attr("data-user");
    var userId = $(".data-hidden").attr("data-user");
    if (love) {
      card.style.transform = "translate(" + moveOutWidth + "px, -100px) rotate(-30deg)";
      addLike(userId, userActiveId);
    } else {
      card.style.transform = "translate(-" + moveOutWidth + "px, -100px) rotate(30deg)";
      addDislike(userId, userActiveId);
    }
    initCards();
    event.preventDefault();
  };
}

var nopeListener = createButtonListener(false);
var loveListener = createButtonListener(true);

nope.addEventListener("click", nopeListener);
love.addEventListener("click", loveListener);
