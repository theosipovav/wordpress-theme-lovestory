//DOM Loaded

jQuery(document).ready(function ($) {
  // FieldInputHelpCity
  var inputCity = $("input#form-city");

  // Автодополнение
  inputCity.autocomplete({
    source: function (request, response) {
      var reg_city = inputCity.val();
      $.ajax({
        url: "/wp-admin/admin-ajax.php?action=template_profile&reg_city=" + reg_city,
        dataType: "json",
        data: {
          term: request.term,
        },
        success: function (data) {
          response(data);
        },
        error: function (jqXHR, exception) {
          console.log(jqXHR);
          console.log(exception);
        },
      });
    },
    minLength: 2,
    delay: 500,
    disabled: false,
  });
});
