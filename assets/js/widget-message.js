
var timerWidgetMessageAll;
var timerWidgetMessagePrivate;
var timerWidgetMessageCheckMessage;

jQuery(function ($) {
    var userId = $("#WidgetMessageAll").attr("user-id");
    timerWidgetMessageCheckMessage = setInterval(function () {
        var ajaxUrl = "/wp-admin/admin-ajax.php?action=widget_message_check&id=" + userId;
        $.ajax({
            url: ajaxUrl,
            dataType: "text",
            success: function (data) {
                if (data == '0') {
                    $("#textWidgetMessageSwitch").html("Сообщения");
                    $("#textWidgetMessageSwitch").removeClass("widget-message-switch-new-message");
                } else {
                    $("#textWidgetMessageSwitch").html("Новые сообщения");
                    $("#textWidgetMessageSwitch").addClass("widget-message-switch-new-message");

                }
            },
            error: function (jqXHR, exception, a) {
                console.log(jqXHR);
                console.log(exception);
            },
        });
    }, 2000);

    // Обновление приватного чата раз в 5 секунд
    timerWidgetMessagePrivate = setInterval(function () {
        messengerPrivateMessagesAutoUpdate(userId, null, false);
    }, 5000);


    if ($("#WidgetMessageAll").hasClass("widget-message-content-active")) {
        var userId = $("#WidgetMessageAll").attr("user-id");
        callWidgetMessageAll(userId);
    }
    if ($("#WidgetMessagePrivate").hasClass("widget-message-content-active")) {

    }



    $(".widget-message--minimize").click(function (e) {
        $(".widget-message--minimize").removeClass("show");
        $(".widget-message--maximize").addClass("show");
        $(".widget-message").removeClass("widget-message-maximize");
        $(".widget-message").addClass("widget-message-minimize");
        $("#ButtonWidgetOpenMessagesUrl").removeClass("show");
        $("#ButtonWidgetRefresh").removeClass("show");
    });
    $("#textWidgetMessageSwitch").click(function (e) {
        $(".widget-message--maximize").removeClass("show");
        $(".widget-message--minimize").addClass("show");
        $(".widget-message").removeClass("widget-message-minimize");
        $(".widget-message").addClass("widget-message-maximize");
        $("#ButtonWidgetOpenMessagesUrl").addClass("show");
        $("#ButtonWidgetRefresh").addClass("show");
    });
    $(".widget-message--maximize").click(function (e) {
        $(".widget-message--maximize").removeClass("show");
        $(".widget-message--minimize").addClass("show");
        $(".widget-message").removeClass("widget-message-minimize");
        $(".widget-message").addClass("widget-message-maximize");
        $("#ButtonWidgetOpenMessagesUrl").addClass("show");
        $("#ButtonWidgetRefresh").addClass("show");
    });




    $(".widget-message-card-user").click(function (e) {
        e.preventDefault();
        var id = $(this).attr("data-user");


        $(".widget-message-list-message").html();
        $.ajax({
            url: "/wp-admin/admin-ajax.php?action=widget_message&id=" + id,
            dataType: "html",
            success: function (data) {
                $(".widget-message-list-message").html(data);
            },
            error: function (jqXHR, exception) {
                console.log(jqXHR);
                console.log(exception);
            },
        });
    });

});

/**
 * Обновление живого чата раз в 5 секунд
 * @param {*} id 
 */
function callWidgetMessageAll(id) {
    timerWidgetMessageAll = setInterval(function () {
        // console.log("timerWidgetMessageAll :: Прослушивается");
        if ($("#WidgetMessageAll").hasClass("widget-message-content-active")) {
            $.ajax({
                url: "/wp-admin/admin-ajax.php?action=widget_message_all&id=" + id,
                dataType: "html",
                success: function (dataHtml) {
                    $("#WidgetMessageAll ul.widget-message-all-list").html(dataHtml);
                },
                error: function (jqXHR, exception) {
                    console.log(jqXHR);
                    console.log(exception);
                },
            });
        } else {
            clearTimeout(timerWidgetMessageAll);
            // console.log("timerWidgetMessageAll :: Отключен");
        }
    }, 5000);
}



/**
 * Загрузить приватный чат
 * @param {*} userId 
 * @param {*} userActiveId 
 */
function clickWidgetMessageAllItem(userId, userActiveId) {
    var actionForHeader = "/wp-admin/admin-ajax.php?action=widget_message_private__header&id_user=" + userId + "&id_user_active=" + userActiveId;
    // Загрузить верхнюю часть окна приватной переписки
    $.ajax({
        url: actionForHeader,
        dataType: "html",
        success: function (dataHtml) {
            $(".widget-message-private-header").html(dataHtml);
            $(".widget-message-private-header").attr("data-status", "1");
            loadWidgetMessagePrivate();
        },
        error: function (jqXHR, exception) {
            console.log(jqXHR);
            console.log(exception);
        },
    });
    // Загрузить сообщения окна приватной переписки
    var actionForContent = "/wp-admin/admin-ajax.php?action=widget_message_private__content&id_user=" + userId + "&id_user_active=" + userActiveId;
    $.ajax({
        url: actionForContent,
        dataType: "html",
        success: function (dataHtml) {
            $(".widget-message-private-list").html(dataHtml);
            $(".widget-message-private-list").attr("data-status", "1");
            loadWidgetMessagePrivate();
        },
        error: function (jqXHR, exception) {
            console.log(jqXHR);
            console.log(exception);
        },
    });
    // Загрузить форму для отправки сообщения
    var actionForForm = "/wp-admin/admin-ajax.php?action=widget_message_private__form&id_user=" + userId + "&id_user_active=" + userActiveId;
    $.ajax({
        url: actionForForm,
        dataType: "html",
        success: function (dataHtml) {
            $(".widget-message-private-form-container").html(dataHtml);
            $(".widget-message-private-form").attr("data-status", "1");
            loadWidgetMessagePrivate();
        },
        error: function (jqXHR, exception) {
            console.log(jqXHR);
            console.log(exception);
        },
    });

}
/**
 * Загрузить приватный чат
 */
function loadWidgetMessagePrivate() {
    var isHeaderLoading = 0;
    var isListLoading = 0;
    var isFormLoading = 0;
    if ($(".widget-message-private-header").attr("data-status") == "1") {
        isHeaderLoading = 1;
    }
    if ($(".widget-message-private-list").attr("data-status") == "1") {
        isListLoading = 1;
    }
    if ($(".widget-message-private-form").attr("data-status") == "1") {
        isFormLoading = 1
    }
    if (isHeaderLoading == 1 && isListLoading == 1 && isFormLoading == 1) {
        $("#WidgetMessageAll.widget-message-content-active").removeClass("widget-message-content-active");
        $("#WidgetMessagePrivate").addClass("widget-message-content-active");
        $(".widget-message-private-list").scrollTop(9999999);
        // var userId = $("#FormWidgetPrivateForm").attr("data-user-id");
        // var userActiveId = $("#FormWidgetPrivateForm").attr("data-active-user-id");

    }
}
/**
 * Кнопка назад
 */
function clickWidgetMessagePrivateBack() {
    $("#WidgetMessageAll").addClass("widget-message-content-active");
    $("#WidgetMessagePrivate.widget-message-content-active").removeClass("widget-message-content-active");
    $(".widget-message-private-header").attr("data-status", "0");
    $(".widget-message-private-list").attr("data-status", "0");
    var userId = $("#WidgetMessageAll").attr("user-id");
    callWidgetMessageAll(userId);
}




/**
 * Авто-обновление приватного чата
 * @param {*} userId 
 * @param {*} userActiveId 
 * @param {*} isScroll 
 */
function messengerPrivateMessagesAutoUpdate(userId, userActiveId = null, isScroll = true) {
    if ($("#WidgetMessagePrivate").hasClass("widget-message-content-active") == false) {
        // console.log("messengerPrivateMessagesAutoUpdate - ОТМЕНЕН (1)");
        return;
    }
    if (userActiveId == null) {
        userActiveId = $("#InputWidgetMessageUserActiveId").val();
    }
    // console.log("messengerPrivateMessagesAutoUpdate - ПРОСЛУШИВАЕТСЯ для userActiveId " + userActiveId);

    var action = "/wp-admin/admin-ajax.php?action=widget_message_private__content&id_user=" + userId + "&id_user_active=" + userActiveId;
    $.ajax({
        url: action,
        dataType: "html",
        success: function (dataHtml) {
            $(".widget-message-private-list").html(dataHtml);
            if (isScroll) {
                $(".widget-message-private-list").scrollTop(9999999);
            }
        },
        error: function (jqXHR, exception) {
            console.log(jqXHR);
            console.log(exception);
        },
    });
}

/**
 * Отправить сообщение пользователю
 * @param {string} type Тип сообщения
 */
function sendMessage(type = 'text') {
    var formData = new FormData(document.getElementById("FormWidgetPrivateForm"));
    var userId = $("#FormWidgetPrivateForm").attr("data-user-id");
    var userActiveId = $("#FormWidgetPrivateForm").attr("data-active-user-id");
    var buttonSendImage = $("#LabelUploadPhoto");
    var buttonSend = $("#ButtonFormWidgetPrivateFormSubmit");
    var action = "/wp-admin/admin-ajax.php?action=widget_message_send";
    if (type == 'photo') {
        action = "/wp-admin/admin-ajax.php?action=widget_message_send_image";
    }
    if ($(buttonSendImage).hasClass("disabled")) {
        return;
    }
    if ($(buttonSend).hasClass("disabled")) {
        return;
    }
    $(buttonSendImage).addClass("disabled");
    $(buttonSend).addClass("disabled");
    $.ajax({
        type: "POST",
        url: action,
        data: formData,
        processData: false,
        contentType: false,
        dataType: 'json',
        success: function (response) {
            messengerPrivateMessagesAutoUpdate(userId, null);
            $("#FormWidgetPrivateForm [name=user_message]").val("");
            $(buttonSendImage).removeClass("disabled");
            $(buttonSend).removeClass("disabled");
            if (response.available_messages <= 0) {
                $("#FormWidgetPrivateForm").html(response.html_alert);
            }
        },
    });
}





function clickWidgetMessageRefresh() {
    $("#WidgetMessageAll").addClass("widget-message-content-active");
    $("#WidgetMessagePrivate").removeClass("widget-message-content-active");
    $(".widget-message-private-header").attr("data-status", "0");
    $(".widget-message-private-list").attr("data-status", "0");
    var userId = $("#WidgetMessageAll").attr("user-id");
    callWidgetMessageAll(userId);

}



function lodToastMessageNotification(userId) {
    // console.log("Fn: lodToastMessageNotification");
    // console.log(userId);
    var action = 'action=wp_ajax_widget_message_check&id=' + userId;
    // console.log(action);
    $.ajax({
        url: action,
        dataType: "html",
        success: function (data) {
            // console.log(data);
        },
        error: function (jqXHR, exception) {
            console.log(jqXHR);
            console.log(exception);
        },
    });



}