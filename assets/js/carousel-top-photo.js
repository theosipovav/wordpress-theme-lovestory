




jQuery(function ($) {





  if (document.querySelector('.carousel-top-photo') != null) {
    const siemaTopPhotos = new Siema({
      selector: ".carousel-top-photo",
      duration: 300,
      easing: "ease-out",
      perPage: {
        0: 5,
        600: 6,
        700: 7,
        900: 8,
        1000: 9,
        1000: 10,
        1150: 11,
        1300: 12,
        1450: 10,
        1650: 11,
      },
      startIndex: 0,
      draggable: true,
      multipleDrag: true,
      threshold: 20,
      loop: false,
      rtl: false,
      onInit: () => { },
      onChange: () => { },
    });
    //setInterval(() => siemaTopPhotos.next(), 3000)






    var styleDefault = $(".carousel-top-photo>div").attr("style");


    //      
    var timerWidgetTopPhoto;
    timerWidgetTopPhoto = setInterval(function () {
      var idLastTopPhoto = $('.carousel-top-photo').attr("data-last-top-photo-id");
      var idCurrentUser = $('.carousel-top-photo').attr("data-current-user");

      var url = "/wp-admin/admin-ajax.php?action=widget_update_top_photo&id=" + idLastTopPhoto + "&id_user=" + idCurrentUser;
      $.ajax({
        url: url,
        dataType: "json",
        success: function (response) {
          if (response.length > 0) {
            //console.log("Ожидается обновление: " + response.length);
            response.forEach(r => {
              console.log(r);
              var elements = $(".carousel-top-photo>div>div");
              var clone = $(elements[0]).clone();
              var add = clone[0];
              $(add).find("a").attr("href", r.user.profile_url);
              $(add).find("img").attr("src", r.user_avatar_url);
              elements.last().remove();
              $(".carousel-top-photo>div").prepend(add);
              $(".carousel-top-photo>div").attr("style", styleDefault);
              if (r.id > idLastTopPhoto) {
                idLastTopPhoto = r.id;
              }
              $('.carousel-top-photo').attr("data-last-top-photo-id", idLastTopPhoto);
            });


          } else {
            // console.log("Обновление не требуется");
          }

        },
        error: function (jqXHR, exception) {
          console.log(jqXHR);
          console.log(exception);
        }
      });




    }, 5000);

  } else {
    const siemaTopPhotos = null;
  }








  $(document).ready(function () {
    $(".module-top-photos").css("opacity", 1);
  });
});