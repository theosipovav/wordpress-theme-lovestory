jQuery(function ($) {
  $(document).ready(function () {
    $(".tooltip").tooltipster({
      contentCloning: true,
      animation: "fade",
      delay: 200,
      contentAsHTML: true,
      theme: ["tooltipster-noir", "tooltipster-noir-customized"],
      functionPosition: function (instance, helper, position) {
        var eTop = $("div#module-top-photos-1").offset().top;
        var topNew = eTop - $(window).scrollTop();
        position.coord.top = topNew + 170;
        position.coord.left += 0;
        return position;
      },
    });

    var carouselTopPhotosInfo = $(".module-top-photos .slide-info");
    for (let i = 0; i < carouselTopPhotosInfo.length; i++) {
      let element = $(carouselTopPhotosInfo[i]);
      let h = element.height() - 10;
      let value = "-" + h + "px";
      $(element).css("bottom", value);
    }




    //  The second way to initialize:
    $("[data-remodal-id=modal2]").remodal({
      modifier: "with-red-theme",
    });

    //
    $("#form-search-filter .disabled").click(function (e) {
      var inst = $("[data-remodal-id=modal-form-search-payment]").remodal();
      inst.open();
    });

    $(".form-group.write-me").click(function (e) {
      if ($(".form-group.write-me").hasClass("active") != true) {
        $(this).addClass("active");
      }
    });
    //

    $(".close-toast").click(function (e) {
      e.preventDefault();
      closetoast = $(this).parent().attr("id");
      $("#" + closetoast).fadeOut(400);
    });

    // Авторизация и регистрация
    // Вход
    $("#ButtonAuthorizationLogin").click(function (e) {
      let target = $(this).attr("data-smodal-target");
      if ($("#" + target).hasClass("active")) {
        $("#" + target).removeClass("active");
        $("body").css("overflow", "auto");
        $(".body-overflow ").css("display", "none");
      } else {
        $("#" + target).addClass("active");
        $("body").css("overflow", "hidden");
        $(".body-overflow ").css("display", "block");
      }
    });
    $("#ButtonAuthorizationLoginV2").click(function (e) {
      $(".smodal").removeClass("active");
      $("body").css("overflow", "auto");
      $(".body-overflow ").css("display", "none");
      let target = $(this).attr("data-smodal-target");
      if ($("#" + target).hasClass("active")) {
        $("#" + target).removeClass("active");
        $("body").css("overflow", "auto");
        $(".body-overflow ").css("display", "none");
      } else {
        $("#" + target).addClass("active");
        $("body").css("overflow", "hidden");
        $(".body-overflow ").css("display", "block");
      }
    });
    // Регистрация
    $("#ButtonAuthorizationRegistration").click(function (e) {
      let target = $(this).attr("data-smodal-target");
      if ($("#" + target).hasClass("active")) {
        $("#" + target).removeClass("active");
        $("body").css("overflow", "auto");
        $(".body-overflow ").css("display", "none");
      } else {
        $("#" + target).addClass("active");
        $("body").css("overflow", "hidden");
        $(".body-overflow ").css("display", "block");
      }
    });
    $(".btn-registration").click(function (e) {
      let target = $(this).attr("data-smodal-target");
      if ($("#" + target).hasClass("active")) {
        $("#" + target).removeClass("active");
        $("body").css("overflow", "auto");
        $(".body-overflow ").css("display", "none");
      } else {
        $("#" + target).addClass("active");
        $("body").css("overflow", "hidden");
        $(".body-overflow ").css("display", "block");
      }
    });

    $(".btn-top-photo-registration").click(function (e) {
      let target = $(this).attr("data-smodal-target");
      if ($("#" + target).hasClass("active")) {
        $("#" + target).removeClass("active");
        $("body").css("overflow", "auto");
        $(".body-overflow ").css("display", "none");
      } else {
        $("#" + target).addClass("active");
        $("body").css("overflow", "hidden");
        $(".body-overflow ").css("display", "block");
      }
    });

    //
    $(".body-overflow").click(function (e) {
      $(".smodal").removeClass("active");
      $("body").css("overflow", "auto");
      $(".body-overflow ").css("display", "none");
    });
    $(".smodal .smodal-close").click(function (e) {
      $(".smodal").removeClass("active");
      $("body").css("overflow", "auto");
      $(".body-overflow ").css("display", "none");
    });
    $(".smodal .btn-cancel").click(function (e) {
      $(".smodal").removeClass("active");
      $("body").css("overflow", "auto");
      $(".body-overflow ").css("display", "none");
    });

    $(".password-view").click(function (e) {
      e.preventDefault();
      let targetId = $(this).attr("data-target");

      let type = $("input#" + targetId).attr("type");
      if (type == "password") {
        $(this).html('<i class="fas fa-eye-slash"></i>');
        $("input#" + targetId).attr("type", "text");
      } else {
        $(this).html('<i class="fas fa-eye"></i>');
        $("input#" + targetId).attr("type", "password");
      }
    });

    $(".btn-auth-reset").click(function (e) {
      let targetId = $(this).attr("data-target");
      $("#" + targetId).toggleClass("active");
    });

    /** Форма фильтра для поиска */
    $("#checkboxFormSearchPhotosYes").click(function (e) {
      if ($(this).is(":checked")) {
        $("#hiddenFormSearchPhotosYes").val("1");
      } else {
        $("#hiddenFormSearchPhotosYes").val("0");
      }
    });
    $("#checkboxFormSearchPhotosNo").click(function (e) {
      if ($(this).is(":checked")) {
        $("#hiddenFormSearchPhotosNo").val("1");
      } else {
        $("#hiddenFormSearchPhotosNo").val("0");
      }
    });
    $("#checkboxFormSearchOnlineYes").click(function (e) {
      if ($(this).is(":checked")) {
        $("#hiddenFormSearchOnlineYes").val("1");
      } else {
        $("#hiddenFormSearchOnlineYes").val("0");
      }
    });
    $("#checkboxFormSearchOnlineNo").click(function (e) {
      if ($(this).is(":checked")) {
        $("#hiddenFormSearchOnlineNo").val("1");
      } else {
        $("#hiddenFormSearchOnlineNo").val("0");
      }
    });

    $("#ButtonFilter").click(function (e) {
      $(".form-search-filter-content").toggleClass("active");
    });

    $("#checkboxFormSearchAll").click(function (e) {
      if ($("#checkboxFormSearchAll").is(":checked")) {
        $("#checkboxFormSearchPhotosYes").prop("checked", true);
        $("#checkboxFormSearchPhotosNo").prop("checked", true);
        $("#checkboxFormSearchOnlineYes").prop("checked", true);
        $("#checkboxFormSearchOnlineNo").prop("checked", true);
        $("#hiddenFormSearchPhotosYes").val("1");
        $("#hiddenFormSearchPhotosNo").val("1");
        $("#hiddenFormSearchOnlineYes").val("1");
        $("#hiddenFormSearchOnlineNo").val("1");
      } else {
        $("#checkboxFormSearchPhotosYes").prop("checked", false);
        $("#checkboxFormSearchPhotosNo").prop("checked", false);
        $("#checkboxFormSearchOnlineYes").prop("checked", false);
        $("#checkboxFormSearchOnlineNo").prop("checked", false);
        $("#hiddenFormSearchPhotosYes").val("0");
        $("#hiddenFormSearchPhotosNo").val("0");
        $("#hiddenFormSearchOnlineYes").val("0");
        $("#hiddenFormSearchOnlineNo").val("0");
      }
    });
    $("#form-search-filter .form-search-filter-checkbox").click(function (e) {
      if ($("#checkboxFormSearchPhotosYes").is(":checked") && $("#checkboxFormSearchPhotosNo").is(":checked") && $("#checkboxFormSearchOnlineYes").is(":checked") && $("#checkboxFormSearchOnlineNo").is(":checked")) {
        $("#checkboxFormSearchAll").prop("checked", true);
      } else {
        $("#checkboxFormSearchAll").prop("checked", false);
      }
    });

    $("#SubmitPasswordPrivatePhoto").click(function (e) {
      e.preventDefault();
      $(".themex-slider-password").addClass("themex-slider-password-load");

      var userId = $("#InputPasswordPrivatePhoto").attr("data-user-id");
      var password = $("#InputPasswordPrivatePhoto").val();

      $.ajax({
        url: "/wp-admin/admin-ajax.php?action=actionPasswordPrivatePhoto&user=" + userId + "&password=" + password,
        dataType: "json",
        success: function (data) {
          if (data.status == 1) {
            $(".themex-slider-password").removeClass("themex-slider-password-load");
            $(".themex-slider-password").addClass("themex-slider-password-hidden");
            $(".themex-slider-password-message").css("opacity", 0);
            $(".widget-private-photo .list-private-photo").removeClass("effect-blur");
            window.setTimeout(function () {
              $(".themex-slider-password").css("display", "none");
            }, 3000);
          } else {
            $(".themex-slider-password").removeClass("themex-slider-password-load");
            $(".themex-slider-password-message").html(data.data);
            $(".themex-slider-password-message").css("opacity", 1);
            $(".widget-private-photo .list-private-photo").addClass("effect-blur");
          }
        },
        error: function (jqXHR, exception) {
          $(".themex-slider-password").removeClass("themex-slider-password-load");
          $(".themex-slider-password-message").html(data.data);
          $(".themex-slider-password-message").css("opacity", 1);
          $(".widget-private-photo .list-private-photo").addClass("effect-blur");
          console.log(jqXHR);
          console.log(exception);
        },
      });
    });





    /**
     * Полноценный чат
     */
    var buttonSend;
    var buttonSendImage;

    $("#FormMessenger").each(function () {
      var form = $(this);
      var textareaMessage = form.find("#user_message");
      buttonSend = form.find(".messenger-send");
      buttonSendImage = form.find(".messenger-send-image");

      var userName = form.attr("data-user-name");
      var userId = form.attr("data-user-id");
      var activeUserName = form.attr("data-active-user-name");
      var activeUserId = form.attr("data-active-user-id");
      var inputLastMessageId = form.find('input[name="last-message-id"]');
      var list = $("ul.bordered-list-message");
      list.scrollTop(9999999);

      /**
       * Отправка форму с сообщением
       */
      $(form).submit(function (e) {
        // Проверяем сообщение
        if (textareaMessage.val() == "") {
          return false;
        }
        // Проверяем отключена ли кнопка
        if ($(buttonSend).hasClass("disabled")) {
          return;
        }
        // Отключаем кнопку
        $(buttonSend).addClass("disabled");
        // Отправляем форму
        formData = new FormData(document.getElementById("FormMessenger"));
        var url = form.attr("action") + "?action=" + $(".action-messenger-send-message").val();
        $.ajax({
          type: "POST",
          url: url,
          data: formData,
          processData: false,
          contentType: false,
          success: function (response) {
            let messages = JSON.parse(response);
            let lastMessageId = 0;
            for (key in messages) {
              if (messages.hasOwnProperty(key)) {
                let content = "";
                if (messages[key]["user_id"] == userId) {
                  content += '<div class="listed-message listed-message-user">';
                } else {
                  content += '<div class="listed-message listed-message-active-user">';
                }
                content += '<div class="message-header">';
                content += '<div class="message-data">' + messages[key]["comment_date"] + "</div>";
                if (messages[key]["user_id"] == userId) {
                  content += '<div class="message-author"><a href="#">' + userName + "</a></div>";
                } else {
                  content += '<div class="message-author"><a href="#">' + activeUserName + "</a></div>";
                }
                content += '<i class="fas fa-circle"></i>';
                content += "</div>";
                content += '<div class="message-text">' + messages[key]["comment_content"] + "</div>";
                content += "</div>";
                $(list).append("<li>" + content + "</li>");
                list.scrollTop(9999999);
                lastMessageId = messages[key]["comment_ID"];
              }
            }
            inputLastMessageId.val(lastMessageId);
            textareaMessage.val("");
            buttonSend.removeClass("disabled");
            //
            clearInterval(intervalAutoUpdate);
            intervalAutoUpdate = setInterval(function () {
              messengerAutoUpdate();
            }, 3000);
          },
        });
        return false;

      });
      $(".formatted-form-msg").each(function () {
        var form = $(this);

        var temporary = form.find(".temporary");

        if (!form.hasClass("disabled")) {
          /*
          var refresh = setInterval(function () {
            form.submit();
          }, 10000);
    
          */
          //form.submit();
          $(".formatted-form-msg").submit(function () {
            var message = $(this).find(".message");
            temporary.val(message.val());
            message.val("");
            form.submit();
            clearInterval(refresh);
            refresh = setInterval(function () {
              form.submit();
            }, 10000);

            return false;
          });
        } else {
          $(".formatted-form-msg").submit(function () {
            return false;
          });
        }
      });
      /**
         * Отправить картинку в сообщение
         * Событие при клике по кнопке "Отправить фото"
         */
      $("input[type=file]").on("change", function () {
        if ($(buttonSendImage).hasClass("disabled")) {
          return;
        }
        $(buttonSendImage).addClass("disabled");
        formData = new FormData(document.getElementById("FormMessenger"));
        var url = form.attr("action") + "?action=" + form.find(".action-messenger-send-image").val();
        $.ajax({
          type: "POST",
          url: url,
          data: formData,
          processData: false,
          contentType: false,
          success: function (response) {
            let messages = JSON.parse(response);
            let lastMessageId = 0;
            for (key in messages) {
              if (messages.hasOwnProperty(key)) {
                let content = "";
                if (messages[key]["user_id"] == userId) {
                  content += '<div class="listed-message listed-message-user">';
                } else {
                  content += '<div class="listed-message listed-message-active-user">';
                }
                content += '<div class="message-header">';
                content += '<div class="message-data">' + messages[key]["comment_date"] + "</div>";
                if (messages[key]["user_id"] == userId) {
                  content += '<div class="message-author"><a href="#">' + userName + "</a></div>";
                } else {
                  content += '<div class="message-author"><a href="#">' + activeUserName + "</a></div>";
                }
                content += '<i class="fas fa-circle"></i>';
                content += "</div>";
                content += '<div class="message-text">' + messages[key]["comment_content"] + "</div>";
                content += "</div>";
                $(list).append("<li>" + content + "</li>");
                list.scrollTop(9999999);
                lastMessageId = messages[key]["comment_ID"];
              }
            }
            inputLastMessageId.val(lastMessageId);
            $(buttonSendImage).removeClass("disabled");
            //
            clearInterval(intervalAutoUpdate);
            intervalAutoUpdate = setInterval(function () {
              messengerAutoUpdate();
            }, 3000);
          },
        });
        return false;
      });

      /**
       * Автообновление
       */
      var intervalAutoUpdate = setInterval(function () {
        messengerAutoUpdate();
      }, 3000);
    });








  });
});
var expanded = false;
function showCheckboxes(id) {
  var checkboxes = document.getElementById(id);
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}
//
//

//




/**
 * Обновить окно с сообщениями
 */
function messengerAutoUpdate() {
  // console.log("messengerAutoUpdate() :: Вызвана");
  var form = $(".form-messages-v2");
  var userName = form.attr("data-user-name");
  var userId = form.attr("data-user-id");
  var activeUserName = form.attr("data-active-user-name");
  var activeUserId = form.attr("data-active-user-id");
  var inputLastMessageId = form.find('input[name="last-message-id"]');
  var list = $("ul.bordered-list-message");
  formData = new FormData(document.getElementById("FormMessenger"));
  var url = form.attr("action") + "?action=themex_messenger_auto_update";
  $.ajax({
    type: "POST",
    url: url,
    data: formData,
    processData: false,
    contentType: false,
    success: function (response) {
      let messages = JSON.parse(response);
      let lastMessageId = 0;

      if (messages.length == 0) {
        return;
      }

      for (key in messages) {
        if (messages.hasOwnProperty(key)) {
          let content = "";
          if (messages[key]["user_id"] == userId) {
            content += '<div class="listed-message listed-message-user">';
          } else {
            content += '<div class="listed-message listed-message-active-user">';
          }
          content += '<div class="message-header">';
          content += '<div class="message-data">' + messages[key]["comment_date"] + "</div>";
          if (messages[key]["user_id"] == userId) {
            content += '<div class="message-author"><a href="#">' + userName + "</a></div>";
          } else {
            content += '<div class="message-author"><a href="#">' + activeUserName + "</a></div>";
          }
          content += '<i class="fas fa-circle"></i>';
          content += "</div>";
          content += '<div class="message-text">' + messages[key]["comment_content"] + "</div>";
          content += "</div>";
          $(list).append("<li>" + content + "</li>");
          list.scrollTop(9999999);
          lastMessageId = messages[key]["comment_ID"];
        }
      }
      inputLastMessageId.val(lastMessageId);
      $(buttonSend).removeClass("disabled");
      //
      clearInterval(intervalAutoUpdate);
      intervalAutoUpdate = setInterval(function () {
        messengerAutoUpdate();
      }, 3000);
    },
  });
}
