jQuery(function ($) {
  $(document).on("click", "#emoji-picker", function (e) {
    e.stopPropagation();
    $(".msg-editor-emoji-list").toggleClass("active");
    $(".msg-editor-emoji-container").toggleClass("active");
  });

  $(document).click(function (e) {
    if ($(e.target).attr("class") != ".intercom-emoji-picker-groups" && $(e.target).parents(".intercom-emoji-picker-groups").length == 0) {
      $(".msg-editor-emoji-list").removeClass("active");
    }
  });

  $(document).on("click", ".intercom-emoji-picker-emoji", function (e) {
    var smile = $(this).html();
    var img = $(this).find("img");
    var emoji = $(img).attr("alt");
    document.getElementById("user_message").value = document.getElementById("user_message").value + emoji;
  });

  $(".intercom-composer-popover-input").on("input", function () {
    var query = this.value;
    if (query != "") {
      $(".intercom-emoji-picker-emoji:not([title*='" + query + "'])").hide();
    } else {
      $(".intercom-emoji-picker-emoji").show();
    }
  });

  //
});
