//DOM Loaded

jQuery(document).ready(function ($) {
  var buttonFieldInputHelp = $(".field-input-help-btn");
  // ContainerInputHelpCountry
  var inputCountry = $("input#InputUserProfileCountry");
  var containerInputHelpCountry = $("#ContainerInputHelpCountry");
  var fieldHelpCountry = $("#HelpInputUserProfileCountry");
  // FieldInputHelpCity
  var inputCity = $("input#InputUserProfileCity");
  var containerInputHelpCity = $("#containerInputHelpCity");
  var filedHelpCity = $("#HelpInputUserProfileCity");

  if (YMaps.location) {
    filedHelpCity.html(YMaps.location.city);
    fieldHelpCountry.html(YMaps.location.country);
    if (false) {
      // DEV
      console.log("Longitude: " + YMaps.location.longitude);
      console.log("Latitude: " + YMaps.location.latitude);
      console.log("Country: " + YMaps.location.country);
      console.log("region: " + YMaps.location.region);
      console.log("City: " + YMaps.location.city);
    }
  }

  if (fieldHelpCountry.html() == inputCountry.val()) {
    containerInputHelpCountry.addClass("hidden");
  }

  if (filedHelpCity.html() == inputCity.val()) {
    containerInputHelpCity.addClass("hidden");
  }

  // Автодополнение
  inputCity.autocomplete({
    source: function (request, response) {
      var reg_city = inputCity.val();
      $.ajax({
        url: "/wp-admin/admin-ajax.php?action=template_profile&reg_city=" + reg_city,
        dataType: "json",
        data: {
          term: request.term,
        },
        success: function (data) {
          response(data);
        },
        error: function (jqXHR, exception) {
          console.log(jqXHR);
          console.log(exception);
        },
      });
    },
    minLength: 2,
    delay: 500,
    disabled: false,
  });
  inputCountry.autocomplete({
    source: function (request, response) {
      var reg = inputCountry.val();
      $.ajax({
        url: "/wp-admin/admin-ajax.php?action=template_profile&reg_country=" + reg,
        dataType: "json",
        data: {
          term: request.term,
        },
        success: function (data) {
          response(data);
        },
        error: function (jqXHR, exception) {
          console.log(jqXHR);
          console.log(exception);
        },
      });
    },
    minLength: 2,
    delay: 500,
    disabled: false,
  });
  // Клик
  buttonFieldInputHelp.click(function (e) {
    let targetFor = $(this).attr("data-for");
    let targetFrom = $(this).attr("data-from");
    let targetField = $(this).attr("data-field");
    if ($("#" + targetFrom).html() != "") {
      $("#" + targetFor).val($("#" + targetFrom).html());
      $("#" + targetField).addClass("hidden");
    }
  });
});
