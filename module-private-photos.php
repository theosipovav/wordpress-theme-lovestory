<?php

/**
 * Модуль "Приватные фотографии"
 */

?>
<div class="widget widget-private-photo clearfix">
    <h4 class="widget-title clearfix">
        <span class="left">Приватные фотографии</span>
        <span class="widget-options">
            <?php if (ThemexUser::isProfile()) { ?>
                <form action="" enctype="multipart/form-data" method="POST" class="upload-form popup-container">
                    <label for="user_private_photo" title="Загрузить новую фотографию"></label>
                    <input type="file" id="user_private_photo" name="user_private_photo" class="shifted" />
                    <input type="hidden" name="user_action" value="add_private_photo" />
                    <input type="hidden" name="nonce" value="<?= wp_create_nonce(THEMEX_PREFIX . 'nonce') ?>" />
                    <?php if (ThemexUser::$data['user']['membership']['private_photos'] <= 0) { ?>
                        <div class="popup hidden">
                            <ul class="error">
                                <li>Вы превысили количество фотографий</li>
                            </ul>
                        </div>
                    <?php } ?>
                </form>
            <?php } ?>
        </span>
    </h4>
    <?php if (empty(ThemexUser::$data['active_user']['private-photos'])) { ?>
        <span class="secondary">Фотографии еще не загружены.</span>
    <?php } else { ?>
        <div class="themex-slider carousel-slider">
            <div class="themex-slider-password d-flex align-items-center">
                <div class="d-flex flex-column justify-content-center flex-grow-1">
                    <label for="SliderPrivatePassword">Пароль</label>
                    <div class="d-flex">
                        <input id="InputPasswordPrivatePhoto" class="flex-grow-1" type="password" data-user-id="<?= ThemexUser::$data['active_user']['ID'] ?>" name="slider-private-password" id="SliderPrivatePassword">
                        <button id="SubmitPasswordPrivatePhoto" class="btn">
                            <i class="fas fa-lock-open"></i>
                        </button>
                    </div>
                    <div class="d-flex justify-content-center">
                        <p class="themex-slider-password-message">...</p>
                    </div>

                </div>
            </div>
            <ul class="list-private-photo effect-blur">
                <?php
                $current = ThemexUser::isProfile();
                $counter = 0;

                foreach (ThemexUser::sortPhotos(ThemexUser::$data['active_user']['private-photos']) as $photo) {
                    $thumbnail = wp_get_attachment_image_src($photo['ID'], 'full');
                    $fullsize = wp_get_attachment_image_src($photo['ID'], 'extended');
                    $counter++;
                    if ($counter == 1) { ?>
                        <li class="clearfix">
                        <?php } ?>
                        <div class="fourcol static-column <?php if ($counter == 3) { ?>last<?php } ?>">
                            <div class="profile-preview widget-profile">
                                <div class="profile-image popup-container">
                                    <a href="<?= $fullsize[0] ?>" class="colorbox" data-group="photos">
                                        <img src="<?= themex_resize($thumbnail[0], 150, 150) ?>" class="fullwidth" alt="" />
                                    </a>
                                    <?php if (!is_user_logged_in()) { ?>
                                        <div class="popup hidden">
                                            <ul class="error">
                                                <li>Войдите в систему для просмотра полноразмерных фотографий</li>
                                            </ul>
                                        </div>
                                    <?php } ?>
                                </div>
                                <?php if ($current) { ?>
                                    <div class="profile-options clearfix">
                                        <div class="profile-option">
                                            <form class="ajax-form" action="<?= AJAX_URL ?>" method="POST">
                                                <?php if (ThemexUser::isFeaturedPhoto($photo['ID'])) : ?>
                                                    <a href="#" title="Характеристика Фото" data-title="Особенность Фото" class="icon-star submit-button current"></a>
                                                    <input type="hidden" class="toggle" name="user_action" value="unfeature_photo" data-value="feature_photo" />
                                                <?php else : ?>
                                                    <a href="#" title="Характеристика Фото" data-title="Особенность Фото" class="icon-star submit-button"></a>
                                                    <input type="hidden" class="toggle" name="user_action" value="feature_photo" data-value="unfeature_photo" />
                                                <?php endif ?>
                                                <input type="hidden" name="user_photo" value="<?= $photo['ID']; ?>" />
                                                <input type="hidden" class="nonce" value="<?= wp_create_nonce(THEMEX_PREFIX . 'nonce') ?>" />
                                                <input type="hidden" class="action" value="<?= THEMEX_PREFIX ?>update_user" />
                                            </form>
                                        </div>
                                        <div class="profile-option">
                                            <form action="" method="POST">
                                                <a href="#" title="Удалить Фотографию" class="submit-button icon-remove"></a>
                                                <input type="hidden" name="user_private_photo" value="<?= $photo['ID']; ?>" />
                                                <input type="hidden" name="user_action" value="remove_private_photo" />
                                                <input type="hidden" name="nonce" value="<?= wp_create_nonce(THEMEX_PREFIX . 'nonce') ?>" />
                                            </form>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php
                        if ($counter == 3) {
                            $counter = 0;
                        ?>
                        </li>
                    <?php
                        }
                    }
                    if ($counter !== 0) {
                    ?>
                    </li>
                <?php } ?>
            </ul>
        </div>
    <?php } ?>
</div>