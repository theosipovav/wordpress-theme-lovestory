<?php
/*
Template Name: Лайки
Template Post Type: page
*/

wp_enqueue_script('hammer', get_template_directory_uri() . '/assets/js/hammer.min.js', [], false, true);
wp_enqueue_script('page-likes', get_template_directory_uri() . '/assets/js/page-likes.js', [], false, true);


// Текущий пользователь
$currentUserId = get_current_user_id();
$currentThemexUser = ThemexUser::getUser(get_current_user_id(), true);


$exclude = [];
if (is_array(ThemexUser::$data['user']['favorites']) && count(ThemexUser::$data['user']['favorites']) > 0) {
    foreach (ThemexUser::$data['user']['favorites'] as $key => $value) {
        $exclude[] = $value["ID"];
    }
}
if (is_array(ThemexUser::$data['user']['ignores']) && count(ThemexUser::$data['user']['ignores']) > 0) {
    foreach (ThemexUser::$data['user']['ignores'] as $key => $value) {
        $exclude[] = $value["ID"];
    }
}
$meta_query = [];
$meta_query[] = [
    'key' => '_themex_city',
    'value' => ThemexUser::$data['user']['profile']['city'],
    'compare' => 'LIKE',
];
$meta_query[] = [
    'key' => '_themex_gender',
    'value' => ThemexUser::$data['user']['profile']['seeking'],
    'compare' => 'LIKE',
];
$meta_query[] = [
    'key' => '_themex_seeking',
    'value' => ThemexUser::$data['user']['profile']['gender'],
    'compare' => 'LIKE',
];
$dtAfter = new DateTime();
$dtAfter->modify("-" . ThemexUser::$data['user']['profile']['write-me-age-after'] . " year");
$meta_query[] = [
    'key' => '_themex_age',
    'value' => $dtAfter->format("Y-m-d"),
    'compare' => '>=',
    'type' => 'DATE'
];
$dtPre = new DateTime();
$dtPre->modify("-" . ThemexUser::$data['user']['profile']['write-me-age-pre'] . " year");
$meta_query[] = [
    'key' => '_themex_age',
    'value' => $dtPre->format("Y-m-d"),
    'compare' => '<=',
    'type' => 'DATE'
];

$users = get_users([
    'meta_query'   => $meta_query,
    'exclude'      => $exclude,
    'orderby'      => 'name',
    'number'       => 10,
    'paged'        => 1,
    'date_query'   => array() // смотрите WP_Date_Query
]);










$meta_query = [];
$meta_query[] = [
    'key' => '_themex_favorites',
    'value' => ThemexUser::$data['user']['ID'],
    'compare' => 'LIKE',
];
$usersYouLike = get_users([
    'meta_query'   => $meta_query,
    'orderby'      => 'name',
    'date_query'   => array()
]);





?>
<?php get_header(); ?>
<div class="section-likes">
    <div class="section-likes-content">
        <?php
        ?>

        <div class="tinder <?php if (count($users) == 0) echo "tinder-no-profiles" ?>">
            <div class="tinder--status">
                <div class="tinder--status-remove">
                    <img src="<?= get_template_directory_uri() . "/assets/img/icon-like-dislike-v1.svg" ?>">
                </div>
                <div class="tinder--status-heart">
                    <img src="<?= get_template_directory_uri() . "/assets/img/icon-like-like.svg" ?>">
                </div>

            </div>
            <div class="tinder--cards">
                <?php
                if (count($users) == 0) {
                ?>
                    <div class="tinder--card tinder--card-no">
                        <img src="<?= get_template_directory_uri() ?>/images/avatar.png" class="avatar" width="600" alt="">
                        <h3>Никаких профилей не найдено.</h3>
                        <p>Повторите попытку позже.</p>
                    </div>
                <?php
                }
                foreach ($users as $user) {
                    ThemexUser::$data['active_user'] = ThemexUser::getUser($user->ID);

                    $userId = ThemexUser::$data['active_user']["ID"];

                    $profile = ThemexUser::$data['active_user']['profile'];
                    $name = $profile["name"];
                    if (validateDate($profile["age"], "Y-m-d")) {
                        $dateTime = new DateTime($profile["age"]);
                        $age = getAge($dateTime->format("Y"), $dateTime->format("m"), $dateTime->format("d"));
                    } else {
                        $age = "???";
                    }
                    $country = $profile["country"];
                    if ($country == 'RU') {
                        $country = 'Россия';
                    }
                    $city = $profile["city"];
                    $status = $themexUser['status']['value'];
                    $about = $profile["description"];
                ?>
                    <div class="tinder--card" data-user="<?= $userId ?>">
                        <?= get_avatar($userId, 600); ?>
                        <h3>
                            <span><?= $name ?></span>,
                            <span><?= $age ?></span>
                        </h3>
                        <p>
                            <span><?= $country ?></span>, <span><?= $city ?></span>
                            <span><?= $about ?></span>
                        </p>
                    </div>
                <?php
                }

                ?>
            </div>
            <div class="tinder--buttons">
                <div id="nope" class="btn-action" @click="decide('nope')">
                    <img src="<?= get_template_directory_uri() . "/assets/img/icon-like-dislike-v2.svg" ?>">
                </div>
                <div class="btn-action" @click="decide('super')" style="display:none">
                    <img src="<?= get_template_directory_uri() . "/assets/img/icon-like-star.svg" ?>">
                </div>
                <div id="love" class="btn-action" @click="decide('like')">
                    <img src="<?= get_template_directory_uri() . "/assets/img/icon-like-like.svg" ?>">
                </div>
            </div>
        </div>
    </div>
    <div class="section-likes-info">

        <div class="widget clearfix">
            <h4 class="widget-title clearfix">
                <span class="left">Взаимные симпатии</span>
                <span class="widget-options"></span>
            </h4>
            <?php if (empty(ThemexUser::$data['user']['favorites'])) { ?>
                <span class="secondary">У вас пока что нет взаимных симпатий</span>
            <?php } else { ?>
                <div class="themex-slider carousel-slider">
                    <ul>
                        <?php
                        $current = ThemexUser::isProfile();
                        $counter = 0;

                        foreach (ThemexUser::$data['user']['favorites'] as $user) {
                            $is = false;
                            foreach (ThemexUser::getUser($user["ID"], true)['favorites'] as $f) {
                                if ($f["ID"] == ThemexUser::$data['user']["ID"]) {
                                    $is = true;
                                    break;
                                }
                            }
                            if ($is == false) {
                                continue;
                            }
                            ThemexUser::$data['active_user'] = ThemexUser::getUser($user['ID']);
                            $counter++;
                            if ($counter == 1) {
                        ?>
                                <li class="clearfix">
                                <?php } ?>
                                <div class="fourcol static-column <?php if ($counter == 3) { ?>last<?php } ?>">
                                    <div class="profile-preview widget-profile">
                                        <div class="profile-image">
                                            <a href="<?php echo ThemexUser::$data['active_user']['profile_url']; ?>" title="<?php echo ThemexUser::$data['active_user']['profile']['full_name']; ?>">
                                                <?php echo get_avatar($user['ID'], 100); ?>
                                            </a>
                                        </div>
                                        <?php if ($current) { ?>
                                            <div class="profile-options clearfix">
                                                <div class="profile-option">
                                                    <?php get_template_part('module', 'status'); ?>
                                                </div>
                                                <div class="profile-option">
                                                    <form action="" method="POST">
                                                        <a href="#" title="<?php _e('Удалить из избранного', 'lovestory'); ?>" class="submit-button icon-remove"></a>
                                                        <input type="hidden" name="user_favorite" value="<?php echo $user['ID']; ?>" />
                                                        <input type="hidden" name="user_action" value="remove_favorite" />
                                                        <input type="hidden" name="nonce" value="<?php echo wp_create_nonce(THEMEX_PREFIX . 'nonce'); ?>" />
                                                    </form>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <?php
                                if ($counter == 3) {
                                    $counter = 0;
                                ?>
                                </li>
                            <?php
                                }
                            }

                            ThemexUser::refresh();
                            if ($counter !== 0) {
                            ?>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            <?php } ?>
        </div>


        <div class="widget clearfix">
            <h4 class="widget-title clearfix">
                <span class="left">Вы понравились</span>
                <span class="widget-options"></span>
            </h4>
            <?php if (count($usersYouLike) == 0) { ?>
                <span class="secondary">Пусто</span>
            <?php } else { ?>
                <div class="themex-slider carousel-slider">
                    <ul>
                        <?php
                        $current = ThemexUser::isProfile();
                        $counter = 0;

                        foreach ($usersYouLike as $user) {
                            ThemexUser::$data['active_user'] = ThemexUser::getUser($user->id);
                            $counter++;
                            if ($counter == 1) {
                        ?>
                                <li class="clearfix">
                                <?php } ?>
                                <div class="fourcol static-column <?php if ($counter == 3) { ?>last<?php } ?>">
                                    <div class="profile-preview widget-profile">
                                        <div class="profile-image">
                                            <a href="<?php echo ThemexUser::$data['active_user']['profile_url']; ?>" title="<?php echo ThemexUser::$data['active_user']['profile']['full_name']; ?>">
                                                <?php echo get_avatar($user->id, 100); ?>
                                            </a>
                                        </div>
                                        <?php if ($current) { ?>
                                            <div class="profile-options clearfix">
                                                <div class="profile-option">
                                                    <?php get_template_part('module', 'status'); ?>
                                                </div>
                                                <div class="profile-option">
                                                    <form action="" method="POST">
                                                        <a href="#" title="<?php _e('Удалить из избранного', 'lovestory'); ?>" class="submit-button icon-remove"></a>
                                                        <input type="hidden" name="user_favorite" value="<?php echo $user->id; ?>" />
                                                        <input type="hidden" name="user_action" value="remove_favorite" />
                                                        <input type="hidden" name="nonce" value="<?php echo wp_create_nonce(THEMEX_PREFIX . 'nonce'); ?>" />
                                                    </form>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <?php
                                if ($counter == 3) {
                                    $counter = 0;
                                ?>
                                </li>
                            <?php
                                }
                            }

                            ThemexUser::refresh();
                            if ($counter !== 0) {
                            ?>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            <?php } ?>
        </div>





    </div>
</div>
<?php get_footer(); ?>