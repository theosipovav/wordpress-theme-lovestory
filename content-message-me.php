<li>
    <div class="listed-message listed-message-user">
        <div class="message-header">
            <div class="message-data"><?php comment_time(get_option('date_format')); ?></div>
            <div class="message-author"><a href="<?php echo get_author_posts_url($comment->user_id); ?>"><?php comment_author(); ?></a></div>
            <i class="fas fa-circle"></i>
        </div>
        <div class="message-text">
            <?php comment_text(); ?>
        </div>
    </div>
</li>