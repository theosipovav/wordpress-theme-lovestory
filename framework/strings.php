<?php 
$strings['cel-znakomstva-name']=__('Цель знакомства', 'lovestory');
$strings['cel-znakomstva-options']=__('Семья, Дружба, Секс, Флирт, Путешествия, Общение, Ищу спонсора, Другое', 'lovestory');
$strings['vneshnost-name']=__('Внешность', 'lovestory');
$strings['vneshnost-options']=__('Европейская, Славянская, Азиатская, Африканская, Ближневосточная, Кавказская, Другая, Не важно', 'lovestory');
$strings['rost-sm-name']=__('Рост, см', 'lovestory');
$strings['rost-sm-options']=__('Меньше 150, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, Выше 200', 'lovestory');
$strings['ves-kg-name']=__('Вес, кг', 'lovestory');
$strings['ves-kg-options']=__('Менее 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, Более 100', 'lovestory');
$strings['znak-zodiaka-name']=__('Знак зодиака', 'lovestory');
$strings['znak-zodiaka-options']=__('Овен, Телец, Близнецы, Рак, Лев, Дева, Весы, Скорпион, Стрелец, Козерог, Водолей, Рыбы', 'lovestory');
$strings['obrazovanie-name']=__('Образование', 'lovestory');
$strings['obrazovanie-options']=__('Неполное среднее, Среднее, Высшее, Несколько высших, Кандидат Наук, Бакалавр, Доцент, Другое, Не важно', 'lovestory');
$strings['znanie-yazykov-mira-name']=__('Знание языков мира', 'lovestory');
$strings['znanie-yazykov-mira-options']=__('Китайский, Испанский, Английский, Арабский, Португальский, Хинди, Русский, Японский, Турецкий, Корейский, Французский, Немецкий, Вьетнамский, Итальянский, Персидский, Знаю несколько языков, Знаю язык жестов, Другой', 'lovestory');
$strings['otnoshenie-k-kureniyu-name']=__('Отношение к курению', 'lovestory');
$strings['otnoshenie-k-kureniyu-options']=__('Курю ежедневно, Бросаю, Курю в компании, Не приемлю, Другое', 'lovestory');
$strings['otnoshenie-k-alkogolyu-name']=__('Отношение к алкоголю', 'lovestory');
$strings['otnoshenie-k-alkogolyu-options']=__('Не пью, Пью в компании иногда, Люблю выпить, Не важно', 'lovestory');
$strings['semejnoe-polozhenie-name']=__('Семейное положение', 'lovestory');
$strings['semejnoe-polozhenie-options']=__('Свободные отношения, В поиске, В браке, В разводе, Всё сложно, Другое, Не важно', 'lovestory');
$strings['seksualnye-predpochteniya-name']=__('Сексуальные предпочтения', 'lovestory');
$strings['seksualnye-predpochteniya-options']=__('Ежедневный секс, Несколько раз в неделю, Раз в неделю, Несколько раз в месяц, Не интересует, Это не главное', 'lovestory');
$strings['orientaciya-name']=__('Ориентация', 'lovestory');
$strings['orientaciya-options']=__('Гетеро, Би, Гомо, Другая', 'lovestory');
$strings['vashi-uvlecheniya-i-hobbi-name']=__('Ваши увлечения и хобби', 'lovestory');
$strings['obo-mne-name']=__('Обо мне', 'lovestory');