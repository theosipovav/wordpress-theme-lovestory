<?php



/**
 * Живой чат
 */
class ThemexLiveChat
{
    public $UserCurrent;
    public $UserActive;

    /**
     * Активные чаты
     */
    public $recipients;
    /**
     * Сообщения с выбранным пользователем
     */
    public $messages;

    function __construct($idUserCurrent = -1)
    {
        $this->UserCurrent = false;
        $this->UserActive = false;
        $this->recipients = [];
        $this->messages = [];
    }

    /**
     * Получить ID последнего сообщения
     */
    public function getIdLastMessage()
    {
        if (count($this->messages) > 0) {
            return $this->messages[count($this->messages) - 1]->comment_ID;
        } else {
            return 0;
        }
    }
}
