<?php

class ThemexTopPhoto
{
    // Создать виджет "Фото-линейка"
    function __construct()
    {
        // Конструктор
    }

    /**
     * 
     */
    public static function getSlidesForUpdate($idLast, $idCurrentUser)
    {
        $slides = [];
        foreach (get_posts(['post_type' => 'top_photo', 'numberposts' => -1]) as $key => $p) {
            if (intval($idLast) == intval($p->ID)) {
                break;
            }
            $activeUser = ThemexUser::getUser($p->post_author, true);
            if (intval($idCurrentUser) != null) {
                $currentUser = ThemexUser::getUser($idCurrentUser, true);
                if ($currentUser['profile']['city'] != $activeUser['profile']['city']) {
                    continue;
                }
            }
            $slide = [];
            $slide['id'] = intval($p->ID);
            $avatar_id = ThemexCore::getUserMeta($activeUser["ID"], 'avatar');
            $default = wp_get_attachment_image_src($avatar_id, 'preview');
            if (isset($default[0])) {
                $image = themex_resize($default[0], 300, 300, true, true);
            }
            if (!isset($image)) {
                if ($activeUser['profile']['gender'] == 'woman') {
                    $image = THEME_URI . 'assets/img/avatar-w.png';
                } else {
                    $image = THEME_URI . 'assets/img/avatar-m.png';
                }
            }
            $slide['user_avatar_url'] = $image;
            $slide['user'] = $activeUser;
            $slides[] = $slide;
            if (count($slides) > 100) {
                break;
            }
        }


        return $slides;
    }
}
