<?php
//Columns
add_shortcode('one_sixth', 'themex_one_sixth');
function themex_one_sixth($atts, $content = null)
{
	return '<div class="twocol column">' . do_shortcode($content) . '</div>';
}

add_shortcode('one_sixth_last', 'themex_one_sixth_last');
function themex_one_sixth_last($atts, $content = null)
{
	return '<div class="twocol column last">' . do_shortcode($content) . '</div><div class="clear"></div>';
}

add_shortcode('one_fourth', 'themex_one_fourth');
function themex_one_fourth($atts, $content = null)
{
	return '<div class="threecol column">' . do_shortcode($content) . '</div>';
}

add_shortcode('one_fourth_last', 'themex_one_fourth_last');
function themex_one_fourth_last($atts, $content = null)
{
	return '<div class="threecol column last">' . do_shortcode($content) . '</div><div class="clear"></div>';
}

add_shortcode('one_third', 'themex_one_third');
function themex_one_third($atts, $content = null)
{
	return '<div class="fourcol column">' . do_shortcode($content) . '</div>';
}

add_shortcode('one_third_last', 'themex_one_third_last');
function themex_one_third_last($atts, $content = null)
{
	return '<div class="fourcol column last">' . do_shortcode($content) . '</div><div class="clear"></div>';
}

add_shortcode('five_twelfths', 'themex_five_twelfths');
function themex_five_twelfths($atts, $content = null)
{
	return '<div class="fivecol column">' . do_shortcode($content) . '</div>';
}

add_shortcode('five_twelfths_last', 'themex_five_twelfths_last');
function themex_five_twelfths_last($atts, $content = null)
{
	return '<div class="fivecol column last">' . do_shortcode($content) . '</div><div class="clear"></div>';
}

add_shortcode('one_half', 'themex_one_half');
function themex_one_half($atts, $content = null)
{
	return '<div class="sixcol column">' . do_shortcode($content) . '</div>';
}

add_shortcode('one_half_last', 'themex_one_half_last');
function themex_one_half_last($atts, $content = null)
{
	return '<div class="sixcol column last">' . do_shortcode($content) . '</div><div class="clear"></div>';
}

add_shortcode('seven_twelfths', 'themex_seven_twelfths');
function themex_seven_twelfths($atts, $content = null)
{
	return '<div class="sevencol column">' . do_shortcode($content) . '</div>';
}

add_shortcode('seven_twelfths_last', 'themex_seven_twelfths_last');
function themex_seven_twelfths_last($atts, $content = null)
{
	return '<div class="sevencol column last">' . do_shortcode($content) . '</div><div class="clear"></div>';
}

add_shortcode('two_thirds', 'themex_two_thirds');
function themex_two_thirds($atts, $content = null)
{
	return '<div class="eightcol column">' . do_shortcode($content) . '</div>';
}

add_shortcode('two_thirds_last', 'themex_two_thirds_last');
function themex_two_thirds_last($atts, $content = null)
{
	return '<div class="eightcol column last">' . do_shortcode($content) . '</div><div class="clear"></div>';
}

add_shortcode('three_fourths', 'themex_three_fourths');
function themex_three_fourths($atts, $content = null)
{
	return '<div class="ninecol column">' . do_shortcode($content) . '</div>';
}

add_shortcode('three_fourths_last', 'themex_three_fourths_last');
function themex_three_fourths_last($atts, $content = null)
{
	return '<div class="ninecol column last">' . do_shortcode($content) . '</div><div class="clear"></div>';
}

//Button
add_shortcode('button', 'themex_button');
function themex_button($atts, $content = null)
{
	extract(shortcode_atts(array(
		'url'     	 => '#',
		'target' => 'self',
		'color'   => '',
		'size'	=> '',
	), $atts));

	$out = '<a href="' . $url . '" target="_' . $target . '" class="button ' . $size . ' ' . $color . '">' . do_shortcode($content) . '</a>';
	return $out;
}


//Title
add_shortcode('title', 'themex_title');
function themex_title($atts, $content = null)
{
	extract(shortcode_atts(array(
		'align' => 'left',
	), $atts));

	$out = '<div class="section-title align' . $align . '"><h1>' . do_shortcode($content) . '</h1></div>';
	return $out;
}

//Stories
add_shortcode('stories', 'themex_stories');
function themex_stories($atts, $content = null)
{
	extract(shortcode_atts(array(
		'number' => '3',
		'sections' => '0',
		'order' => 'date',
		'category' => '0',
	), $atts));

	if ($order == 'random') {
		$order = 'rand';
	}

	$args = array(
		'post_type' => 'story',
		'showposts' => $number,
		'orderby' => $order,
	);

	if (!empty($category)) {
		$args['tax_query'][] = array(
			'taxonomy' => 'story_category',
			'terms' => $category,
			'field' => 'term_id',
		);
	}

	$query = new WP_Query($args);

	$out = '<section class="featured-stories">';
	while ($query->have_posts()) {
		$query->the_post();

		$GLOBALS['content'] = wpautop(get_the_excerpt());
		if (intval($sections) != 0) {
			$GLOBALS['content'] = themex_sections(wpautop(get_the_content()), $sections);
		}

		$GLOBALS['content'] = themex_excerpt($GLOBALS['content'], get_permalink());

		ob_start();
		get_template_part('content', 'story-list');
		$out .= ob_get_contents();
		ob_end_clean();
	}
	$out .= '</section>';

	wp_reset_query();
	return $out;
}

//Profiles
add_shortcode('profiles', 'themex_profiles');
function themex_profiles($atts, $content = null)
{
	extract(shortcode_atts(array(
		'number' => '3',
		'order' => 'date',
		'id' => '',
	), $atts));

	$args = array(
		'role' => 'subscriber',
		'number' => $number,
		'orderby' => 'registered',
		'order' => 'DESC',
		'exclude' => get_current_user_id(),
	);

	if (!empty($id)) {
		$args['include'] = explode(',', $id);
	}

	if (ThemexCore::checkOption('user_name')) {
		$args['meta_query'] = array(
			array(
				'key' => '_' . THEMEX_PREFIX . 'updated',
				'value' => '',
				'compare' => '!=',
			),
		);
	} else {
		$args['meta_query'] = array(
			array(
				'key' => 'first_name',
				'value' => '',
				'compare' => '!=',
			),
		);
	}

	if (ThemexCore::checkOption('user_avatar')) {
		$args['meta_query'] = array(
			array(
				'key' => '_' . THEMEX_PREFIX . 'avatar',
				'value' => '',
				'compare' => '!=',
			),
		);
	}

	if ($order == 'name') {
		$args['orderby'] = 'display_name';
		$args['order'] = 'ASC';
	}

	if ($order == 'status' && isset($_SESSION['users']) && !empty($_SESSION['users'])) {
		$args['exclude'] = array_merge(array(get_current_user_id()), array_keys($_SESSION['users']));
		$users = get_users($args);

		$args['include'] = array_diff(array_keys($_SESSION['users']), array(get_current_user_id()));
		$args['exclude'] = get_current_user_id();
		$users = array_slice(array_merge(get_users($args), $users), 0, $number);
	} else {
		$users = get_users($args);
	}

	$out = '<section class="featured-profiles">';
	foreach ($users as $user) {
		$GLOBALS['user'] = $user;

		ob_start();
		get_template_part('content', 'profile-list');
		$out .= ob_get_contents();
		ob_end_clean();
	}
	$out .= '</section>';

	return $out;
}

//Tabs
add_shortcode('tabs', 'themex_tabs');
function themex_tabs($atts, $content = null)
{
	$out = '<div class="tabs-container"><ul class="tabs clearfix">';

	$tabs = explode('][', $content);
	foreach ($tabs as $tab) {
		$title = '';
		preg_match('/tab\s{1,}title=\"(.*)\"/', $tab, $matches);
		if (isset($matches[1])) {
			$title = $matches[1];
		}

		if (!empty($title)) {
			$out .= '<li><h5><a href="#' . themex_sanitize_key($title) . '">' . $title . '</a></h5></li>';
		}
	}

	$out .= '</ul><div class="panes">';
	$out .= do_shortcode($content);
	$out .= '</div></div>';

	return $out;
}

add_shortcode('tab', 'themex_tabs_panes');
function themex_tabs_panes($atts, $content = null)
{
	extract(shortcode_atts(array(
		'title' => '',
	), $atts));

	$out = '<div class="pane" id="' . themex_sanitize_key($title) . '-tab">' . do_shortcode($content) . '</div>';
	return $out;
}

//Toggle
add_shortcode('toggles', 'themex_toggles');
function themex_toggles($atts, $content = null)
{
	$out = '<div class="toggles-container">' . do_shortcode($content) . '</div>';
	return $out;
}

add_shortcode('toggle', 'themex_toggle');
function themex_toggle($atts, $content = null)
{
	extract(shortcode_atts(array(
		'title'    	 => '',
	), $atts));

	$out = '<div class="toggle-container"><div class="toggle-title"><h4>' . $title . '</h4></div>';
	$out .= '<div class="toggle-content"><p>' . do_shortcode($content) . '</p></div></div>';

	return $out;
}



/**
 * Добавлено новое
 * 
 * TheOsipovAV
 * theosipovav@yandex.ru
 * theosipovav@gmail.com
 * 
 */

//ButtonRegistration
add_shortcode('buttonRegistration', 'themex_buttonRegistration');
function themex_buttonRegistration($atts, $content = null)
{
	extract(shortcode_atts(array(
		'id' => '',
		'class'	=> 'button large secondary btn-registration',
	), $atts));

	if (is_user_logged_in()) {
		$out = "";
	} else {
		$out = '<button id="' . $id . '"  data-smodal-target="SModalRegistration" class="' . $class . '">' . do_shortcode($content) . '</button>';
		$out .= do_shortcode('[miniorange_social_login]');
	}
	return $out;
}

// ContentForRegistered
// Отображение контента только для зарегистрированных пользователей
add_shortcode('content_for_registered', 'themex_buttonRegistration');
function themex_ContentForRegistered($atts, $content = null)
{
	if (is_user_logged_in() && !is_null($content) && !is_feed()) {
		return do_shortcode($content);
	}
	return '';
}
// ContentForUnregistered
// Отображение контента только для незарегистрированных пользователей
add_shortcode('content_for_unregistered', 'themex_buttonUnregistration');
function themex_buttonUnregistration($atts, $content = null)
{
	if (!is_user_logged_in() && !is_null($content) && !is_feed()) {
		return do_shortcode($content);
	}
	return '';
}



//buttonLink 
add_shortcode('buttonLink', 'themex_buttonLink');
function themex_buttonLink($atts, $content = null)
{
	extract(shortcode_atts(array(
		'id' => '',
		'class'	=> 'button large secondary btn-registration',
	), $atts));

	if (is_user_logged_in()) {
		$out = "";
	} else {
		$out = '<button id="' . $id . '"  data-smodal-target="SModalRegistration" class="' . $class . '">' . do_shortcode($content) . '</button>';
		$out .= do_shortcode('[miniorange_social_login]');
	}
	return $out;
}


/**
 */
function shortcodeViewBalance($atts = [], $content = null)
{
	$options = shortcode_atts(
		array(
			'url' => '',
		),
		$atts
	);
	if ($options['url'] == '') {
		return ThemexInterface::renderWidgetBalance(ThemexUser::$data['user']['ID']);
	} else {
		return ThemexInterface::renderWidgetBalance(ThemexUser::$data['user']['ID'], $options['url']);
	}
}
add_shortcode('view_balance', 'shortcodeViewBalance');

/**
 */
function shortcodeLogoSite($atts = [], $content = null)
{
	$out = '';
	$out .= '<div class="d-flex justify-content-center">';
	$out .= ThemexInterface::renderLogoSite();
	$out .= '</div>';
	return $out;
}
add_shortcode('logo_site', 'shortcodeLogoSite');



/**
 */
function shortcodeViewProductTopPhoto($atts = [], $content = null)
{
	$options = shortcode_atts(
		array(
			'id' => 0,
		),
		$atts
	);
	$out = '';
	$out .= '<div class="d-flex justify-content-center">';
	$out .= ThemexInterface::renderViewProductTopPhoto($options['id']);
	$out .= '</div>';
	return $out;
}
add_shortcode('view_product_top_photo', 'shortcodeViewProductTopPhoto');


/**
 */
function shortcodeViewProductMessages($atts = [], $content = null)
{
	$options = shortcode_atts(
		array(
			'id' => 0,
		),
		$atts
	);
	$out = '';
	$out .= '<div class="d-flex justify-content-center">';
	$out .= ThemexInterface::renderViewProductMessages($options['id']);
	$out .= '</div>';
	return $out;
}
add_shortcode('view_product_messages', 'shortcodeViewProductMessages');
